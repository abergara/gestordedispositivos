﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DominioGestorDispositivos
{
    public class RegistroDeVariable
    {
        public int RegistroDeVariableId { get; set; }
        public int DispositivoId { get; set; }
        public int VariableId { get; set; }
        public double Valor { get; set; }
        public DateTime FechaYHora { get; set; }

        public RegistroDeVariable()
        {

        }

        public RegistroDeVariable(int dispositivoId, int variableId, double valor, DateTime fechaYHora)
        { 
            DispositivoId = dispositivoId;
            VariableId = variableId;
            Valor = valor;
            FechaYHora = fechaYHora;
        }
    }
}
