﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DominioGestorDispositivos
{
    public class TipoDeDispositivo
    {
        public int TipoDeDispositivoId { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public virtual List<Variable> Variables { get; set; }

        public TipoDeDispositivo()
        {

        }

        public TipoDeDispositivo(string nombre, string descripcion, List<Variable> variables)
        {
            Nombre = nombre;
            Descripcion = descripcion;
            Variables = variables;
        }

        public override bool Equals(object obj)
        {
            if (obj is TipoDeDispositivo)
            {
                return Nombre.Equals(((TipoDeDispositivo)obj).Nombre) ||
                       TipoDeDispositivoId == ((TipoDeDispositivo)obj).TipoDeDispositivoId;
            }          
            else
            {
                return false;
            }
        }
    }
}
