﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DominioGestorDispositivos
{
    public class Dispositivo
    {
        public int DispositivoId { get; set; }
        public TipoDeDispositivo TipoDeDispositivo { get; set; }
        public string Nombre { get; set; }

        public Dispositivo()
        { 
        
        }

        public Dispositivo(string nombre, TipoDeDispositivo unTipoDeDispositivo)
        {
            Nombre = nombre;
            TipoDeDispositivo = unTipoDeDispositivo;
        }

        public override bool Equals(object obj)
        {
            if (obj is Dispositivo)
            {
                return DispositivoId == ((Dispositivo)obj).DispositivoId;
            }
            else
            {
                return false;
            }
        }
    }
}
