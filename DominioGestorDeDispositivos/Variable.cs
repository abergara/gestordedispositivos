﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DominioGestorDispositivos
{
    public class Variable
    {
        public int VariableId { get; set; }
        public string Nombre { get; set; }
        public double Minimo { get; set; }
        public double Maximo { get; set; }

        public Variable()
        {

        }

        public Variable(string nombre, double minimo, double maximo)
        {
            Nombre = nombre;
            Minimo = minimo;
            Maximo = maximo;
        }

        public override bool Equals(object obj)
        {
            if (obj is Variable)
            {
                return VariableId == ((Variable)obj).VariableId;
            }
            else
            {
                return false;
            }
        }
    }
}
