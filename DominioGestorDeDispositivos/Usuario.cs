﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DominioGestorDispositivos
{
    public class Usuario
    {
        public int UsuarioId { get; set; }
        public string Nombre { get; set; }
        public string Email { get; set; }
        public string Clave { get; set; }
        public virtual List<Dispositivo> Dispositivos { get; set; }

        public Usuario()
        { 
        
        }

        public Usuario(string nombre, string email, string clave)
        {
            Nombre = nombre;
            Email = email;
            Clave = clave;
            Dispositivos = new List<Dispositivo>();
        }

        public override bool Equals(object obj)
        {
            if (obj is Usuario)
            {
                return Email == ((Usuario)obj).Email;

            }
            else
            {
                return false;
            }
        }

        public void AgregarDispositivo(Dispositivo unDispositivo)
        {
            Dispositivos.Add(unDispositivo);
        }

        public void ModificarDispositivo(Dispositivo unDispositivo, Dispositivo dispositivoModificado)
        {
            Dispositivos.Remove(unDispositivo);
            Dispositivos.Add(dispositivoModificado);
        }

        public void EliminarDispositivo(Dispositivo unDispositivo)
        {
            Dispositivos.Remove(unDispositivo);
        }
    }
}
