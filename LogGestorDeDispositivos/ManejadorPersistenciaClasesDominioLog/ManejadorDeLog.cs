﻿using ClasesDominioLogGestorDeDispositivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistenciaClasesDominioLog
{
    public abstract class ManejadorDeLog
    {
        private static ManejadorDeLog instanciaManejadorDeLog;

        public static ManejadorDeLog ObtenerInstanciaManejadorDeLog()
        {
            if (instanciaManejadorDeLog == null)
            {
                instanciaManejadorDeLog = new ManejadorDeLogConEntityFramework();
            }
            return instanciaManejadorDeLog;
        }

        public abstract void RegistrarInicioDeSesion(string usuario);

        public abstract void RegistrarImportacionDeValores(string usuario);

        public abstract List<RegistroDeLog> ObtenerRegistrosPorFecha(DateTime fechaInicio, DateTime fechaFin);
    }
}
