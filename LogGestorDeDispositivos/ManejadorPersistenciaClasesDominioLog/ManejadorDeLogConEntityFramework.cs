﻿using ClasesDominioLogGestorDeDispositivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistenciaClasesDominioLog
{
    public class ManejadorDeLogConEntityFramework: ManejadorDeLog  
    {
        public override void RegistrarInicioDeSesion(string usuario)
        {
            RegistroDeLog unRegistroDeLog = new RegistroDeLog(usuario);
            unRegistroDeLog.Accion = RegistroDeLog.TipoAccion.InicioDeSesion;
            AgregarRegistroDeLog(unRegistroDeLog);
        }

        public override void RegistrarImportacionDeValores(string usuario)
        {
            RegistroDeLog unRegistroDeLog = new RegistroDeLog(usuario);
            unRegistroDeLog.Accion = RegistroDeLog.TipoAccion.ImportacionDeValores;
            AgregarRegistroDeLog(unRegistroDeLog);
        }

        private void AgregarRegistroDeLog(RegistroDeLog unRegistroDeLog)
        {
            using (var persistenciaBaseDatosDeLog = new ContextoClasesDominioLog())
            {
                persistenciaBaseDatosDeLog.RegistrosDeLog.Add(unRegistroDeLog);
                persistenciaBaseDatosDeLog.SaveChanges();
            }
        }

        public override List<RegistroDeLog> ObtenerRegistrosPorFecha(DateTime fechaInicio, DateTime fechaFin)
        {
            List<RegistroDeLog> registrosDeLogARetornar = new List<RegistroDeLog>();
            using (var persistenciaBaseDatosDeLog = new ContextoClasesDominioLog())
            {
                var registrosDeLog = from registroDeLog in persistenciaBaseDatosDeLog.RegistrosDeLog
                                     where (DateTime.Compare(registroDeLog.FechaYHora, fechaInicio) > 0 || DateTime.Compare(registroDeLog.FechaYHora, fechaInicio) == 0) &&
                                     (DateTime.Compare(registroDeLog.FechaYHora, fechaFin) < 0 || DateTime.Compare(registroDeLog.FechaYHora, fechaFin) == 0)
                                    orderby registroDeLog.FechaYHora
                                    select registroDeLog;
                foreach (var registroDeLog in registrosDeLog)
                {
                    registrosDeLogARetornar.Add(registroDeLog);
                }
                if (registrosDeLogARetornar.Count == 0)
                {
                    throw new System.ArgumentException("No existen registros entre esas fechas");
                }
                return registrosDeLogARetornar;
            }
        }
    }
}
