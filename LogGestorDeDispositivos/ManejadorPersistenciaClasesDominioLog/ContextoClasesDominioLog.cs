﻿using ClasesDominioLogGestorDeDispositivos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistenciaClasesDominioLog
{
    public class ContextoClasesDominioLog : DbContext
    {
        public DbSet<RegistroDeLog> RegistrosDeLog { get; set; }
    }
}
