﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClasesDominioLogGestorDeDispositivos
{
    public class RegistroDeLog
    {
        public int RegistroDeLogId { get; set; }
        public DateTime FechaYHora {get; set; }
        public string Usuario {get; set; }
        public TipoAccion Accion {get; set; }
        public enum TipoAccion { InicioDeSesion, ImportacionDeValores};

        public RegistroDeLog(string usuario)
        {
            Usuario = usuario;
            FechaYHora = DateTime.Now;
        }

        public RegistroDeLog()
        {
         
        }
    }
}
