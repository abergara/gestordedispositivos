﻿using ClasesDominioLogGestorDeDispositivos;
using PersistenciaClasesDominioLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PruebasConXUnit
{
    [Collection("MisPruebas")]
    public class PruebaManejadorDeLog
    {
        private void EliminarDatosDePrueba()
        {
            using (var persistenciaBaseDatosLog = new ContextoClasesDominioLog())
            {
                foreach (var registro in persistenciaBaseDatosLog.RegistrosDeLog)
                {
                    persistenciaBaseDatosLog.RegistrosDeLog.Remove(registro);
                }
                persistenciaBaseDatosLog.SaveChanges();
            }
        }

        [Fact]
        public void PruebaObtenerInstanciaManejadorDeLog()
        {
            ManejadorDeLog unManejadorLog = ManejadorDeLog.ObtenerInstanciaManejadorDeLog();
            Boolean instanciaBienObtenida = unManejadorLog != null;
            Assert.True(instanciaBienObtenida);
        }

        [Fact]
        public void RegistrarInicioDeSesion()
        {
            ManejadorDeLog unManejadorLog = ManejadorDeLog.ObtenerInstanciaManejadorDeLog();
            unManejadorLog.RegistrarInicioDeSesion("usuario");
            DateTime fechaDesde = DateTime.Parse("13/10/1991");
            DateTime fechaHasta = DateTime.Parse("13/10/2048");
            List<RegistroDeLog> registrosIngresados = unManejadorLog.ObtenerRegistrosPorFecha(fechaDesde, fechaHasta);
            Boolean registroBienIngresado = registrosIngresados.Count == 1;
            registroBienIngresado = registroBienIngresado && registrosIngresados.ElementAt(0).Usuario.Equals("usuario");
            EliminarDatosDePrueba();
            Assert.True(registroBienIngresado);
        }

        [Fact]
        public void RegistrarImportacionDeValores()
        {
            ManejadorDeLog unManejadorLog = ManejadorDeLog.ObtenerInstanciaManejadorDeLog();
            unManejadorLog.RegistrarImportacionDeValores("admin");
            DateTime fechaDesde = DateTime.Parse("13/10/1991");
            DateTime fechaHasta = DateTime.Parse("13/10/2048");
            List<RegistroDeLog> registrosIngresados = unManejadorLog.ObtenerRegistrosPorFecha(fechaDesde, fechaHasta);
            Boolean registroBienIngresado = registrosIngresados.Count == 1;
            registroBienIngresado = registroBienIngresado && registrosIngresados.ElementAt(0).Usuario.Equals("admin");
            EliminarDatosDePrueba();
            Assert.True(registroBienIngresado);
        }

        [Fact]
        public void ObtenerRegistrosPorFecha()
        {
            ManejadorDeLog unManejadorLog = ManejadorDeLog.ObtenerInstanciaManejadorDeLog();
            unManejadorLog.RegistrarImportacionDeValores("admin");
            unManejadorLog.RegistrarImportacionDeValores("admin");
            unManejadorLog.RegistrarImportacionDeValores("admin");
            DateTime fechaDesde = DateTime.Parse("13/10/1991");
            DateTime fechaHasta = DateTime.Parse("13/10/2048");
            List<RegistroDeLog> registrosIngresados = unManejadorLog.ObtenerRegistrosPorFecha(fechaDesde, fechaHasta);
            Boolean registroBienIngresado = registrosIngresados.Count == 3;
            EliminarDatosDePrueba();
            Assert.True(registroBienIngresado);
        }
    }
}
