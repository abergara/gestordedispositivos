﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestorTiposDeDispositivoYVariables
{
    internal class RestriccionesDeInterfaz
    {
        public bool EsLetraTeclaPresionada(KeyPressEventArgs e)
        {
            return char.IsLetter(e.KeyChar);
        }

        public bool EsBarraEspaciadoraTeclaPresionada(KeyPressEventArgs e)
        {
            return e.KeyChar == (char)Keys.Space;
        }

        public bool EsEnterTeclaPresionada(KeyPressEventArgs e)
        {
            return e.KeyChar == (char)Keys.Enter;
        }

        public bool EsBackTeclaPresionada(KeyPressEventArgs e)
        {
            return e.KeyChar == (char)Keys.Back;
        }

        public void NoPermitirSelectorNumericoPasarseDeLargo(int largo, NumericUpDown selectorNumerico, KeyPressEventArgs e)
        {
            RestriccionesDeInterfaz restricciones = new RestriccionesDeInterfaz();
            if (selectorNumerico.Text.Length > largo && !restricciones.EsBackTeclaPresionada(e))
            {
                e.Handled = true;
            }
        }

        public void NoPermitirSelectorVacio(NumericUpDown selectorNumerico)
        {
            if (EstaVacioSelectorNumerico(selectorNumerico))
            {
                selectorNumerico.Text = selectorNumerico.Minimum.ToString();
            }
        }

        public bool EstaVacioSelectorNumerico(NumericUpDown selectorNumerico)
        {
            return string.IsNullOrEmpty(selectorNumerico.Text);
        }

        public bool EstaVaciaCajaTexto(TextBox cajaTexto)
        {
            return string.IsNullOrEmpty(cajaTexto.Text.Trim());
        }

        public void SoloIngresoLetrasEnCajaTextoNombre(KeyPressEventArgs e)
        {
            if (!EsLetraTeclaPresionada(e) && !EsBarraEspaciadoraTeclaPresionada(e) &&
                !EsEnterTeclaPresionada(e) && !EsBackTeclaPresionada(e))
            {
                e.Handled = true;
                return;
            }
        }

        public bool MostrarMensajeDeConfirmacionConCadena(string mensajeAMostrar)
        {
            bool respuestaDeUsuario = true;
            var respuesta = MessageBox.Show(mensajeAMostrar, "Mensaje de Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (respuesta == DialogResult.No)
            {
                respuestaDeUsuario = false;
            }
            return respuestaDeUsuario;
        }
    }
}
