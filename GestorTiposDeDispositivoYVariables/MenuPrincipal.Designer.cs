﻿namespace GestorTiposDeDispositivoYVariables
{
    partial class MenuPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pestañas = new System.Windows.Forms.TabControl();
            this.pestañaVariables = new System.Windows.Forms.TabPage();
            this.botonNuevaVariale = new System.Windows.Forms.Button();
            this.etiquetaVariables = new System.Windows.Forms.Label();
            this.grillaDatosVariables = new System.Windows.Forms.DataGridView();
            this.columnaID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnaNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnaMinimo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnaMaximo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.botonEliminarVariable = new System.Windows.Forms.Button();
            this.BotonModificarVariable = new System.Windows.Forms.Button();
            this.pestañaTiposDeDispositivo = new System.Windows.Forms.TabPage();
            this.botonNuevoTipoDeDispositivo = new System.Windows.Forms.Button();
            this.etiquetaTiposDeDispositivos = new System.Windows.Forms.Label();
            this.grillaDatosVariablesTipoDeDispositivo = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.etiquetaVariablesTiposDeDispositivo = new System.Windows.Forms.Label();
            this.botonEliminarTipoDeDispositivo = new System.Windows.Forms.Button();
            this.botonModificarTipoDeDispositivo = new System.Windows.Forms.Button();
            this.grillaDatosTiposDeDispositivos = new System.Windows.Forms.DataGridView();
            this.columnaTipoID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnaTipoNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnaDescripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pestañaRegistroDeVariable = new System.Windows.Forms.TabPage();
            this.selectorNumericoValor = new System.Windows.Forms.NumericUpDown();
            this.etiquetaValor = new System.Windows.Forms.Label();
            this.botonRegistrar = new System.Windows.Forms.Button();
            this.etiquetaDispositivos = new System.Windows.Forms.Label();
            this.grillaDatosVariablesRegistro = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.etiquetaVariablesRegistro = new System.Windows.Forms.Label();
            this.grillaDatosDispositivos = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoDeDispositivoID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pestañaImportador = new System.Windows.Forms.TabPage();
            this.botonImportar = new System.Windows.Forms.Button();
            this.etiquetaImportadores = new System.Windows.Forms.Label();
            this.grillaDatosImportadores = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.botonConsultar = new System.Windows.Forms.Button();
            this.etiquetaFechaHasta = new System.Windows.Forms.Label();
            this.etiquetaFechaDesde = new System.Windows.Forms.Label();
            this.calendarioFechaHasta = new System.Windows.Forms.DateTimePicker();
            this.calendarioFechaDesde = new System.Windows.Forms.DateTimePicker();
            this.etiquetaLog = new System.Windows.Forms.Label();
            this.grillaDatosLog = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pestañas.SuspendLayout();
            this.pestañaVariables.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grillaDatosVariables)).BeginInit();
            this.pestañaTiposDeDispositivo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grillaDatosVariablesTipoDeDispositivo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grillaDatosTiposDeDispositivos)).BeginInit();
            this.pestañaRegistroDeVariable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.selectorNumericoValor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grillaDatosVariablesRegistro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grillaDatosDispositivos)).BeginInit();
            this.pestañaImportador.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grillaDatosImportadores)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grillaDatosLog)).BeginInit();
            this.SuspendLayout();
            // 
            // pestañas
            // 
            this.pestañas.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.pestañas.Controls.Add(this.pestañaVariables);
            this.pestañas.Controls.Add(this.pestañaTiposDeDispositivo);
            this.pestañas.Controls.Add(this.pestañaRegistroDeVariable);
            this.pestañas.Controls.Add(this.pestañaImportador);
            this.pestañas.Controls.Add(this.tabPage1);
            this.pestañas.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pestañas.ItemSize = new System.Drawing.Size(58, 18);
            this.pestañas.Location = new System.Drawing.Point(12, 12);
            this.pestañas.Name = "pestañas";
            this.pestañas.SelectedIndex = 0;
            this.pestañas.Size = new System.Drawing.Size(778, 464);
            this.pestañas.TabIndex = 0;
            // 
            // pestañaVariables
            // 
            this.pestañaVariables.Controls.Add(this.botonNuevaVariale);
            this.pestañaVariables.Controls.Add(this.etiquetaVariables);
            this.pestañaVariables.Controls.Add(this.grillaDatosVariables);
            this.pestañaVariables.Controls.Add(this.botonEliminarVariable);
            this.pestañaVariables.Controls.Add(this.BotonModificarVariable);
            this.pestañaVariables.Location = new System.Drawing.Point(4, 22);
            this.pestañaVariables.Name = "pestañaVariables";
            this.pestañaVariables.Padding = new System.Windows.Forms.Padding(3);
            this.pestañaVariables.Size = new System.Drawing.Size(770, 438);
            this.pestañaVariables.TabIndex = 2;
            this.pestañaVariables.Text = "Variables";
            this.pestañaVariables.UseVisualStyleBackColor = true;
            // 
            // botonNuevaVariale
            // 
            this.botonNuevaVariale.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botonNuevaVariale.Location = new System.Drawing.Point(527, 401);
            this.botonNuevaVariale.Name = "botonNuevaVariale";
            this.botonNuevaVariale.Size = new System.Drawing.Size(75, 23);
            this.botonNuevaVariale.TabIndex = 23;
            this.botonNuevaVariale.Text = "Nuevo";
            this.botonNuevaVariale.UseVisualStyleBackColor = true;
            this.botonNuevaVariale.Click += new System.EventHandler(this.botonNuevaVariale_Click);
            // 
            // etiquetaVariables
            // 
            this.etiquetaVariables.AutoSize = true;
            this.etiquetaVariables.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.etiquetaVariables.Location = new System.Drawing.Point(3, 9);
            this.etiquetaVariables.Name = "etiquetaVariables";
            this.etiquetaVariables.Size = new System.Drawing.Size(79, 20);
            this.etiquetaVariables.TabIndex = 22;
            this.etiquetaVariables.Text = "Variables:";
            // 
            // grillaDatosVariables
            // 
            this.grillaDatosVariables.AllowUserToAddRows = false;
            this.grillaDatosVariables.AllowUserToDeleteRows = false;
            this.grillaDatosVariables.AllowUserToResizeColumns = false;
            this.grillaDatosVariables.AllowUserToResizeRows = false;
            this.grillaDatosVariables.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grillaDatosVariables.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnaID,
            this.columnaNombre,
            this.columnaMinimo,
            this.columnaMaximo});
            this.grillaDatosVariables.Location = new System.Drawing.Point(6, 35);
            this.grillaDatosVariables.MultiSelect = false;
            this.grillaDatosVariables.Name = "grillaDatosVariables";
            this.grillaDatosVariables.Size = new System.Drawing.Size(758, 342);
            this.grillaDatosVariables.TabIndex = 21;
            // 
            // columnaID
            // 
            this.columnaID.HeaderText = "ID";
            this.columnaID.Name = "columnaID";
            this.columnaID.ReadOnly = true;
            this.columnaID.Width = 40;
            // 
            // columnaNombre
            // 
            this.columnaNombre.HeaderText = "Nombre";
            this.columnaNombre.Name = "columnaNombre";
            this.columnaNombre.ReadOnly = true;
            this.columnaNombre.Width = 282;
            // 
            // columnaMinimo
            // 
            this.columnaMinimo.HeaderText = "Mínimo";
            this.columnaMinimo.Name = "columnaMinimo";
            this.columnaMinimo.ReadOnly = true;
            this.columnaMinimo.Width = 196;
            // 
            // columnaMaximo
            // 
            this.columnaMaximo.HeaderText = "Máximo";
            this.columnaMaximo.Name = "columnaMaximo";
            this.columnaMaximo.ReadOnly = true;
            this.columnaMaximo.Width = 196;
            // 
            // botonEliminarVariable
            // 
            this.botonEliminarVariable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botonEliminarVariable.Location = new System.Drawing.Point(689, 401);
            this.botonEliminarVariable.Name = "botonEliminarVariable";
            this.botonEliminarVariable.Size = new System.Drawing.Size(75, 23);
            this.botonEliminarVariable.TabIndex = 17;
            this.botonEliminarVariable.Text = "Eliminar";
            this.botonEliminarVariable.UseVisualStyleBackColor = true;
            this.botonEliminarVariable.Click += new System.EventHandler(this.botonEliminarVariable_Click);
            // 
            // BotonModificarVariable
            // 
            this.BotonModificarVariable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BotonModificarVariable.Location = new System.Drawing.Point(608, 401);
            this.BotonModificarVariable.Name = "BotonModificarVariable";
            this.BotonModificarVariable.Size = new System.Drawing.Size(75, 23);
            this.BotonModificarVariable.TabIndex = 16;
            this.BotonModificarVariable.Text = "Modificar";
            this.BotonModificarVariable.UseVisualStyleBackColor = true;
            this.BotonModificarVariable.Click += new System.EventHandler(this.BotonModificarVariable_Click);
            // 
            // pestañaTiposDeDispositivo
            // 
            this.pestañaTiposDeDispositivo.Controls.Add(this.botonNuevoTipoDeDispositivo);
            this.pestañaTiposDeDispositivo.Controls.Add(this.etiquetaTiposDeDispositivos);
            this.pestañaTiposDeDispositivo.Controls.Add(this.grillaDatosVariablesTipoDeDispositivo);
            this.pestañaTiposDeDispositivo.Controls.Add(this.etiquetaVariablesTiposDeDispositivo);
            this.pestañaTiposDeDispositivo.Controls.Add(this.botonEliminarTipoDeDispositivo);
            this.pestañaTiposDeDispositivo.Controls.Add(this.botonModificarTipoDeDispositivo);
            this.pestañaTiposDeDispositivo.Controls.Add(this.grillaDatosTiposDeDispositivos);
            this.pestañaTiposDeDispositivo.Location = new System.Drawing.Point(4, 22);
            this.pestañaTiposDeDispositivo.Name = "pestañaTiposDeDispositivo";
            this.pestañaTiposDeDispositivo.Padding = new System.Windows.Forms.Padding(3);
            this.pestañaTiposDeDispositivo.Size = new System.Drawing.Size(770, 438);
            this.pestañaTiposDeDispositivo.TabIndex = 1;
            this.pestañaTiposDeDispositivo.Text = "Tipos de Dispositivo";
            this.pestañaTiposDeDispositivo.UseVisualStyleBackColor = true;
            // 
            // botonNuevoTipoDeDispositivo
            // 
            this.botonNuevoTipoDeDispositivo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botonNuevoTipoDeDispositivo.Location = new System.Drawing.Point(527, 401);
            this.botonNuevoTipoDeDispositivo.Name = "botonNuevoTipoDeDispositivo";
            this.botonNuevoTipoDeDispositivo.Size = new System.Drawing.Size(75, 23);
            this.botonNuevoTipoDeDispositivo.TabIndex = 21;
            this.botonNuevoTipoDeDispositivo.Text = "Nuevo";
            this.botonNuevoTipoDeDispositivo.UseVisualStyleBackColor = true;
            this.botonNuevoTipoDeDispositivo.Click += new System.EventHandler(this.botonNuevoTipoDeDispositivo_Click);
            // 
            // etiquetaTiposDeDispositivos
            // 
            this.etiquetaTiposDeDispositivos.AutoSize = true;
            this.etiquetaTiposDeDispositivos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.etiquetaTiposDeDispositivos.Location = new System.Drawing.Point(3, 9);
            this.etiquetaTiposDeDispositivos.Name = "etiquetaTiposDeDispositivos";
            this.etiquetaTiposDeDispositivos.Size = new System.Drawing.Size(150, 20);
            this.etiquetaTiposDeDispositivos.TabIndex = 20;
            this.etiquetaTiposDeDispositivos.Text = "Tipos de Dispositvo:";
            // 
            // grillaDatosVariablesTipoDeDispositivo
            // 
            this.grillaDatosVariablesTipoDeDispositivo.AllowUserToAddRows = false;
            this.grillaDatosVariablesTipoDeDispositivo.AllowUserToDeleteRows = false;
            this.grillaDatosVariablesTipoDeDispositivo.AllowUserToResizeColumns = false;
            this.grillaDatosVariablesTipoDeDispositivo.AllowUserToResizeRows = false;
            this.grillaDatosVariablesTipoDeDispositivo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grillaDatosVariablesTipoDeDispositivo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
            this.grillaDatosVariablesTipoDeDispositivo.Location = new System.Drawing.Point(6, 274);
            this.grillaDatosVariablesTipoDeDispositivo.Name = "grillaDatosVariablesTipoDeDispositivo";
            this.grillaDatosVariablesTipoDeDispositivo.Size = new System.Drawing.Size(508, 150);
            this.grillaDatosVariablesTipoDeDispositivo.TabIndex = 19;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 40;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Nombre";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Width = 224;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Mínimo";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Máximo";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // etiquetaVariablesTiposDeDispositivo
            // 
            this.etiquetaVariablesTiposDeDispositivo.AutoSize = true;
            this.etiquetaVariablesTiposDeDispositivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.etiquetaVariablesTiposDeDispositivo.Location = new System.Drawing.Point(6, 249);
            this.etiquetaVariablesTiposDeDispositivo.Name = "etiquetaVariablesTiposDeDispositivo";
            this.etiquetaVariablesTiposDeDispositivo.Size = new System.Drawing.Size(79, 20);
            this.etiquetaVariablesTiposDeDispositivo.TabIndex = 18;
            this.etiquetaVariablesTiposDeDispositivo.Text = "Variables:";
            // 
            // botonEliminarTipoDeDispositivo
            // 
            this.botonEliminarTipoDeDispositivo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botonEliminarTipoDeDispositivo.Location = new System.Drawing.Point(689, 401);
            this.botonEliminarTipoDeDispositivo.Name = "botonEliminarTipoDeDispositivo";
            this.botonEliminarTipoDeDispositivo.Size = new System.Drawing.Size(75, 23);
            this.botonEliminarTipoDeDispositivo.TabIndex = 17;
            this.botonEliminarTipoDeDispositivo.Text = "Eliminar";
            this.botonEliminarTipoDeDispositivo.UseVisualStyleBackColor = true;
            this.botonEliminarTipoDeDispositivo.Click += new System.EventHandler(this.botonEliminarTipoDeDispositivo_Click);
            // 
            // botonModificarTipoDeDispositivo
            // 
            this.botonModificarTipoDeDispositivo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botonModificarTipoDeDispositivo.Location = new System.Drawing.Point(608, 401);
            this.botonModificarTipoDeDispositivo.Name = "botonModificarTipoDeDispositivo";
            this.botonModificarTipoDeDispositivo.Size = new System.Drawing.Size(75, 23);
            this.botonModificarTipoDeDispositivo.TabIndex = 16;
            this.botonModificarTipoDeDispositivo.Text = "Modificar";
            this.botonModificarTipoDeDispositivo.UseVisualStyleBackColor = true;
            this.botonModificarTipoDeDispositivo.Click += new System.EventHandler(this.botonModificarTipoDeDispositivo_Click);
            // 
            // grillaDatosTiposDeDispositivos
            // 
            this.grillaDatosTiposDeDispositivos.AllowUserToAddRows = false;
            this.grillaDatosTiposDeDispositivos.AllowUserToDeleteRows = false;
            this.grillaDatosTiposDeDispositivos.AllowUserToResizeColumns = false;
            this.grillaDatosTiposDeDispositivos.AllowUserToResizeRows = false;
            this.grillaDatosTiposDeDispositivos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grillaDatosTiposDeDispositivos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnaTipoID,
            this.columnaTipoNombre,
            this.columnaDescripcion});
            this.grillaDatosTiposDeDispositivos.Cursor = System.Windows.Forms.Cursors.Default;
            this.grillaDatosTiposDeDispositivos.Location = new System.Drawing.Point(6, 35);
            this.grillaDatosTiposDeDispositivos.MultiSelect = false;
            this.grillaDatosTiposDeDispositivos.Name = "grillaDatosTiposDeDispositivos";
            this.grillaDatosTiposDeDispositivos.Size = new System.Drawing.Size(758, 186);
            this.grillaDatosTiposDeDispositivos.TabIndex = 15;
            this.grillaDatosTiposDeDispositivos.Click += new System.EventHandler(this.grillaDatosTiposDeDispositivos_Click);
            // 
            // columnaTipoID
            // 
            this.columnaTipoID.HeaderText = "ID";
            this.columnaTipoID.Name = "columnaTipoID";
            this.columnaTipoID.ReadOnly = true;
            this.columnaTipoID.Width = 40;
            // 
            // columnaTipoNombre
            // 
            this.columnaTipoNombre.HeaderText = "Nombre";
            this.columnaTipoNombre.Name = "columnaTipoNombre";
            this.columnaTipoNombre.ReadOnly = true;
            this.columnaTipoNombre.Width = 282;
            // 
            // columnaDescripcion
            // 
            this.columnaDescripcion.HeaderText = "Descripción";
            this.columnaDescripcion.Name = "columnaDescripcion";
            this.columnaDescripcion.ReadOnly = true;
            this.columnaDescripcion.Width = 392;
            // 
            // pestañaRegistroDeVariable
            // 
            this.pestañaRegistroDeVariable.Controls.Add(this.selectorNumericoValor);
            this.pestañaRegistroDeVariable.Controls.Add(this.etiquetaValor);
            this.pestañaRegistroDeVariable.Controls.Add(this.botonRegistrar);
            this.pestañaRegistroDeVariable.Controls.Add(this.etiquetaDispositivos);
            this.pestañaRegistroDeVariable.Controls.Add(this.grillaDatosVariablesRegistro);
            this.pestañaRegistroDeVariable.Controls.Add(this.etiquetaVariablesRegistro);
            this.pestañaRegistroDeVariable.Controls.Add(this.grillaDatosDispositivos);
            this.pestañaRegistroDeVariable.Location = new System.Drawing.Point(4, 22);
            this.pestañaRegistroDeVariable.Name = "pestañaRegistroDeVariable";
            this.pestañaRegistroDeVariable.Padding = new System.Windows.Forms.Padding(3);
            this.pestañaRegistroDeVariable.Size = new System.Drawing.Size(770, 438);
            this.pestañaRegistroDeVariable.TabIndex = 3;
            this.pestañaRegistroDeVariable.Text = "Registro de Variable";
            this.pestañaRegistroDeVariable.UseVisualStyleBackColor = true;
            this.pestañaRegistroDeVariable.Enter += new System.EventHandler(this.pestañaRegistroDeVariable_Enter);
            // 
            // selectorNumericoValor
            // 
            this.selectorNumericoValor.DecimalPlaces = 2;
            this.selectorNumericoValor.Location = new System.Drawing.Point(523, 404);
            this.selectorNumericoValor.Maximum = new decimal(new int[] {
            1874919423,
            2328306,
            0,
            0});
            this.selectorNumericoValor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.selectorNumericoValor.Name = "selectorNumericoValor";
            this.selectorNumericoValor.Size = new System.Drawing.Size(160, 20);
            this.selectorNumericoValor.TabIndex = 28;
            this.selectorNumericoValor.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // etiquetaValor
            // 
            this.etiquetaValor.AutoSize = true;
            this.etiquetaValor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.etiquetaValor.Location = new System.Drawing.Point(520, 382);
            this.etiquetaValor.Name = "etiquetaValor";
            this.etiquetaValor.Size = new System.Drawing.Size(50, 20);
            this.etiquetaValor.TabIndex = 27;
            this.etiquetaValor.Text = "Valor:";
            // 
            // botonRegistrar
            // 
            this.botonRegistrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botonRegistrar.Location = new System.Drawing.Point(689, 401);
            this.botonRegistrar.Name = "botonRegistrar";
            this.botonRegistrar.Size = new System.Drawing.Size(75, 23);
            this.botonRegistrar.TabIndex = 25;
            this.botonRegistrar.Text = "Registrar";
            this.botonRegistrar.UseVisualStyleBackColor = true;
            this.botonRegistrar.Click += new System.EventHandler(this.botonRegistrar_Click);
            // 
            // etiquetaDispositivos
            // 
            this.etiquetaDispositivos.AutoSize = true;
            this.etiquetaDispositivos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.etiquetaDispositivos.Location = new System.Drawing.Point(3, 9);
            this.etiquetaDispositivos.Name = "etiquetaDispositivos";
            this.etiquetaDispositivos.Size = new System.Drawing.Size(94, 20);
            this.etiquetaDispositivos.TabIndex = 24;
            this.etiquetaDispositivos.Text = "Dispositvos:";
            // 
            // grillaDatosVariablesRegistro
            // 
            this.grillaDatosVariablesRegistro.AllowUserToAddRows = false;
            this.grillaDatosVariablesRegistro.AllowUserToDeleteRows = false;
            this.grillaDatosVariablesRegistro.AllowUserToResizeColumns = false;
            this.grillaDatosVariablesRegistro.AllowUserToResizeRows = false;
            this.grillaDatosVariablesRegistro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grillaDatosVariablesRegistro.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8});
            this.grillaDatosVariablesRegistro.Location = new System.Drawing.Point(6, 274);
            this.grillaDatosVariablesRegistro.MultiSelect = false;
            this.grillaDatosVariablesRegistro.Name = "grillaDatosVariablesRegistro";
            this.grillaDatosVariablesRegistro.Size = new System.Drawing.Size(508, 150);
            this.grillaDatosVariablesRegistro.TabIndex = 23;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "ID";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 40;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Nombre";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 224;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Mínimo";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "Máximo";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // etiquetaVariablesRegistro
            // 
            this.etiquetaVariablesRegistro.AutoSize = true;
            this.etiquetaVariablesRegistro.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.etiquetaVariablesRegistro.Location = new System.Drawing.Point(6, 249);
            this.etiquetaVariablesRegistro.Name = "etiquetaVariablesRegistro";
            this.etiquetaVariablesRegistro.Size = new System.Drawing.Size(79, 20);
            this.etiquetaVariablesRegistro.TabIndex = 22;
            this.etiquetaVariablesRegistro.Text = "Variables:";
            // 
            // grillaDatosDispositivos
            // 
            this.grillaDatosDispositivos.AllowUserToAddRows = false;
            this.grillaDatosDispositivos.AllowUserToDeleteRows = false;
            this.grillaDatosDispositivos.AllowUserToResizeColumns = false;
            this.grillaDatosDispositivos.AllowUserToResizeRows = false;
            this.grillaDatosDispositivos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grillaDatosDispositivos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.TipoDeDispositivoID,
            this.dataGridViewTextBoxColumn11});
            this.grillaDatosDispositivos.Cursor = System.Windows.Forms.Cursors.Default;
            this.grillaDatosDispositivos.Location = new System.Drawing.Point(6, 35);
            this.grillaDatosDispositivos.MultiSelect = false;
            this.grillaDatosDispositivos.Name = "grillaDatosDispositivos";
            this.grillaDatosDispositivos.Size = new System.Drawing.Size(758, 186);
            this.grillaDatosDispositivos.TabIndex = 21;
            this.grillaDatosDispositivos.Click += new System.EventHandler(this.grillaDatosDipositivo_Click);
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "ID";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 40;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "Nombre";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 282;
            // 
            // TipoDeDispositivoID
            // 
            this.TipoDeDispositivoID.HeaderText = "ID";
            this.TipoDeDispositivoID.Name = "TipoDeDispositivoID";
            this.TipoDeDispositivoID.ReadOnly = true;
            this.TipoDeDispositivoID.Width = 40;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "Tipo de Dispositivo";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Width = 352;
            // 
            // pestañaImportador
            // 
            this.pestañaImportador.Controls.Add(this.botonImportar);
            this.pestañaImportador.Controls.Add(this.etiquetaImportadores);
            this.pestañaImportador.Controls.Add(this.grillaDatosImportadores);
            this.pestañaImportador.Location = new System.Drawing.Point(4, 22);
            this.pestañaImportador.Name = "pestañaImportador";
            this.pestañaImportador.Padding = new System.Windows.Forms.Padding(3);
            this.pestañaImportador.Size = new System.Drawing.Size(770, 438);
            this.pestañaImportador.TabIndex = 4;
            this.pestañaImportador.Text = "Importador de Registros";
            this.pestañaImportador.UseVisualStyleBackColor = true;
            // 
            // botonImportar
            // 
            this.botonImportar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botonImportar.Location = new System.Drawing.Point(691, 404);
            this.botonImportar.Name = "botonImportar";
            this.botonImportar.Size = new System.Drawing.Size(75, 23);
            this.botonImportar.TabIndex = 28;
            this.botonImportar.Text = "Importar";
            this.botonImportar.UseVisualStyleBackColor = true;
            this.botonImportar.Click += new System.EventHandler(this.botonImportar_Click);
            // 
            // etiquetaImportadores
            // 
            this.etiquetaImportadores.AutoSize = true;
            this.etiquetaImportadores.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.etiquetaImportadores.Location = new System.Drawing.Point(5, 12);
            this.etiquetaImportadores.Name = "etiquetaImportadores";
            this.etiquetaImportadores.Size = new System.Drawing.Size(108, 20);
            this.etiquetaImportadores.TabIndex = 27;
            this.etiquetaImportadores.Text = "Importadores:";
            // 
            // grillaDatosImportadores
            // 
            this.grillaDatosImportadores.AllowUserToAddRows = false;
            this.grillaDatosImportadores.AllowUserToDeleteRows = false;
            this.grillaDatosImportadores.AllowUserToResizeColumns = false;
            this.grillaDatosImportadores.AllowUserToResizeRows = false;
            this.grillaDatosImportadores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grillaDatosImportadores.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn13});
            this.grillaDatosImportadores.Cursor = System.Windows.Forms.Cursors.Default;
            this.grillaDatosImportadores.Location = new System.Drawing.Point(8, 38);
            this.grillaDatosImportadores.MultiSelect = false;
            this.grillaDatosImportadores.Name = "grillaDatosImportadores";
            this.grillaDatosImportadores.Size = new System.Drawing.Size(758, 346);
            this.grillaDatosImportadores.TabIndex = 26;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "Nombre";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Width = 715;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.botonConsultar);
            this.tabPage1.Controls.Add(this.etiquetaFechaHasta);
            this.tabPage1.Controls.Add(this.etiquetaFechaDesde);
            this.tabPage1.Controls.Add(this.calendarioFechaHasta);
            this.tabPage1.Controls.Add(this.calendarioFechaDesde);
            this.tabPage1.Controls.Add(this.etiquetaLog);
            this.tabPage1.Controls.Add(this.grillaDatosLog);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(770, 438);
            this.tabPage1.TabIndex = 5;
            this.tabPage1.Text = "Consulta de Log";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // botonConsultar
            // 
            this.botonConsultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botonConsultar.Location = new System.Drawing.Point(594, 46);
            this.botonConsultar.Name = "botonConsultar";
            this.botonConsultar.Size = new System.Drawing.Size(75, 23);
            this.botonConsultar.TabIndex = 30;
            this.botonConsultar.Text = "Consultar";
            this.botonConsultar.UseVisualStyleBackColor = true;
            this.botonConsultar.Click += new System.EventHandler(this.botonConsultar_Click);
            // 
            // etiquetaFechaHasta
            // 
            this.etiquetaFechaHasta.AutoSize = true;
            this.etiquetaFechaHasta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.etiquetaFechaHasta.Location = new System.Drawing.Point(336, 23);
            this.etiquetaFechaHasta.Name = "etiquetaFechaHasta";
            this.etiquetaFechaHasta.Size = new System.Drawing.Size(105, 20);
            this.etiquetaFechaHasta.TabIndex = 29;
            this.etiquetaFechaHasta.Text = "Fecha Hasta:";
            // 
            // etiquetaFechaDesde
            // 
            this.etiquetaFechaDesde.AutoSize = true;
            this.etiquetaFechaDesde.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.etiquetaFechaDesde.Location = new System.Drawing.Point(75, 23);
            this.etiquetaFechaDesde.Name = "etiquetaFechaDesde";
            this.etiquetaFechaDesde.Size = new System.Drawing.Size(106, 20);
            this.etiquetaFechaDesde.TabIndex = 28;
            this.etiquetaFechaDesde.Text = "Fecha desde:";
            // 
            // calendarioFechaHasta
            // 
            this.calendarioFechaHasta.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.calendarioFechaHasta.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.calendarioFechaHasta.Location = new System.Drawing.Point(340, 48);
            this.calendarioFechaHasta.MaxDate = new System.DateTime(3000, 12, 31, 0, 0, 0, 0);
            this.calendarioFechaHasta.MinDate = new System.DateTime(1990, 5, 7, 0, 0, 0, 0);
            this.calendarioFechaHasta.Name = "calendarioFechaHasta";
            this.calendarioFechaHasta.Size = new System.Drawing.Size(201, 20);
            this.calendarioFechaHasta.TabIndex = 27;
            this.calendarioFechaHasta.Value = new System.DateTime(2015, 11, 19, 20, 0, 0, 0);
            // 
            // calendarioFechaDesde
            // 
            this.calendarioFechaDesde.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.calendarioFechaDesde.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.calendarioFechaDesde.Location = new System.Drawing.Point(79, 48);
            this.calendarioFechaDesde.MaxDate = new System.DateTime(3000, 12, 31, 0, 0, 0, 0);
            this.calendarioFechaDesde.MinDate = new System.DateTime(1990, 5, 7, 0, 0, 0, 0);
            this.calendarioFechaDesde.Name = "calendarioFechaDesde";
            this.calendarioFechaDesde.Size = new System.Drawing.Size(201, 20);
            this.calendarioFechaDesde.TabIndex = 26;
            this.calendarioFechaDesde.Value = new System.DateTime(2015, 11, 19, 20, 0, 0, 0);
            // 
            // etiquetaLog
            // 
            this.etiquetaLog.AutoSize = true;
            this.etiquetaLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.etiquetaLog.Location = new System.Drawing.Point(6, 94);
            this.etiquetaLog.Name = "etiquetaLog";
            this.etiquetaLog.Size = new System.Drawing.Size(40, 20);
            this.etiquetaLog.TabIndex = 25;
            this.etiquetaLog.Text = "Log:";
            // 
            // grillaDatosLog
            // 
            this.grillaDatosLog.AllowUserToAddRows = false;
            this.grillaDatosLog.AllowUserToDeleteRows = false;
            this.grillaDatosLog.AllowUserToResizeColumns = false;
            this.grillaDatosLog.AllowUserToResizeRows = false;
            this.grillaDatosLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grillaDatosLog.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16});
            this.grillaDatosLog.Cursor = System.Windows.Forms.Cursors.Default;
            this.grillaDatosLog.Location = new System.Drawing.Point(6, 119);
            this.grillaDatosLog.MultiSelect = false;
            this.grillaDatosLog.Name = "grillaDatosLog";
            this.grillaDatosLog.Size = new System.Drawing.Size(758, 304);
            this.grillaDatosLog.TabIndex = 22;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "ID";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Width = 40;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.HeaderText = "Fecha y Hora";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Width = 224;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.HeaderText = "Usuario";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Width = 250;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.HeaderText = "Tipo de Acción";
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.Width = 200;
            // 
            // MenuPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(802, 488);
            this.Controls.Add(this.pestañas);
            this.Name = "MenuPrincipal";
            this.Text = "MenuPrincipal";
            this.pestañas.ResumeLayout(false);
            this.pestañaVariables.ResumeLayout(false);
            this.pestañaVariables.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grillaDatosVariables)).EndInit();
            this.pestañaTiposDeDispositivo.ResumeLayout(false);
            this.pestañaTiposDeDispositivo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grillaDatosVariablesTipoDeDispositivo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grillaDatosTiposDeDispositivos)).EndInit();
            this.pestañaRegistroDeVariable.ResumeLayout(false);
            this.pestañaRegistroDeVariable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.selectorNumericoValor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grillaDatosVariablesRegistro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grillaDatosDispositivos)).EndInit();
            this.pestañaImportador.ResumeLayout(false);
            this.pestañaImportador.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grillaDatosImportadores)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grillaDatosLog)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl pestañas;
        private System.Windows.Forms.TabPage pestañaVariables;
        private System.Windows.Forms.Button botonEliminarVariable;
        private System.Windows.Forms.Button BotonModificarVariable;
        private System.Windows.Forms.TabPage pestañaTiposDeDispositivo;
        private System.Windows.Forms.Label etiquetaTiposDeDispositivos;
        private System.Windows.Forms.DataGridView grillaDatosVariablesTipoDeDispositivo;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.Label etiquetaVariablesTiposDeDispositivo;
        private System.Windows.Forms.Button botonEliminarTipoDeDispositivo;
        private System.Windows.Forms.Button botonModificarTipoDeDispositivo;
        private System.Windows.Forms.DataGridView grillaDatosTiposDeDispositivos;
        private System.Windows.Forms.DataGridView grillaDatosVariables;
        private System.Windows.Forms.Label etiquetaVariables;
        private System.Windows.Forms.Button botonNuevaVariale;
        private System.Windows.Forms.Button botonNuevoTipoDeDispositivo;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnaTipoID;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnaTipoNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnaDescripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnaID;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnaNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnaMinimo;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnaMaximo;
        private System.Windows.Forms.TabPage pestañaRegistroDeVariable;
        private System.Windows.Forms.Label etiquetaDispositivos;
        private System.Windows.Forms.DataGridView grillaDatosVariablesRegistro;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.Label etiquetaVariablesRegistro;
        private System.Windows.Forms.DataGridView grillaDatosDispositivos;
        private System.Windows.Forms.Label etiquetaValor;
        private System.Windows.Forms.Button botonRegistrar;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoDeDispositivoID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.TabPage pestañaImportador;
        private System.Windows.Forms.Button botonImportar;
        private System.Windows.Forms.Label etiquetaImportadores;
        private System.Windows.Forms.DataGridView grillaDatosImportadores;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.NumericUpDown selectorNumericoValor;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button botonConsultar;
        private System.Windows.Forms.Label etiquetaFechaHasta;
        private System.Windows.Forms.Label etiquetaFechaDesde;
        private System.Windows.Forms.DateTimePicker calendarioFechaHasta;
        private System.Windows.Forms.DateTimePicker calendarioFechaDesde;
        private System.Windows.Forms.Label etiquetaLog;
        private System.Windows.Forms.DataGridView grillaDatosLog;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
    }
}