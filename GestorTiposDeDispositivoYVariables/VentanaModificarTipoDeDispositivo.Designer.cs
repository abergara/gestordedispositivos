﻿namespace GestorTiposDeDispositivoYVariables
{
    partial class VentanaModificarTipoDeDispositivo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.botonModificar = new System.Windows.Forms.Button();
            this.etiquetaModificarTipoDeDispositivo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // botonModificar
            // 
            this.botonModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botonModificar.Location = new System.Drawing.Point(495, 386);
            this.botonModificar.Name = "botonModificar";
            this.botonModificar.Size = new System.Drawing.Size(75, 23);
            this.botonModificar.TabIndex = 18;
            this.botonModificar.Text = "Modificar";
            this.botonModificar.UseVisualStyleBackColor = true;
            this.botonModificar.Click += new System.EventHandler(this.botonModificar_Click);
            // 
            // etiquetaModificarTipoDeDispositivo
            // 
            this.etiquetaModificarTipoDeDispositivo.AutoSize = true;
            this.etiquetaModificarTipoDeDispositivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.etiquetaModificarTipoDeDispositivo.Location = new System.Drawing.Point(9, 9);
            this.etiquetaModificarTipoDeDispositivo.Name = "etiquetaModificarTipoDeDispositivo";
            this.etiquetaModificarTipoDeDispositivo.Size = new System.Drawing.Size(213, 20);
            this.etiquetaModificarTipoDeDispositivo.TabIndex = 26;
            this.etiquetaModificarTipoDeDispositivo.Text = "Modificar Tipo de Dispositivo:";
            // 
            // VentanaModificarTipoDeDispositivo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(587, 421);
            this.Controls.Add(this.etiquetaModificarTipoDeDispositivo);
            this.Controls.Add(this.botonModificar);
            this.Name = "VentanaModificarTipoDeDispositivo";
            this.Text = "Modificar Tipo de Dispositivo";
            this.Controls.SetChildIndex(this.botonModificar, 0);
            this.Controls.SetChildIndex(this.etiquetaModificarTipoDeDispositivo, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button botonModificar;
        private System.Windows.Forms.Label etiquetaModificarTipoDeDispositivo;
    }
}