﻿namespace GestorTiposDeDispositivoYVariables
{
    partial class VentanaGestionVariables
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.selectorNumericoMinimo = new System.Windows.Forms.NumericUpDown();
            this.selectorNumericoMaximo = new System.Windows.Forms.NumericUpDown();
            this.etiquetaMaximo = new System.Windows.Forms.Label();
            this.etiquetaMinimo = new System.Windows.Forms.Label();
            this.etiquetaNombre = new System.Windows.Forms.Label();
            this.cajaTextoNombre = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.selectorNumericoMinimo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectorNumericoMaximo)).BeginInit();
            this.SuspendLayout();
            // 
            // selectorNumericoMinimo
            // 
            this.selectorNumericoMinimo.DecimalPlaces = 2;
            this.selectorNumericoMinimo.Location = new System.Drawing.Point(87, 78);
            this.selectorNumericoMinimo.Maximum = new decimal(new int[] {
            1874919423,
            2328306,
            0,
            0});
            this.selectorNumericoMinimo.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.selectorNumericoMinimo.Name = "selectorNumericoMinimo";
            this.selectorNumericoMinimo.Size = new System.Drawing.Size(91, 20);
            this.selectorNumericoMinimo.TabIndex = 19;
            this.selectorNumericoMinimo.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.selectorNumericoMinimo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.selectorNumericoMinimo_KeyPress);
            this.selectorNumericoMinimo.Leave += new System.EventHandler(this.selectorNumericoMinimo_Leave);
            // 
            // selectorNumericoMaximo
            // 
            this.selectorNumericoMaximo.DecimalPlaces = 2;
            this.selectorNumericoMaximo.Location = new System.Drawing.Point(87, 109);
            this.selectorNumericoMaximo.Maximum = new decimal(new int[] {
            1874919423,
            2328306,
            0,
            0});
            this.selectorNumericoMaximo.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.selectorNumericoMaximo.Name = "selectorNumericoMaximo";
            this.selectorNumericoMaximo.Size = new System.Drawing.Size(91, 20);
            this.selectorNumericoMaximo.TabIndex = 18;
            this.selectorNumericoMaximo.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.selectorNumericoMaximo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.selectorNumericoMaximo_KeyPress);
            this.selectorNumericoMaximo.Leave += new System.EventHandler(this.selectorNumericoMaximo_Leave);
            // 
            // etiquetaMaximo
            // 
            this.etiquetaMaximo.AutoSize = true;
            this.etiquetaMaximo.Location = new System.Drawing.Point(15, 111);
            this.etiquetaMaximo.Name = "etiquetaMaximo";
            this.etiquetaMaximo.Size = new System.Drawing.Size(46, 13);
            this.etiquetaMaximo.TabIndex = 17;
            this.etiquetaMaximo.Text = "Máximo:";
            // 
            // etiquetaMinimo
            // 
            this.etiquetaMinimo.AutoSize = true;
            this.etiquetaMinimo.Location = new System.Drawing.Point(15, 80);
            this.etiquetaMinimo.Name = "etiquetaMinimo";
            this.etiquetaMinimo.Size = new System.Drawing.Size(45, 13);
            this.etiquetaMinimo.TabIndex = 16;
            this.etiquetaMinimo.Text = "Mínimo:";
            // 
            // etiquetaNombre
            // 
            this.etiquetaNombre.AutoSize = true;
            this.etiquetaNombre.Location = new System.Drawing.Point(15, 49);
            this.etiquetaNombre.Name = "etiquetaNombre";
            this.etiquetaNombre.Size = new System.Drawing.Size(47, 13);
            this.etiquetaNombre.TabIndex = 15;
            this.etiquetaNombre.Text = "Nombre:";
            // 
            // cajaTextoNombre
            // 
            this.cajaTextoNombre.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cajaTextoNombre.Location = new System.Drawing.Point(87, 46);
            this.cajaTextoNombre.Name = "cajaTextoNombre";
            this.cajaTextoNombre.Size = new System.Drawing.Size(182, 20);
            this.cajaTextoNombre.TabIndex = 14;
            // 
            // VentanaGestionVariables
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.selectorNumericoMinimo);
            this.Controls.Add(this.selectorNumericoMaximo);
            this.Controls.Add(this.etiquetaMaximo);
            this.Controls.Add(this.etiquetaMinimo);
            this.Controls.Add(this.etiquetaNombre);
            this.Controls.Add(this.cajaTextoNombre);
            this.Name = "VentanaGestionVariables";
            this.Text = "VentanaGestionVariablecs";
            ((System.ComponentModel.ISupportInitialize)(this.selectorNumericoMinimo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectorNumericoMaximo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown selectorNumericoMinimo;
        private System.Windows.Forms.NumericUpDown selectorNumericoMaximo;
        private System.Windows.Forms.Label etiquetaMaximo;
        private System.Windows.Forms.Label etiquetaMinimo;
        private System.Windows.Forms.Label etiquetaNombre;
        private System.Windows.Forms.TextBox cajaTextoNombre;
    }
}