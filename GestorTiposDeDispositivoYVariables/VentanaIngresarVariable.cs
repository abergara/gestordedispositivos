﻿using DominioGestorDispositivos;
using PersistenciaGestorDispositivos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestorTiposDeDispositivoYVariables
{
    public partial class VentanaIngresarVariable : VentanaGestionVariables
    {
        private ManejadorVariable unManejadorVariable;
        private MenuPrincipal menuPrincipal;
        
        public VentanaIngresarVariable(MenuPrincipal unMenuPrincipal)
        {
            InitializeComponent();
            unManejadorVariable = new ManejadorVariable();
            menuPrincipal = unMenuPrincipal;
        }

        private void botonGuardar_Click(object sender, EventArgs e)
        {
            if (DatosCorrectosParaCrearVariable())
            {
                Variable variableAAgregar = CrearVariable();
                AgregarVariable(variableAAgregar);
            }
        }

        private void AgregarVariable(Variable variableAAgregar)
        {
            try
            {
                unManejadorVariable.AgregarVariable(variableAAgregar);
                MessageBox.Show("Variable ingresada correctamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                menuPrincipal.RefrescarGrillaDatosVariables();
                this.Dispose();
            }
            catch (ArgumentException e)
            {
                MessageBox.Show(e.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
