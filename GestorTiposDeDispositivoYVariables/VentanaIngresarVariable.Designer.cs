﻿namespace GestorTiposDeDispositivoYVariables
{
    partial class VentanaIngresarVariable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.botonGuardar = new System.Windows.Forms.Button();
            this.etiquetaNuevaVariable = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // botonGuardar
            // 
            this.botonGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botonGuardar.Location = new System.Drawing.Point(197, 208);
            this.botonGuardar.Name = "botonGuardar";
            this.botonGuardar.Size = new System.Drawing.Size(75, 23);
            this.botonGuardar.TabIndex = 10;
            this.botonGuardar.Text = "Guardar";
            this.botonGuardar.UseVisualStyleBackColor = true;
            this.botonGuardar.Click += new System.EventHandler(this.botonGuardar_Click);
            // 
            // etiquetaNuevaVariable
            // 
            this.etiquetaNuevaVariable.AutoSize = true;
            this.etiquetaNuevaVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.etiquetaNuevaVariable.Location = new System.Drawing.Point(14, 9);
            this.etiquetaNuevaVariable.Name = "etiquetaNuevaVariable";
            this.etiquetaNuevaVariable.Size = new System.Drawing.Size(120, 20);
            this.etiquetaNuevaVariable.TabIndex = 26;
            this.etiquetaNuevaVariable.Text = "Nueva Variable:";
            // 
            // VentanaIngresarVariable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 257);
            this.Controls.Add(this.etiquetaNuevaVariable);
            this.Controls.Add(this.botonGuardar);
            this.Name = "VentanaIngresarVariable";
            this.Text = "Nueva Variable";
            this.Controls.SetChildIndex(this.botonGuardar, 0);
            this.Controls.SetChildIndex(this.etiquetaNuevaVariable, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button botonGuardar;
        private System.Windows.Forms.Label etiquetaNuevaVariable;

    }
}