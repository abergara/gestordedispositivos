﻿using DominioGestorDispositivos;
using PersistenciaGestorDispositivos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestorTiposDeDispositivoYVariables
{
    public partial class VentanaGestionTipoDeDispositivo : Form
    {
        private RestriccionesDeInterfaz restricciones;
        private ManejadorVariable unManejadorVariable;
        
        public VentanaGestionTipoDeDispositivo()
        {
            InitializeComponent();
            restricciones = new RestriccionesDeInterfaz();
            unManejadorVariable = new ManejadorVariable();
        }

        public void CargarVariablesEnGrillaDatosVariables()
        {
            List<Variable> variables = new List<Variable>();
            try
            {
                variables = unManejadorVariable.ObtenerTodasLasVariablesQueNoEstanAsociadasAAlgunTipoDeDispositivo();
            }
            catch (ArgumentException e)
            {
                this.Dispose();
                throw e;
            }
            foreach (var variable in variables)
            {
                AgregarFilaAGrillaVariables(variable, false);
            }
        }

        public void CargarVariablesEnGrillaDatosVariablesModificar()
        {
            List<Variable> variables = new List<Variable>();
            try
            {
                variables = unManejadorVariable.ObtenerTodasLasVariablesQueNoEstanAsociadasAAlgunTipoDeDispositivo();
            }
            catch (ArgumentException e)
            {
                MessageBox.Show(e.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            foreach (var variable in variables)
            {
                AgregarFilaAGrillaVariables(variable, false);
            }
        }

        public void AgregarFilaAGrillaVariables(Variable unaVariable, Boolean variableSeleccionada)
        {
            grillaDatosVariables.Rows.Add(variableSeleccionada, unaVariable.VariableId, unaVariable.Nombre, unaVariable.Minimo, unaVariable.Maximo);
        }

        public Boolean DatosCorrectosParaCrearTipoDeDispositivo()
        {
            return !NombreTipoDeDispositivoVacio() && !DescripcionTipoDeDispositivoVacio();
        }

        private Boolean NombreTipoDeDispositivoVacio()
        {
            Boolean nombreNoVacio = false;
            if (restricciones.EstaVaciaCajaTexto(cajaTextoNombre))
            {
                MessageBox.Show("El nombre no puede ser vacío", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                nombreNoVacio = true;
            }
            return nombreNoVacio;
        }

        private Boolean DescripcionTipoDeDispositivoVacio()
        {
            Boolean descripcionNoVacia = false;
            if (restricciones.EstaVaciaCajaTexto(cajaTextoDescripcion))
            {
                MessageBox.Show("La descripción no puede ser vacía", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                descripcionNoVacia = true;
            }
            return descripcionNoVacia;
        }

        public TipoDeDispositivo CrearTipoDeDispositivo()
        {
            string nombre = cajaTextoNombre.Text;
            string descripcion = cajaTextoDescripcion.Text;
            List<Variable> variables = CrearVariables();
            TipoDeDispositivo unTipoDeDispositivo = new TipoDeDispositivo(nombre, descripcion, variables);
            return unTipoDeDispositivo;
        }

        private List<Variable> CrearVariables()
        {
            List<Variable> variables = new List<Variable>();
            Variable unaVariable;
            foreach (DataGridViewRow fila in grillaDatosVariables.Rows)
            {
                if ((bool)fila.Cells[0].Value)
                {
                    unaVariable = CrearVariable(fila);
                    variables.Add(unaVariable);
                }
            }
            return variables;
        }

        private Variable CrearVariable(DataGridViewRow fila)
        {
            int id = Int32.Parse(fila.Cells[1].Value.ToString());
            string nombre = fila.Cells[2].Value.ToString();
            double minimo = double.Parse(fila.Cells[3].Value.ToString());
            double maximo = double.Parse(fila.Cells[4].Value.ToString());
            Variable unaVariable = new Variable(nombre, minimo, maximo);
            unaVariable.VariableId = id;
            return unaVariable;
        }

        public void ModificarTextoCajaTextoNombre(string nombre)
        {
            cajaTextoNombre.Text = nombre;
        }

        public void ModificarTextoCajaTextoDescripcion(string descripcion)
        {
            cajaTextoDescripcion.Text = descripcion;
        }
    }
}
