﻿namespace GestorTiposDeDispositivoYVariables
{
    partial class VentanaGestionTipoDeDispositivo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.etiquetaVariables = new System.Windows.Forms.Label();
            this.grillaDatosVariables = new System.Windows.Forms.DataGridView();
            this.columnaAgregar = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.columnaID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnaNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnaMinimo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnaMaximo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cajaTextoDescripcion = new System.Windows.Forms.TextBox();
            this.etiquetaDescripcion = new System.Windows.Forms.Label();
            this.etiquetaNombre = new System.Windows.Forms.Label();
            this.cajaTextoNombre = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.grillaDatosVariables)).BeginInit();
            this.SuspendLayout();
            // 
            // etiquetaVariables
            // 
            this.etiquetaVariables.AutoSize = true;
            this.etiquetaVariables.Location = new System.Drawing.Point(10, 99);
            this.etiquetaVariables.Name = "etiquetaVariables";
            this.etiquetaVariables.Size = new System.Drawing.Size(53, 13);
            this.etiquetaVariables.TabIndex = 17;
            this.etiquetaVariables.Text = "Variables:";
            // 
            // grillaDatosVariables
            // 
            this.grillaDatosVariables.AllowUserToAddRows = false;
            this.grillaDatosVariables.AllowUserToDeleteRows = false;
            this.grillaDatosVariables.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grillaDatosVariables.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnaAgregar,
            this.columnaID,
            this.columnaNombre,
            this.columnaMinimo,
            this.columnaMaximo});
            this.grillaDatosVariables.Location = new System.Drawing.Point(13, 125);
            this.grillaDatosVariables.Name = "grillaDatosVariables";
            this.grillaDatosVariables.Size = new System.Drawing.Size(557, 236);
            this.grillaDatosVariables.TabIndex = 16;
            // 
            // columnaAgregar
            // 
            this.columnaAgregar.HeaderText = "Agregar";
            this.columnaAgregar.Name = "columnaAgregar";
            // 
            // columnaID
            // 
            this.columnaID.HeaderText = "ID";
            this.columnaID.Name = "columnaID";
            this.columnaID.ReadOnly = true;
            this.columnaID.Width = 40;
            // 
            // columnaNombre
            // 
            this.columnaNombre.HeaderText = "Nombre";
            this.columnaNombre.Name = "columnaNombre";
            this.columnaNombre.ReadOnly = true;
            this.columnaNombre.Width = 224;
            // 
            // columnaMinimo
            // 
            this.columnaMinimo.HeaderText = "Mínimo";
            this.columnaMinimo.Name = "columnaMinimo";
            this.columnaMinimo.ReadOnly = true;
            this.columnaMinimo.Width = 75;
            // 
            // columnaMaximo
            // 
            this.columnaMaximo.HeaderText = "Máximo";
            this.columnaMaximo.Name = "columnaMaximo";
            this.columnaMaximo.ReadOnly = true;
            this.columnaMaximo.Width = 75;
            // 
            // cajaTextoDescripcion
            // 
            this.cajaTextoDescripcion.Location = new System.Drawing.Point(82, 66);
            this.cajaTextoDescripcion.Name = "cajaTextoDescripcion";
            this.cajaTextoDescripcion.Size = new System.Drawing.Size(488, 20);
            this.cajaTextoDescripcion.TabIndex = 15;
            // 
            // etiquetaDescripcion
            // 
            this.etiquetaDescripcion.AutoSize = true;
            this.etiquetaDescripcion.Location = new System.Drawing.Point(10, 69);
            this.etiquetaDescripcion.Name = "etiquetaDescripcion";
            this.etiquetaDescripcion.Size = new System.Drawing.Size(66, 13);
            this.etiquetaDescripcion.TabIndex = 14;
            this.etiquetaDescripcion.Text = "Descripción:";
            // 
            // etiquetaNombre
            // 
            this.etiquetaNombre.AutoSize = true;
            this.etiquetaNombre.Location = new System.Drawing.Point(10, 38);
            this.etiquetaNombre.Name = "etiquetaNombre";
            this.etiquetaNombre.Size = new System.Drawing.Size(47, 13);
            this.etiquetaNombre.TabIndex = 13;
            this.etiquetaNombre.Text = "Nombre:";
            // 
            // cajaTextoNombre
            // 
            this.cajaTextoNombre.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cajaTextoNombre.Location = new System.Drawing.Point(82, 35);
            this.cajaTextoNombre.Name = "cajaTextoNombre";
            this.cajaTextoNombre.Size = new System.Drawing.Size(488, 20);
            this.cajaTextoNombre.TabIndex = 12;
            // 
            // VentanaGestionTipoDeDispositivo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(581, 390);
            this.Controls.Add(this.etiquetaVariables);
            this.Controls.Add(this.grillaDatosVariables);
            this.Controls.Add(this.cajaTextoDescripcion);
            this.Controls.Add(this.etiquetaDescripcion);
            this.Controls.Add(this.etiquetaNombre);
            this.Controls.Add(this.cajaTextoNombre);
            this.Name = "VentanaGestionTipoDeDispositivo";
            this.Text = "VentanaGestionTipoDeDispositivo";
            ((System.ComponentModel.ISupportInitialize)(this.grillaDatosVariables)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label etiquetaVariables;
        private System.Windows.Forms.DataGridView grillaDatosVariables;
        private System.Windows.Forms.DataGridViewCheckBoxColumn columnaAgregar;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnaID;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnaNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnaMinimo;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnaMaximo;
        private System.Windows.Forms.TextBox cajaTextoDescripcion;
        private System.Windows.Forms.Label etiquetaDescripcion;
        private System.Windows.Forms.Label etiquetaNombre;
        private System.Windows.Forms.TextBox cajaTextoNombre;
    }
}