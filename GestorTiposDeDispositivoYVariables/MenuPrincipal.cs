﻿using ClasesDominioLogGestorDeDispositivos;
using DominioGestorDispositivos;
using ImportadorDeRegistros;
using PersistenciaClasesDominioLog;
using PersistenciaGestorDispositivos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestorTiposDeDispositivoYVariables
{
    public partial class MenuPrincipal : Form
    {
        private RestriccionesDeInterfaz restricciones;
        private ManejadorVariable unManejadorVariable;
        private ManejadorDispositivo unManejadorDispositivo;
        private ManejadorTipoDeDispositivo unManejadorTipoDeDispositivo;
        private ManejadorRegistrosDeVariable unManejadorRegistrosDeVariable;
        private List<IImportador> importadores;

        public MenuPrincipal()
        {
            InitializeComponent();
            restricciones = new RestriccionesDeInterfaz();
            unManejadorVariable = new ManejadorVariable();
            unManejadorDispositivo = new ManejadorDispositivo();
            unManejadorTipoDeDispositivo = new ManejadorTipoDeDispositivo();
            unManejadorRegistrosDeVariable = new ManejadorRegistrosDeVariable();
            CargarVariablesEnGrillaDatosVariables();
            CargarTiposDeDispositivoEnGrillasDatosTiposDeDispositivo();
            CargarDispositivosEnGrillaDatosDispositivo();
            CargarImportadoresEnGrillaDatosImportadores();
        }

        private void CargarVariablesEnGrillaDatosVariables()
        {
            List<Variable> variables = new List<Variable>();
            try
            {
                variables = unManejadorVariable.ObtenerTodasLasVariables();
            }
            catch (ArgumentException e)
            {
                MessageBox.Show(e.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            CrearFilasGrilla(variables, grillaDatosVariables);
        }

        private void CrearFilasGrilla(List<Variable> variables, DataGridView grilla)
        {
            grilla.Rows.Clear();
            foreach (var variable in variables)
            {
                grilla.Rows.Add(variable.VariableId, variable.Nombre, variable.Minimo, variable.Maximo);
            }
        }

        private void CargarTiposDeDispositivoEnGrillasDatosTiposDeDispositivo()
        {
            List<TipoDeDispositivo> tiposDeDispositivos = new List<TipoDeDispositivo>();
            try
            {
                tiposDeDispositivos = unManejadorTipoDeDispositivo.ObtenerTodosLosTiposDeDispositivos();
            }
            catch (ArgumentException e)
            {
                MessageBox.Show(e.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            foreach (var tipoDeDispositivo in tiposDeDispositivos)
            {
                grillaDatosTiposDeDispositivos.Rows.Add(tipoDeDispositivo.TipoDeDispositivoId, tipoDeDispositivo.Nombre, tipoDeDispositivo.Descripcion);
            }
        }

        private void CargarDispositivosEnGrillaDatosDispositivo()
        {
            List<Dispositivo> dispositivos = new List<Dispositivo>();
            try
            {
                dispositivos = unManejadorDispositivo.ObtenerTodosLosDispositivos();
            }
            catch (ArgumentException e)
            {
                MessageBox.Show(e.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            foreach (var dispositivo in dispositivos)
            {
                grillaDatosDispositivos.Rows.Add(dispositivo.DispositivoId, dispositivo.Nombre,dispositivo.TipoDeDispositivo.TipoDeDispositivoId, dispositivo.TipoDeDispositivo.Nombre);
            }
        }

        private void botonNuevoTipoDeDispositivo_Click(object sender, EventArgs e)
        {
            try
            {
                VentanaIngresarTipoDeDispositivo ventanaIngresarTipoDeDispositivo = new VentanaIngresarTipoDeDispositivo(this);
                ventanaIngresarTipoDeDispositivo.Show();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (ObjectDisposedException)
            {
                MessageBox.Show("No existen variables ingresadas", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void botonModificarTipoDeDispositivo_Click(object sender, EventArgs e)
        {
            try
            {
                TipoDeDispositivo tipoDeDispositivoSeleccionado = ObtenerTipoDeDispositivoSeleccionado();
                TipoDeDispositivo tipoDeDispositivoAModificar = unManejadorTipoDeDispositivo.ObtenerTipoDeDispositivo(tipoDeDispositivoSeleccionado);
                VerificarQueSePuedeModificarTipoDeDispositivo(tipoDeDispositivoAModificar);
                VentanaModificarTipoDeDispositivo ventanaModificarTipoDeDispositivo = new VentanaModificarTipoDeDispositivo(this, tipoDeDispositivoAModificar);
                ventanaModificarTipoDeDispositivo.Show();
            }
            catch (ArgumentException excepcion)
            {
                MessageBox.Show(excepcion.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (ObjectDisposedException)
            {
                MessageBox.Show("No existen variables ingresadas", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void VerificarQueSePuedeModificarTipoDeDispositivo(TipoDeDispositivo tipoDeDispositivoAModificar)
        {
            Boolean tipoDeDispositivoAsociadoAAlgunDispositivo = unManejadorTipoDeDispositivo.TipoDeDispositivoEstaAsociadoAAlgunDispositivo(tipoDeDispositivoAModificar);
            if (tipoDeDispositivoAsociadoAAlgunDispositivo)
            {
                throw new System.ArgumentException("No se puede modificar un tipo de dispositivo asociado a un dispositivo");
            }
        }

        private void botonNuevaVariale_Click(object sender, EventArgs e)
        {
            VentanaIngresarVariable ventanaIngresarVariable = new VentanaIngresarVariable(this);
            ventanaIngresarVariable.Show();
        }

        private void BotonModificarVariable_Click(object sender, EventArgs e)
        {
            try
            {
                Variable variableAModificar = ObtenerVariableSeleccionada(grillaDatosVariables);
                VarificarQueSePuedeModificarVariable(variableAModificar);
                VentanaModificarVariable ventanaModificarVariable = new VentanaModificarVariable(this, variableAModificar);
                ventanaModificarVariable.Show();
            }
            catch (ArgumentException excepcion)
            {
                MessageBox.Show(excepcion.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void VarificarQueSePuedeModificarVariable(Variable variableAModificar)
        {
            Boolean variableAsociadaAAlgunTipo = unManejadorVariable.VariableEstaAsociadaAAlgunTipoDeDispositivo(variableAModificar);
            if (variableAsociadaAAlgunTipo)
            {
                throw new System.ArgumentException("No se puede modificar una variable asociada a un tipo de dispositivo");
            }
        }

        private void botonEliminarVariable_Click(object sender, EventArgs e)
        {
            try
            {
                Variable variableAEliminar = ObtenerVariableSeleccionada(grillaDatosVariables);
                unManejadorVariable.EliminarVariable(variableAEliminar);
                MessageBox.Show("Variable eliminada correctamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                RefrescarGrillaDatosVariables();
            }
            catch (ArgumentException excepcion)
            {
                MessageBox.Show(excepcion.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void RefrescarGrillaDatosVariables()
        {
            grillaDatosVariables.Rows.Clear();
            CargarVariablesEnGrillaDatosVariables();
        }

        private Variable ObtenerVariableSeleccionada(DataGridView grillaVariables)
        {
            DataGridViewRow filaSeleccionada = grillaVariables.CurrentRow;
            int variableIdSeleccionada = (int)filaSeleccionada.Cells[0].Value;
            string nombre = filaSeleccionada.Cells[1].Value.ToString();
            double minimo = (double)filaSeleccionada.Cells[2].Value;
            double maximo = (double)filaSeleccionada.Cells[3].Value;
            Variable variableSeleccionada = new Variable(nombre, minimo, maximo);
            variableSeleccionada.VariableId = variableIdSeleccionada;
            return variableSeleccionada;
        }
       
        private void grillaDatosTiposDeDispositivos_Click(object sender, EventArgs e)
        {
            TipoDeDispositivo unTipoDeDispositivo = ObtenerTipoDeDispositivoSeleccionado();
            CargarGrillaDatosVariablesDeTipoDeDispositivo(unTipoDeDispositivo, grillaDatosVariablesTipoDeDispositivo);
        }

        private void grillaDatosDipositivo_Click(object sender, EventArgs e)
        {
            Dispositivo unDispositivo = ObtenerDispositivoSeleccionado();
            TipoDeDispositivo unTipoDeDispositivo = unDispositivo.TipoDeDispositivo;
            CargarGrillaDatosVariablesDeTipoDeDispositivo(unTipoDeDispositivo, grillaDatosVariablesRegistro);
        }

        private Dispositivo ObtenerDispositivoSeleccionado()
        {
            Dispositivo dispositivoSeleccionado = new Dispositivo(); 
            if (grillaDatosDispositivos.RowCount != 0)
            {
                DataGridViewRow filaSeleccionada = grillaDatosDispositivos.CurrentRow;
                int dispositivoIdSeleccionado = (int)filaSeleccionada.Cells[0].Value;
                string nombre = filaSeleccionada.Cells[1].Value.ToString();
                int tipoDeDispositivoId = (int)filaSeleccionada.Cells[2].Value;
                string nombreTipoDeDispositivo = filaSeleccionada.Cells[3].Value.ToString();
                TipoDeDispositivo tipoDeDispositivoDelDispositivo = new TipoDeDispositivo();
                tipoDeDispositivoDelDispositivo.TipoDeDispositivoId = tipoDeDispositivoId;
                tipoDeDispositivoDelDispositivo.Nombre = nombreTipoDeDispositivo;
                dispositivoSeleccionado = new Dispositivo(nombre, tipoDeDispositivoDelDispositivo);
                dispositivoSeleccionado.DispositivoId = dispositivoIdSeleccionado;
            }
            return dispositivoSeleccionado;
        }

        private TipoDeDispositivo ObtenerTipoDeDispositivoSeleccionado()
        {
            TipoDeDispositivo tipoDeDispositivoSeleccionado = new TipoDeDispositivo();
            if(grillaDatosTiposDeDispositivos.RowCount != 0)
            {
                DataGridViewRow filaSeleccionada = grillaDatosTiposDeDispositivos.CurrentRow;
                int tipoDeDispositivoIdSeleccionado = (int)filaSeleccionada.Cells[0].Value;
                string nombre = filaSeleccionada.Cells[1].Value.ToString();
                tipoDeDispositivoSeleccionado = new TipoDeDispositivo();
                tipoDeDispositivoSeleccionado.TipoDeDispositivoId = tipoDeDispositivoIdSeleccionado;
                tipoDeDispositivoSeleccionado.Nombre = nombre;
            }
            return tipoDeDispositivoSeleccionado;
        }

        private void CargarGrillaDatosVariablesDeTipoDeDispositivo(TipoDeDispositivo unTipoDeDispositivo, DataGridView grillaVariables)
        {
            try
            {
                TipoDeDispositivo tipoDeDispositivoObtenido = unManejadorTipoDeDispositivo.ObtenerTipoDeDispositivo(unTipoDeDispositivo);
                CrearFilasGrilla(tipoDeDispositivoObtenido.Variables, grillaVariables);
            }
            catch (ArgumentException excepcion)
            {
                MessageBox.Show(excepcion.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void botonEliminarTipoDeDispositivo_Click(object sender, EventArgs e)
        {
            try
            {
                TipoDeDispositivo tipoDeDispositivoAEliminar = ObtenerTipoDeDispositivoSeleccionado();
                unManejadorTipoDeDispositivo.EliminarTipoDeDispositivo(tipoDeDispositivoAEliminar);
                MessageBox.Show("Tipo de dispositivo eliminado correctamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                RefrescarGrillaDatosTiposDeDispositivo();
            }
            catch (ArgumentException excepcion)
            {
                MessageBox.Show(excepcion.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void RefrescarGrillaDatosTiposDeDispositivo()
        {
            grillaDatosTiposDeDispositivos.Rows.Clear();
            grillaDatosVariablesTipoDeDispositivo.Rows.Clear();
            grillaDatosVariablesRegistro.Rows.Clear();
            CargarTiposDeDispositivoEnGrillasDatosTiposDeDispositivo();
        }

        private void botonRegistrar_Click(object sender, EventArgs e)
        {
            if (DatosCorrectosParaCrearRegistroDeVariable())
            {
                RegistroDeVariable unRegistroDeVariable = CrearRegistroDeVariable();
                try
                {
                    unManejadorRegistrosDeVariable.AgregarRegistroDeVariable(unRegistroDeVariable);
                    MessageBox.Show("Registro ingresado correctamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (ArgumentException excepcion)
                {
                    MessageBox.Show(excepcion.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        private Boolean DatosCorrectosParaCrearRegistroDeVariable()
        {
            return HayVariableEnGrillaVariables() && HayDispositivoEnGrillaDispositivos(); 
        }

        private Boolean HayVariableEnGrillaVariables()
        {
            Boolean hayVariable = true;
            if (grillaDatosVariablesRegistro.RowCount == 0)
            {
                MessageBox.Show("Debe seleccionarse una variable", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                hayVariable = false;
            }
            return hayVariable;
        }

        private Boolean HayDispositivoEnGrillaDispositivos()
        {
            Boolean hayDispositivo = true;
            if (grillaDatosDispositivos.RowCount == 0)
            {
                MessageBox.Show("Debe seleccionarse un dispositivo", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                hayDispositivo = false;
            }
            return hayDispositivo;
        }

        private RegistroDeVariable CrearRegistroDeVariable()
        {
            Dispositivo dispositivoSeleccionado = ObtenerDispositivoSeleccionado();
            Variable variableSeleccionada = ObtenerVariableSeleccionada(grillaDatosVariablesRegistro);
            double valor = double.Parse(selectorNumericoValor.Text);
            DateTime fechaYHoraActual = DateTime.Now;
            RegistroDeVariable unRegistroDeVariable = new RegistroDeVariable(dispositivoSeleccionado.DispositivoId, variableSeleccionada.VariableId, valor, fechaYHoraActual);
            return unRegistroDeVariable;
        }

        private void selectorNumericoMinimo_Leave(object sender, EventArgs e)
        {
            restricciones.NoPermitirSelectorVacio(selectorNumericoValor);
        }

        private void selectorNumericoMinimo_KeyPress(object sender, KeyPressEventArgs e)
        {
            restricciones.NoPermitirSelectorNumericoPasarseDeLargo(15, selectorNumericoValor, e);
        }

        private void CargarImportadoresEnGrillaDatosImportadores()
        {
            CargarImportadores();
            if (importadores != null)
            {
                foreach (var importador in importadores)
                {
                    grillaDatosImportadores.Rows.Add(importador.ObtenerNombre());
                }    
            }
        }

        private void CargarImportadores()
        {
            GestorDeImportadores unGestor = new GestorDeImportadores();
            try
            {
                importadores = unGestor.ObtenerImportadores();
            }
            catch (Exception excepcion)
            {
                MessageBox.Show(excepcion.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }

        private void botonImportar_Click(object sender, EventArgs e)
        {
            if(importadores.Count > 0)
            {
                RealizarImportacion();
            }
        }

        private void RealizarImportacion()
        {
            IImportador unImportador;
            List<ObjetoTransferenciaDatosRegistroDeVariable> registros = new List<ObjetoTransferenciaDatosRegistroDeVariable>();
            string estadoImportacion = "sin errores";
            int cantidadDeRegistrosCargados = 0;
            int cantidadDeRegistrosNoCargados = 0;
            try
            {
                unImportador = ObtenerImportadorSeleccionado();
                registros = unImportador.ObtenerRegistros();
            }
            catch (Exception excepcion)
            {
                MessageBox.Show(excepcion.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                estadoImportacion = "No se pudieron obtener los registros";
            }
            ManejadorDispositivo unManejadorDispositivo = new ManejadorDispositivo();
            foreach (var DTO in registros)
            {
                try
                {
                    Variable unaVariable = unManejadorDispositivo.ObtenerVariableDeDispositivo(DTO.DispositivoId, DTO.NombreVariable);
                    RegistroDeVariable unRegistro = new RegistroDeVariable(DTO.DispositivoId, unaVariable.VariableId, DTO.Valor, DTO.FechaYHora);
                    unManejadorRegistrosDeVariable.AgregarRegistroDeVariable(unRegistro);
                    cantidadDeRegistrosCargados++;
                }
                catch (ArgumentException excepcion)
                {
                    MessageBox.Show(excepcion.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    estadoImportacion = "Se encontraron errores:";
                    cantidadDeRegistrosNoCargados++;
                }
            }
            try
            {
                ManejadorDeLog unManejadorLog = ManejadorDeLog.ObtenerInstanciaManejadorDeLog();
                unManejadorLog.RegistrarImportacionDeValores("Admin");
            }
            catch (Exception excepcion)
            {
                MessageBox.Show(excepcion.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                estadoImportacion = "No se pudo acceder al log";
            }
            MessageBox.Show("Finalizó el ingreso de registros. " + estadoImportacion + "\nCantidad de registros importados: " + cantidadDeRegistrosCargados.ToString() + "\nCantidad de registros no importados: " + cantidadDeRegistrosNoCargados.ToString(), "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private IImportador ObtenerImportadorSeleccionado()
        {
            DataGridViewRow filaSeleccionada = grillaDatosImportadores.CurrentRow;
            string nombre = filaSeleccionada.Cells[0].Value.ToString();
            string nombreImportador = "";
            IImportador unImportador = importadores.First();
            foreach (var importador in importadores)
            {
                nombreImportador = importador.ObtenerNombre();
                if (nombreImportador.Equals(nombre))
                {
                    unImportador = importador;
                    break;
                }
            }
            return unImportador;
        }

        private void botonConsultar_Click(object sender, EventArgs e)
        {
            if (FechaDesdeEsMenorOIgualAFechaHasta(calendarioFechaDesde.Text, calendarioFechaHasta.Text))
            {
                DateTime fechaDesde = DateTime.Parse(calendarioFechaDesde.Text);
                DateTime fechaHasta = DateTime.Parse(calendarioFechaHasta.Text);
                ManejadorDeLog unManejadorLog = ManejadorDeLog.ObtenerInstanciaManejadorDeLog();
                try
                {
                    List<RegistroDeLog> registrosDeLog = unManejadorLog.ObtenerRegistrosPorFecha(fechaDesde, fechaHasta);
                    CrearFilasGrillaDatosLog(registrosDeLog);
                }
                catch (ArgumentException excepcion)
                {
                    MessageBox.Show(excepcion.Message, "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("'Fecha desde' debe ser menor o igual que 'fecha hasta'", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private bool FechaDesdeEsMenorOIgualAFechaHasta(string fechaDesde, string fechaHasta)
        {
            DateTime fechaPosterior = DateTime.Parse(fechaHasta);
            DateTime fechaAnterior = DateTime.Parse(fechaDesde);
            return (DateTime.Compare(fechaPosterior, fechaAnterior) >= 0);
        }

        private void CrearFilasGrillaDatosLog(List<RegistroDeLog> registrosDeLog)
        {
            grillaDatosLog.Rows.Clear();
            foreach (var registro in registrosDeLog)
            {
                grillaDatosLog.Rows.Add(registro.RegistroDeLogId, registro.FechaYHora, registro.Usuario, registro.Accion);
            }
        }

        private void pestañaRegistroDeVariable_Enter(object sender, EventArgs e)
        {
            RefrescarGrillaDatosDispositivos();
        }

        public void RefrescarGrillaDatosDispositivos()
        {
            grillaDatosDispositivos.Rows.Clear();
            CargarDispositivosEnGrillaDatosDispositivo();
        }
    }
}
