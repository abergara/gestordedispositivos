﻿using DominioGestorDispositivos;
using PersistenciaGestorDispositivos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestorTiposDeDispositivoYVariables
{
    public partial class VentanaGestionVariables : Form
    {
        private RestriccionesDeInterfaz restricciones;
        
        public VentanaGestionVariables()
        {
            InitializeComponent();
            restricciones = new RestriccionesDeInterfaz();
        }

        public Boolean DatosCorrectosParaCrearVariable()
        {
            bool nombreNoVacio = true;
            if (restricciones.EstaVaciaCajaTexto(cajaTextoNombre))
            {
                MessageBox.Show("El nombre no puede ser vacío", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                nombreNoVacio = false;
            }
            return nombreNoVacio;
        }

        public Variable CrearVariable()
        {
            string nombre = cajaTextoNombre.Text;
            double minimo = double.Parse(selectorNumericoMinimo.Text);
            double maximo = double.Parse(selectorNumericoMaximo.Text);
            Variable unaVariable = new Variable(nombre, minimo, maximo);
            return unaVariable;
        }

        public void ModificarTextoCajaTextoNombre(string nombre)
        {
            cajaTextoNombre.Text = nombre;
        }

        public void ModificarValorSelectorNumericoMinimo(string valor)
        {
            selectorNumericoMinimo.Text = valor;
        }

        public void ModificarValorSelectorNumericoMaximo(string valor)
        {
            selectorNumericoMaximo.Text = valor;
        }

        private void selectorNumericoMinimo_Leave(object sender, EventArgs e)
        {
            restricciones.NoPermitirSelectorVacio(selectorNumericoMinimo);
        }

        private void selectorNumericoMinimo_KeyPress(object sender, KeyPressEventArgs e)
        {
            restricciones.NoPermitirSelectorNumericoPasarseDeLargo(15, selectorNumericoMinimo, e);
        }

        private void selectorNumericoMaximo_Leave(object sender, EventArgs e)
        {
            restricciones.NoPermitirSelectorVacio(selectorNumericoMaximo);
        }

        private void selectorNumericoMaximo_KeyPress(object sender, KeyPressEventArgs e)
        {
            restricciones.NoPermitirSelectorNumericoPasarseDeLargo(15, selectorNumericoMaximo, e);
        }
    }
}
