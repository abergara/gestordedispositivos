﻿namespace GestorTiposDeDispositivoYVariables
{
    partial class VentanaModificarVariable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.botonModificar = new System.Windows.Forms.Button();
            this.etiquetaModificarVariable = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // botonModificar
            // 
            this.botonModificar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botonModificar.Location = new System.Drawing.Point(197, 215);
            this.botonModificar.Name = "botonModificar";
            this.botonModificar.Size = new System.Drawing.Size(75, 23);
            this.botonModificar.TabIndex = 20;
            this.botonModificar.Text = "Modificar";
            this.botonModificar.UseVisualStyleBackColor = true;
            this.botonModificar.Click += new System.EventHandler(this.botonModificar_Click);
            // 
            // etiquetaModificarVariable
            // 
            this.etiquetaModificarVariable.AutoSize = true;
            this.etiquetaModificarVariable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.etiquetaModificarVariable.Location = new System.Drawing.Point(14, 9);
            this.etiquetaModificarVariable.Name = "etiquetaModificarVariable";
            this.etiquetaModificarVariable.Size = new System.Drawing.Size(139, 20);
            this.etiquetaModificarVariable.TabIndex = 26;
            this.etiquetaModificarVariable.Text = "Modificar Variable:";
            // 
            // VentanaModificarVariable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.etiquetaModificarVariable);
            this.Controls.Add(this.botonModificar);
            this.Name = "VentanaModificarVariable";
            this.Text = "Modificar Variable";
            this.Controls.SetChildIndex(this.botonModificar, 0);
            this.Controls.SetChildIndex(this.etiquetaModificarVariable, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button botonModificar;
        private System.Windows.Forms.Label etiquetaModificarVariable;
    }
}