﻿using DominioGestorDispositivos;
using PersistenciaGestorDispositivos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestorTiposDeDispositivoYVariables
{
    public partial class VentanaModificarVariable : VentanaGestionVariables
    {
        private ManejadorVariable unManejadorVariable;
        private MenuPrincipal menuPrincipal;
        private Variable variableAModificar;

        public VentanaModificarVariable(MenuPrincipal unMenuPrincipal, Variable unaVariable)
        {
            InitializeComponent();
            unManejadorVariable = new ManejadorVariable();
            menuPrincipal = unMenuPrincipal;
            variableAModificar = unaVariable;
            CargarDatosVariable();
        }

        private void CargarDatosVariable()
        {
            ModificarTextoCajaTextoNombre(variableAModificar.Nombre);
            ModificarValorSelectorNumericoMinimo(variableAModificar.Minimo.ToString());
            ModificarValorSelectorNumericoMaximo(variableAModificar.Maximo.ToString());
        }

        private void botonModificar_Click(object sender, EventArgs e)
        {
            if (DatosCorrectosParaCrearVariable())
            {
                Variable variableModificada = CrearVariable();
                variableModificada.VariableId = variableAModificar.VariableId;
                ModificarVariable(variableModificada);
            }
        }

        private void ModificarVariable(Variable unaVariable)
        {
            try
            {
                unManejadorVariable.ModificarVariable(unaVariable);
                MessageBox.Show("Variable modificada correctamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                menuPrincipal.RefrescarGrillaDatosVariables();
                this.Dispose();
            }
            catch (ArgumentException e)
            {
                MessageBox.Show(e.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
