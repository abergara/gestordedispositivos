﻿using ImportadorDeRegistros;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GestorTiposDeDispositivoYVariables
{
    internal class GestorDeImportadores
    {
        public List<IImportador> ObtenerImportadores()
        {
            List<IImportador> importadores = new List<IImportador>();
            string carpeta = ConfigurationManager.AppSettings["Dlls"].ToString();
            DirectoryInfo directory = new DirectoryInfo(carpeta);
            FileInfo[] files;
            try
            {
                files = directory.GetFiles("*.dll");
            }
            catch (Exception excepcion)
            {
                throw excepcion;
            }

            foreach (FileInfo file in files)
	        {
                IImportador importador;
                Assembly dll = Assembly.LoadFile(file.FullName);
                Type[] tipos = dll.GetTypes();
                foreach (Type tipo in tipos)
                {
                    if (typeof(IImportador).IsAssignableFrom(tipo))
                    {
                        importador = (IImportador)Activator.CreateInstance(tipo);
                        importadores.Add(importador);
                    }
                }
	        }
            return importadores;
        }
    }
}
