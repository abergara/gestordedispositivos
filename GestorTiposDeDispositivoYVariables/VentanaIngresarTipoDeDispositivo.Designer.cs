﻿namespace GestorTiposDeDispositivoYVariables
{
    partial class VentanaIngresarTipoDeDispositivo
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.botonGuardar = new System.Windows.Forms.Button();
            this.etiquetaNuevoTipoDeDispositivo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // botonGuardar
            // 
            this.botonGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botonGuardar.Location = new System.Drawing.Point(497, 386);
            this.botonGuardar.Name = "botonGuardar";
            this.botonGuardar.Size = new System.Drawing.Size(75, 23);
            this.botonGuardar.TabIndex = 5;
            this.botonGuardar.Text = "Guardar";
            this.botonGuardar.UseVisualStyleBackColor = true;
            this.botonGuardar.Click += new System.EventHandler(this.botonGuardar_Click);
            // 
            // etiquetaNuevoTipoDeDispositivo
            // 
            this.etiquetaNuevoTipoDeDispositivo.AutoSize = true;
            this.etiquetaNuevoTipoDeDispositivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.etiquetaNuevoTipoDeDispositivo.Location = new System.Drawing.Point(9, 6);
            this.etiquetaNuevoTipoDeDispositivo.Name = "etiquetaNuevoTipoDeDispositivo";
            this.etiquetaNuevoTipoDeDispositivo.Size = new System.Drawing.Size(194, 20);
            this.etiquetaNuevoTipoDeDispositivo.TabIndex = 25;
            this.etiquetaNuevoTipoDeDispositivo.Text = "Nuevo Tipo de Dispositivo:";
            // 
            // VentanaIngresarTipoDeDispositivo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(587, 421);
            this.Controls.Add(this.etiquetaNuevoTipoDeDispositivo);
            this.Controls.Add(this.botonGuardar);
            this.Name = "VentanaIngresarTipoDeDispositivo";
            this.Text = "Nuevo Tipo de Dispositivo";
            this.Controls.SetChildIndex(this.botonGuardar, 0);
            this.Controls.SetChildIndex(this.etiquetaNuevoTipoDeDispositivo, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button botonGuardar;
        private System.Windows.Forms.Label etiquetaNuevoTipoDeDispositivo;
    }
}

