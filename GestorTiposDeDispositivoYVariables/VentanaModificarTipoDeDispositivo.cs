﻿using DominioGestorDispositivos;
using PersistenciaGestorDispositivos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestorTiposDeDispositivoYVariables
{
    public partial class VentanaModificarTipoDeDispositivo : VentanaGestionTipoDeDispositivo
    {
        private ManejadorTipoDeDispositivo unManejadorTipoDeDispositivo;
        private MenuPrincipal menuPrincipal;
        private TipoDeDispositivo tipoDeDispositivoAModificar;

        public VentanaModificarTipoDeDispositivo(MenuPrincipal unMenuPrincipal, TipoDeDispositivo unTipoDeDispositivo)
        {
            InitializeComponent();
            unManejadorTipoDeDispositivo = new ManejadorTipoDeDispositivo();
            menuPrincipal = unMenuPrincipal;
            tipoDeDispositivoAModificar = unTipoDeDispositivo;
            CargarVariablesEnGrillaDatosVariablesModificar();
            CargarDatosTipoDeDispositivo();
        }

        private void CargarDatosTipoDeDispositivo()
        {
            ModificarTextoCajaTextoNombre(tipoDeDispositivoAModificar.Nombre);
            ModificarTextoCajaTextoDescripcion(tipoDeDispositivoAModificar.Descripcion);
            foreach (var variable in tipoDeDispositivoAModificar.Variables)
            {
                AgregarFilaAGrillaVariables(variable, true);
            }
        }

        private void botonModificar_Click(object sender, EventArgs e)
        {
            if (DatosCorrectosParaCrearTipoDeDispositivo())
            {
                TipoDeDispositivo tipoDeDispositivoModificado = CrearTipoDeDispositivo();
                tipoDeDispositivoModificado.TipoDeDispositivoId = tipoDeDispositivoAModificar.TipoDeDispositivoId;
                ModificarTipoDeDispositivo(tipoDeDispositivoModificado);
            }
        }

        private void ModificarTipoDeDispositivo(TipoDeDispositivo unTipoDeDispositivo)
        {
            try
            {
                unManejadorTipoDeDispositivo.ModificarTipoDeDispositivo(unTipoDeDispositivo);
                MessageBox.Show("Tipo de dispositivo modificado correctamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                menuPrincipal.RefrescarGrillaDatosTiposDeDispositivo();
                menuPrincipal.RefrescarGrillaDatosVariables();
                this.Dispose();
            }
            catch (ArgumentException e)
            {
                MessageBox.Show(e.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
