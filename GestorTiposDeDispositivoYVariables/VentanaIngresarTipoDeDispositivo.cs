﻿using DominioGestorDispositivos;
using PersistenciaGestorDispositivos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestorTiposDeDispositivoYVariables
{
    public partial class VentanaIngresarTipoDeDispositivo : VentanaGestionTipoDeDispositivo
    {
        private ManejadorTipoDeDispositivo unManejadorTipoDeDispositivo;
        private MenuPrincipal menuPrincipal;

        public VentanaIngresarTipoDeDispositivo(MenuPrincipal unMenuPrincipal)
        {
            InitializeComponent();
            unManejadorTipoDeDispositivo = new ManejadorTipoDeDispositivo();
            menuPrincipal = unMenuPrincipal;
            CargarVariablesEnGrillaDatosVariables();
        }

        private void botonGuardar_Click(object sender, EventArgs e)
        {
            if (DatosCorrectosParaCrearTipoDeDispositivo())
            {
                TipoDeDispositivo tipoDeDispositivoAAgregar = CrearTipoDeDispositivo();
                AgregarTipoDeDispositivo(tipoDeDispositivoAAgregar);
            }
        }

        private void AgregarTipoDeDispositivo(TipoDeDispositivo tipoDeDispositivoAAgregar)
        {
            try
            {
                unManejadorTipoDeDispositivo.AgregarTipoDeDispositivo(tipoDeDispositivoAAgregar);
                MessageBox.Show("Tipo de dispositivo ingresado correctamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                menuPrincipal.RefrescarGrillaDatosTiposDeDispositivo();
                this.Dispose();
            }
            catch (ArgumentException e)
            {
                MessageBox.Show(e.Message, "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
