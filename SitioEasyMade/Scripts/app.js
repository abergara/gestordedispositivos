﻿(function () {
    'use strict';
    var app = angular.module('appGestorDeDispositivos', ['ngRoute', 'ng-fusioncharts']);
    app.config(function ($routeProvider) {

        $routeProvider

        .when('/', {
            templateUrl: 'Paginas/login.html',
            controller: 'controladorDeLogin'
        })

        .when('/dispositivos', {
            templateUrl: 'Paginas/dispositivos.html',
            controller: 'controladorDeDispositivo'
        })

        .when('/tiposDeDispositivo', {
            templateUrl: 'Paginas/tiposDeDispositivo.html',
            controller: 'controladorDeTipoDeDispositivo'
        })

        .when('/registrosDeVariables', {
            templateUrl: 'Paginas/registrosDeVariables.html',
            controller: 'controladorDeRegistrosDeVariables'
        })

        .when('/nuevoDispositivo', {
            templateUrl: 'Paginas/nuevoDispositivo.html',
            controller: 'controladorDeDispositivo'
        })

    });
    app.controller('controladorGlobal', function ($scope, $rootScope, $http) {
        $rootScope.usuarioLogueadoId = -1;
        PNotify.prototype.options.styling = "fontawesome";
        $rootScope.urlWebApi = 'http://localhost:49866/';
        $http({
            url: $rootScope.urlWebApi + 'api/DeviceTypes',
            method: 'GET'
        })
            .success(function (result) {
                $scope.TiposDeDispositivo = result;
            })
            .error(function (data, status) {
                $(function () {
                    new PNotify({
                        title: 'Error',
                        text: data.Message,
                        type: 'error'
                    });
                });
                location.href = "Index.html";
            });

        $rootScope.obtenerUsuario = function () {
            if ($rootScope.usuarioLogueadoId != -1) {
                $http.get($rootScope.urlWebApi + 'api/Users/' + $scope.usuarioLogueadoId)
                        .success(function (result) {
                            $rootScope.usuario = result;
                        })
                        .error(function (data, status) {
                            console.log(data.Message);
                        });
            }
        }

        $scope.cerrarSesion = function () {
            $rootScope.usuarioLogueadoId = -1;
            location.href = 'index.html';
        }
    });
})();