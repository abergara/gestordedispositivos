﻿(function () {
    'use strict';
    angular
        .module('appGestorDeDispositivos')
        .controller('controladorDeUsuario', function ($scope, $rootScope, $http, $log) {
            console.log("controladorDeUsuario");
            $http.get($rootScope.urlWebApi + 'api/Users')
                .success(function (result) {
                    $scope.usuarios = result;
                })
                .error(function (data, status) {
                    $log.error(data);
                });
        });
})();
