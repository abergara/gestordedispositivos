﻿(function () {
    'use strict';
    angular
        .module('appGestorDeDispositivos')
        .controller('controladorDeDispositivo', function ($scope, $rootScope, $http, $log) {
            if ($scope.usuarioLogueadoId == -1) {
                location.href = "Aplicacion.html";
            }
            $rootScope.obtenerUsuario();

            $scope.mostrarEditar = false;
            $scope.mostrarEditarImagen = false;
            $scope.mostrarEditarNombre = false;

            $("#btnAgregarDispositivo").on('click', function () {
                location.href = '#/nuevoDispositivo';
            });

            $scope.subirImagen = function (dispositivo) {
                var files = $("#inputFile" + dispositivo.DispositivoId).get(0).files;
                var data = new FormData();
                for (var i = 0; i < files.length; i++) {
                    data.append("file" + i, files[i]);
                }
                $.ajax({
                    type: "POST",
                    url: $rootScope.urlWebApi + "api/Files/"+dispositivo.DispositivoId,
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (result) {
                        if (result) {
                            $(function () {
                                new PNotify({
                                    title: 'Imagen guardada',
                                    text: 'Su imagen se guardó correctamente',
                                    type: 'success'
                                });
                            });
                            $rootScope.obtenerUsuario();
                            $("#inputFile").val('');
                        }
                    }
                })
                $scope.mostrarEditarImagen = false;
                $scope.mostrarEditar = false;
            };

            $scope.eliminarDispositivo = function (dispositivoABorrar) {
                $http({
                    url: $rootScope.urlWebApi + 'api/Devices/' + dispositivoABorrar.DispositivoId,
                    method: "DELETE"
                })

                .success(function () {
                    $(function () {
                        new PNotify({
                            title: 'Eliminación correcta',
                            text: 'Dispositivo eliminado correctamente.',
                            type: 'success'
                        });
                    });
                    $scope.obtenerUsuario();
                })

                .error(function (data, status) {
                    $log.error(data);
                });
            };

            $scope.agregarDispositivo = function () {
                var index = 0;
                for (var i = 0; i < $scope.TiposDeDispositivo.length && index == 0; i++) {
                    if ($scope.TiposDeDispositivo[i].TipoDeDispositivoId == $scope.dispositivo.TipoDeDispositivo) {
                        index = i;
                    }
                }
                var dispositivoParaAgregar = {
                    DispositivoId: 0,
                    Nombre: $scope.dispositivo.Nombre,
                    TipoDeDispositivo: $scope.TiposDeDispositivo[index]
                };
                $http.put($rootScope.urlWebApi + 'api/Devices/' + $scope.usuario.UsuarioId, dispositivoParaAgregar)
                    
                .success(function () {
                    $(function () {
                        new PNotify({
                            title: 'Creación correcta',
                            text: 'Dispositivo agregado correctamente.',
                            type: 'success'
                        });
                    });
                    location.href = '#/';

                })

                .error(function (data, status) {
                    $log.error(data);
                });
            };

            $scope.modificarDispositivo = function (dispositivoAModificar) {
                $scope.mostrarEditarNombre = false;
                var config = {
                    header: { 'Content-Type' : 'application/json'}
                };
                var dispositivo = JSON.stringify(dispositivoAModificar);
                $http.post($rootScope.urlWebApi + 'api/Devices', dispositivoAModificar, config)

                .success(function () {
                    $(function () {
                        new PNotify({
                            title: 'Modificación correcta',
                            text: 'Dispositivo modificado correctamente.',
                            type: 'success'
                        });
                    });
                    $scope.mostrarEditar = false;
                    $rootScope.obtenerUsuario();
                })

                .error(function (data, status) {
                    $log.error(data);
                });
            };

            $scope.clickBotonEditar = function (nombre) {
                $scope.mostrarEditar = !$scope.mostrarEditar;
                $scope.nombreViejo = nombre;
            };

            $scope.cancelarEdicion = function (dispositivo) {
                $scope.mostrarEditarNombre = false;
                dispositivo.Nombre = $scope.nombreViejo;
                $scope.mostrarEditar = !$scope.mostrarEditar;
            };
        });
})();
