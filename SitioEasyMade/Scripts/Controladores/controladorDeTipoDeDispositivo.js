﻿(function () {
    'use strict';
    angular
        .module('appGestorDeDispositivos')
        .controller('controladorDeTipoDeDispositivo', function ($scope, $rootScope, $http, $log) {
            if ($scope.usuarioLogueadoId == -1) {
                location.href = "Aplicacion.html";
            }
            $http.get($rootScope.urlWebApi + 'api/DeviceTypes')

                .success(function (result) {
                    $scope.tiposDeDispositivo = result;
                })

                .error(function (data, status) {
                    $log.error(data);
                });
        });
})();
