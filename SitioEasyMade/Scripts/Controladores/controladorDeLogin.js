﻿(function () {
    'use strict';
    angular
        .module('appGestorDeDispositivos')
        .controller('controladorDeLogin', function ($scope, $rootScope, $http, $log) {
            if ($scope.usuarioLogueadoId != -1) {
                location.href = "Aplicacion.html#/dispositivos";
            }
            $scope.loguearUsuario = function () {

                $http.get($rootScope.urlWebApi + 'api/Users/?id=1&email=' +
                    $scope.usuarioARegistrarse.Email + '&clave=' + $scope.usuarioARegistrarse.Clave)

                .success(function (result) {
                    $rootScope.usuarioLogueadoId = result;
                    $scope.obtenerUsuario();
                    location.href = "#/dispositivos";
                })

                .error(function (data, status) {
                    $(function () {
                        new PNotify({
                            title: 'Error.',
                            text: data.Message,
                            type: 'error'
                        });
                    });
                });
            }
        });
})();
