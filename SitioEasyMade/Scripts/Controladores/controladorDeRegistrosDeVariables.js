﻿(function () {
    'use strict';
    angular
        .module('appGestorDeDispositivos')
        .controller('controladorDeRegistrosDeVariables', function ($scope, $rootScope, $http) {
            if ($scope.usuarioLogueadoId == -1) {
                location.href = "Aplicacion.html";
            }

            $scope.graficaDeRegistros = "";
            $scope.primeraVez = true;
            $scope.opcionesDeGrafica = {
                vacia: "",
                lineal: "Paginas/graficaLineal.html"
            };
            $scope.cambioDispositivoSeleccionado = function () {
                var dispositivoId = $scope.dispositivoSeleccionado;
                for (var i = 0; i < $scope.usuario.Dispositivos.length; i++) {
                    if ($scope.usuario.Dispositivos[i].DispositivoId == dispositivoId) {
                        $scope.miTipoDeDispositivo = $scope.usuario.Dispositivos[i].TipoDeDispositivo;
                        i = $scope.usuario.Dispositivos.length;
                    }
                }
            };
            
            $scope.obtenerRegistros = function () {
                $scope.graficaDeRegistros = $scope.opcionesDeGrafica.vacia;
                $scope.fechaDesde = $("#inputFechaDesde").val();
                $scope.fechaHasta = $("#inputFechaHasta").val();
                if ($scope.fechaHasta < $scope.fechaDesde) {
                    $scope.fechasErradas = true;
                }
                else
                {
                    $scope.fechasErradas = false;
                    $scope.urlConsultaRegistros = $rootScope.urlWebApi + 'api/VariableRecords/' + $scope.dispositivoSeleccionado
                        + '?idVariable=' + $scope.variableSeleccionada + '&fechaDesde=' + $scope.fechaDesde
                        + '&fechaHasta=' + $scope.fechaHasta;


                    $.getJSON($scope.urlConsultaRegistros)
                    .success(function (data) {
                        $('#bodyDeRegistros').empty();
                        var codigoBody = '';
                        $scope.registrosDeVariable = data;
                        $scope.obtenerDatosParaGrafica();
                        $scope.graficaDeRegistros = $scope.opcionesDeGrafica.lineal;


                        $.each(data, function (key, val) {
                               var row = '<td>' + val.FechaYHora + '</td><td>' + val.Valor + '</td>';
                               codigoBody += '<tr>' + row;
                               codigoBody += '</tr>';
                        })
                        if (!$scope.primeraVez) {
                            $scope.tabla.destroy();
                        }
                        $('#tablaDeRegistros > tbody').html("");
                        $('#tablaDeRegistros > tbody:last-child').append(codigoBody);
                        $scope.tabla = $('#tablaDeRegistros').DataTable({
                            dom: 'Bfrtip',
                            buttons: [
                                'csv', 'excel', 'pdf'
                            ]
                        });
                        $scope.primeraVez = false; 
                    })
                    .error(function (data, status) {
                        $log.error(data);
                    });
                }
            }

            $scope.obtenerDatosParaGrafica = function () {

                $scope.atributos = {
                    "caption": "Grafica de la Variable Id:" + $scope.variableSeleccionada + " del Dispositivo Id:" + $scope.dispositivoSeleccionado,
                    "numberprefix": "",
                    "plotgradientcolor": "",
                    "bgcolor": "FFFFFF",
                    "showalternatehgridcolor": "0",
                    "divlinecolor": "CCCCCC",
                    "showvalues": "0",
                    "showcanvasborder": "0",
                    "canvasborderalpha": "0",
                    "canvasbordercolor": "CCCCCC",
                    "canvasborderthickness": "1",
                    "yaxismaxvalue": $scope.variableSeleccionada.Maximo,
                    "captionpadding": "30",
                    "linethickness": "3",
                    "yaxisvaluespadding": "30",
                    "legendshadow": "0",
                    "legendborderalpha": "0",
                    "palettecolors": "#f8bd19,#008ee4,#33bdda,#e44a00,#6baa01,#583e78",
                    "showborder": "0"
                };

                $scope.categories = [
                       {
                           "category": []
                       }
                ];

                $scope.dataset = [
                    {
                        "seriesname": "Id de Variable: "+ $scope.variableSeleccionada,
                        "data": []
                    }
                ];

                for (var i = 0; i < $scope.registrosDeVariable.length; i++) {
                    $scope.categories[0].category.push({ "label": $scope.registrosDeVariable[i].FechaYHora });
                    $scope.dataset[0].data.push({ "value": $scope.registrosDeVariable[i].Valor });
                }

                
            }

        });
})();
