﻿var app = angular.module('appPaginaEasyMade', []);

app.controller('controladorDeRegistro', function ($scope,$http) {
    PNotify.prototype.options.styling = "fontawesome";
    $scope.urlWebApi = 'http://localhost:49866/';
    $scope.submitted = function () {
        $scope.enviado = true;
        if ((!$scope.formulario.txtNombre.$dirty || !$scope.formulario.txtEmail.$dirty || !$scope.formulario.txtPassword.$dirty) 
            || ($scope.formulario.txtNombre.$invalid || $scope.formulario.txtEmail.$invalid || $scope.formulario.txtPassword.$invalid)) {

        } else {
            $scope.registrarUsuario();
        }
    }

    $scope.registrarUsuario = function () {
        $(function () {
            new PNotify({
                title: 'Información',
                text: 'El usuario se está ingresando',
                type: 'info'
            });
        });
        var config = {
            header: { 'Content-Type': 'application/json' }
        };
        var usuario = JSON.stringify($scope.usuarioARegistrar);
        $http.post($scope.urlWebApi+'api/Users', usuario, config)

        .success(function () {
            $(function () {
                new PNotify({
                    title: 'Usuario Ingresado',
                    text: 'El usuario se ingresó correctamente',
                    type: 'success'
                });
            });
            location.href = "Aplicacion.html";
        })

        .error(function (data, status) {
            $(function () {
                new PNotify({
                    title: 'Imagen guardada',
                    text: data.Message,
                    type: 'error'
                });
            });
        });
    };
});