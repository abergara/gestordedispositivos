﻿using ImportadorDeRegistros;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LectorJSON
{
    public class LectorJSON : IImportador
    {
        public string ObtenerNombre()
        {
            return "LectorJSON";
        }

        public List<ObjetoTransferenciaDatosRegistroDeVariable> ObtenerRegistros()
        {
            string rutaArchivo = Path.Combine(Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%"), "registros\\registros.json");
            using (StreamReader r = new StreamReader(rutaArchivo))
            {
                string json = r.ReadToEnd();
                List<ObjetoTransferenciaDatosRegistroDeVariable> items = JsonConvert.DeserializeObject<List<ObjetoTransferenciaDatosRegistroDeVariable>>(json);
                return items;
            }
        }
    }
}
