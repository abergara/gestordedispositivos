﻿using DominioGestorDispositivos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistenciaGestorDispositivos
{
    public class ContextoClasesGestorDeDispositivos : DbContext
    {
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Dispositivo> Dispositivos { get; set; }
        public DbSet<TipoDeDispositivo> TiposDeDispositivos { get; set; }
        public DbSet<Variable> Variables { get; set; }
        public DbSet<RegistroDeVariable> RegistrosDeVariables { get; set; }
    }
}
