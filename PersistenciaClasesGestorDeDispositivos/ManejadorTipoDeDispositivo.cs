﻿using DominioGestorDispositivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistenciaGestorDispositivos
{
    public class ManejadorTipoDeDispositivo
    {
        public List<TipoDeDispositivo> ObtenerTodosLosTiposDeDispositivos()
        {
            if (!ExistenTiposDeDispositivos())
            {
                throw new System.ArgumentException("No existen tipos de dispositivos ingresados");
            }
            List<TipoDeDispositivo> tiposDeDispositivosARetornar = new List<TipoDeDispositivo>();
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                var tiposDeDispositivos = from tipoDeDispositivo in persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos.Include("Variables")
                                          select tipoDeDispositivo;
                foreach (TipoDeDispositivo tipoDeDispositivo in tiposDeDispositivos)
                {
                    TipoDeDispositivo unTipo = new TipoDeDispositivo(tipoDeDispositivo.Nombre, tipoDeDispositivo.Descripcion, tipoDeDispositivo.Variables);
                    unTipo.TipoDeDispositivoId = tipoDeDispositivo.TipoDeDispositivoId;
                    tiposDeDispositivosARetornar.Add(unTipo);
                }
                return tiposDeDispositivosARetornar;
            }
        }

        private Boolean ExistenTiposDeDispositivos()
        {
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                List<TipoDeDispositivo> tiposDeDispositivos = persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos.ToList();
                int cantidadDeTiposDeDispositivo = tiposDeDispositivos.Count;
                return cantidadDeTiposDeDispositivo != 0;
            }
        }

        public void AgregarTipoDeDispositivo(TipoDeDispositivo unTipoDeDispositivo)
        {
            if (ExisteTipoDeDispositivo(unTipoDeDispositivo))
            {
                throw new System.ArgumentException("Ya existe un tipo de dispositivo con ese nombre");
            }
            if (unTipoDeDispositivo.Variables.Count == 0)
            {
                throw new System.ArgumentException("No se puede crear un tipo de dispositivo sin variables");
            }
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                if (unTipoDeDispositivo.Variables.Count != 0)
                {
                    foreach (var variable in unTipoDeDispositivo.Variables)
                    {
                        if (YaExisteVariableConEseNombreEnTipoDeDispositivo(variable, unTipoDeDispositivo.Variables))
                        {
                            throw new System.ArgumentException("No se puede crear un tipo de dispositivo con dos variables con el mismo nombre");
                        }
                        persistenciaBaseDatosGestorDispositivos.Variables.Attach(variable);
                    }
                }
                persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos.Add(unTipoDeDispositivo);
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
        }

        private Boolean YaExisteVariableConEseNombreEnTipoDeDispositivo(Variable unaVariable, List<Variable> variables)
        {
            Boolean existeVariable = false;
            int cantidadDeVariablesConElMismoNombre = 0;
            foreach (var variable in variables)
            {
                if (variable.Nombre.Equals(unaVariable.Nombre))
                {
                    cantidadDeVariablesConElMismoNombre++;
                    if (cantidadDeVariablesConElMismoNombre == 2)
                    {
                        existeVariable = true;
                        break;
                    }
                }
            }
            return existeVariable;
        }

        private Boolean ExisteTipoDeDispositivo(TipoDeDispositivo unTipoDeDispositivo)
        {
            Boolean elTipoDeDispositivoExiste = false;
            if (ExistenTiposDeDispositivos())
            {
                List<TipoDeDispositivo> todosLosTiposDeDispositivo = ObtenerTodosLosTiposDeDispositivos();
                elTipoDeDispositivoExiste = todosLosTiposDeDispositivo.Contains(unTipoDeDispositivo);
            }
            return elTipoDeDispositivoExiste;
        }

        public void EliminarTipoDeDispositivo(TipoDeDispositivo tipoDeDispositivoAEliminar)
        {
            if (!ExisteTipoDeDispositivo(tipoDeDispositivoAEliminar))
            {
                throw new System.ArgumentException("No se puede eliminar un tipo de dispositivo no existente");
            }
            if (TipoDeDispositivoEstaAsociadoAAlgunDispositivo(tipoDeDispositivoAEliminar))
            {
                throw new System.ArgumentException("No se puede eliminar un tipo de dispositivo asociado a un dispositivo");
            }
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                TipoDeDispositivo elUnicoTipoDeDispositivo;
                var unTipo = from tipoDeDispositivo in persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos.Include("Variables")
                                  where tipoDeDispositivo.TipoDeDispositivoId == tipoDeDispositivoAEliminar.TipoDeDispositivoId
                                  select tipoDeDispositivo;
                elUnicoTipoDeDispositivo = unTipo.SingleOrDefault();
                persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos.Remove(elUnicoTipoDeDispositivo);
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
        }

        public Boolean TipoDeDispositivoEstaAsociadoAAlgunDispositivo(TipoDeDispositivo unTipoDeDispositivo)
        {
            Boolean tipoDeDispositivoEstaAsociado = false;
            ManejadorDispositivo unManejadorDipositivo = new ManejadorDispositivo();
            try
            {
                Dispositivo unDispositivo;
                List<Dispositivo> todosLosDispositivos = unManejadorDipositivo.ObtenerTodosLosDispositivos();
                for (int i = 0; i < todosLosDispositivos.Count && !tipoDeDispositivoEstaAsociado; i++)
                {
                    unDispositivo = todosLosDispositivos.ElementAt(i);
                    tipoDeDispositivoEstaAsociado = unDispositivo.TipoDeDispositivo.Equals(unTipoDeDispositivo);
                }
            }
            catch (ArgumentException e)
            {
                if (e.Message.Equals("No existen dispositivos ingresados"))
                {
                    tipoDeDispositivoEstaAsociado = false;
                }
            }
            return tipoDeDispositivoEstaAsociado;
        }

        public void ModificarTipoDeDispositivo(TipoDeDispositivo tipoDeDispositivoAModificar)
        {
            if (!ExisteTipoDeDispositivo(tipoDeDispositivoAModificar))
            {
                throw new System.ArgumentException("No se puede modificar un tipo de dispositivo no existente");
            }
            if (YaExisteOtroTipoDeDispositivoConEseNombre(tipoDeDispositivoAModificar))
            {
                throw new System.ArgumentException("Ya existe un tipo de dispositivo con ese nombre");
            }
            if (TipoDeDispositivoEstaAsociadoAAlgunDispositivo(tipoDeDispositivoAModificar))
            {
                throw new System.ArgumentException("No se puede modificar un tipo de dispositivo asociado a un dispositivo");
            }
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                TipoDeDispositivo elUnicoTipoDeDispositivo;
                var unTipoDeDispositivo = from unTipo in persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos.Include("Variables")
                                          where unTipo.TipoDeDispositivoId == tipoDeDispositivoAModificar.TipoDeDispositivoId
                                          select unTipo;
                elUnicoTipoDeDispositivo = unTipoDeDispositivo.SingleOrDefault();
                elUnicoTipoDeDispositivo.Nombre = tipoDeDispositivoAModificar.Nombre;
                elUnicoTipoDeDispositivo.Descripcion = tipoDeDispositivoAModificar.Descripcion;
                
                List<Variable> variablesNuevas = tipoDeDispositivoAModificar.Variables;
                List<Variable> variablesExistentes = elUnicoTipoDeDispositivo.Variables;
                List<Variable> variablesAEliminar = new List<Variable>();

                foreach (var variable in variablesExistentes)
                {
                    if (!variablesNuevas.Contains(variable))
                    {
                        variablesAEliminar.Add(variable);
                    }
                }
                foreach (var variable in variablesNuevas)
                {
                    if (YaExisteVariableConEseNombreEnTipoDeDispositivo(variable, variablesNuevas))
                    {
                        throw new System.ArgumentException("No se puede crear un tipo de dispositivo con dos variables con el mismo nombre");
                    }
                    if (!variablesExistentes.Contains(variable))
                    {
                        persistenciaBaseDatosGestorDispositivos.Variables.Attach(variable);
                        elUnicoTipoDeDispositivo.Variables.Add(variable);
                    }
                }
                foreach (var variable in variablesAEliminar)
                {
                    elUnicoTipoDeDispositivo.Variables.Remove(variable);
                }

                VerificarCantidadDeVariablesDelTipoDeDispositivo(elUnicoTipoDeDispositivo);
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
        }

        private void VerificarCantidadDeVariablesDelTipoDeDispositivo(TipoDeDispositivo unTipoDeDispositivo)
        {
            int cantidadDeVariables = unTipoDeDispositivo.Variables.Count;
            if (cantidadDeVariables == 0)
            {
                throw new System.ArgumentException("El tipo de dispositivo debe poseer al menos una variable");
            }
        }

        private Boolean YaExisteOtroTipoDeDispositivoConEseNombre(TipoDeDispositivo tipoDeDispositivoAModificar)
        {
            Boolean elNombreDeTipoDeDispositivoExiste = false;
            if (ExistenTiposDeDispositivos())
            {
                List<TipoDeDispositivo> todosLosTiposDeDispositivo = ObtenerTodosLosTiposDeDispositivos();
                TipoDeDispositivo unTipoDeDispositivo;
                for (int i = 0; i < todosLosTiposDeDispositivo.Count && !elNombreDeTipoDeDispositivoExiste; i++)
                {
                    unTipoDeDispositivo = todosLosTiposDeDispositivo.ElementAt(i);
                    if (unTipoDeDispositivo.Nombre.Equals(tipoDeDispositivoAModificar.Nombre) &&
                        unTipoDeDispositivo.TipoDeDispositivoId != tipoDeDispositivoAModificar.TipoDeDispositivoId)
                    {
                        elNombreDeTipoDeDispositivoExiste = true;
                    }
                }
            }
            return elNombreDeTipoDeDispositivoExiste;
        }

        public TipoDeDispositivo ObtenerTipoDeDispositivo(TipoDeDispositivo unTipoDeDispositivo)
        {
            if (!ExisteTipoDeDispositivo(unTipoDeDispositivo))
            {
                throw new System.ArgumentException("No se puede obtener un tipo de dispositivo no existente");
            }
            TipoDeDispositivo elUnicoTipoDeDispositivo;
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                var tipoDeDispositivo = from unTipo in persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos.Include("Variables")
                              where unTipo.TipoDeDispositivoId == unTipoDeDispositivo.TipoDeDispositivoId
                              select unTipo;
                elUnicoTipoDeDispositivo = tipoDeDispositivo.SingleOrDefault();
            }
            TipoDeDispositivo tipoDeDispositivoCorrecto = new TipoDeDispositivo(elUnicoTipoDeDispositivo.Nombre, elUnicoTipoDeDispositivo.Descripcion, elUnicoTipoDeDispositivo.Variables);
            tipoDeDispositivoCorrecto.TipoDeDispositivoId = elUnicoTipoDeDispositivo.TipoDeDispositivoId;
            return tipoDeDispositivoCorrecto;
        }
    }
}
