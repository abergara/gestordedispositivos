﻿using DominioGestorDispositivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PersistenciaGestorDispositivos
{
    public class ManejadorUsuario
    {
        public void AgregarUsuario(Usuario unUsuario)
        {
            if (ExisteUsuario(unUsuario))
            {
                throw new System.ArgumentException("Ya existe un usuario asociado a ese email");
            }
            if (!FormatoDeEmailCorrecto(unUsuario.Email))
            {
                throw new System.ArgumentException("El formato de email para el usuario no es correcto");
            }
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                if (unUsuario.Dispositivos.Count != 0)
                {
                    foreach (var dispositivo in unUsuario.Dispositivos)
                    {
                        persistenciaBaseDatosGestorDispositivos.Dispositivos.Attach(dispositivo);
                    }   
                }
                persistenciaBaseDatosGestorDispositivos.Usuarios.Add(unUsuario);
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
        }

        private Boolean FormatoDeEmailCorrecto(string email)
        {
            Boolean formatoCorrecto;
            string expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(email, expresion))
            {
                formatoCorrecto = ValidarEmail(email, expresion);
            }
            else
            {
                formatoCorrecto = false;
            }
            return formatoCorrecto;
        }

        private Boolean ValidarEmail(string email, string expresion)
        {
            Boolean formatoCorrecto;
            if (Regex.Replace(email, expresion, String.Empty).Length == 0)
            {
                formatoCorrecto = true;
            }
            else
            {
                formatoCorrecto = false;
            }
            return formatoCorrecto;
        }

        public Boolean ExisteUsuario(Usuario unUsuario)
        {
            Boolean elUsuarioExiste = false;
            if (ExistenUsuarios())
            {
                List<Usuario> todosLosUsuarios = ObtenerTodosLosUsuarios();
                elUsuarioExiste = todosLosUsuarios.Contains(unUsuario);
            }
            return elUsuarioExiste;
        }

        private List<Usuario> ObtenerTodosLosUsuarios()
        {
            List<Usuario> usuariosARetornar = new List<Usuario>();
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                var usuarios = from usuario in persistenciaBaseDatosGestorDispositivos.Usuarios.Include("Dispositivos")
                               select usuario;
                foreach (var usuario in usuarios)
                {
                    usuariosARetornar.Add(usuario);
                }
                return usuariosARetornar;
            }
        }

        public void EliminarUsuario(Usuario unUsuario)
        {
            if (!ExisteUsuario(unUsuario))
            {
                throw new System.ArgumentException("No se puede eliminar un usuario no existente");
            }
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                Usuario elUnicoUsuario;
                var usuarioAEliminar = from usuario in persistenciaBaseDatosGestorDispositivos.Usuarios
                                       where usuario.UsuarioId == unUsuario.UsuarioId
                                       select usuario;
                elUnicoUsuario = usuarioAEliminar.SingleOrDefault();
                persistenciaBaseDatosGestorDispositivos.Usuarios.Remove(elUnicoUsuario);
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
        }

        public void ModificarUsuario(Usuario usuarioModificado)
        {
            if (!ExisteUsuario(usuarioModificado))
            {
                throw new System.ArgumentException("No se puede modificar un usuario no existente");
            }
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                Usuario elUnicoUsuario;
                var unUsuario = from usuario in persistenciaBaseDatosGestorDispositivos.Usuarios.Include("Dispositivos")
                                where usuario.UsuarioId == usuarioModificado.UsuarioId
                                select usuario;
                elUnicoUsuario = unUsuario.SingleOrDefault();
                elUnicoUsuario.Nombre = usuarioModificado.Nombre;
                elUnicoUsuario.Clave = usuarioModificado.Clave;
                if (usuarioModificado.Dispositivos.Count > 0)
                {
                    int posicionDelUltimoDispositivoAgregado = usuarioModificado.Dispositivos.Count - 1;
                    Dispositivo dispositivoAgregado = usuarioModificado.Dispositivos.ElementAt(posicionDelUltimoDispositivoAgregado);
                    persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos.Attach(dispositivoAgregado.TipoDeDispositivo);
                    elUnicoUsuario.Dispositivos.Add(dispositivoAgregado);
                }
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
        }

        public Usuario ObtenerUsuario(string email)
        {
            Usuario elUsuario = new Usuario();
            elUsuario.Email = email;
            if (!ExisteUsuario(elUsuario))
            {
                throw new System.ArgumentException("No existe un usuario con ese email");
            }
            Usuario elUnicoUsuario;
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                var usuario = from unUsuario in persistenciaBaseDatosGestorDispositivos.Usuarios.Include("Dispositivos")
                              where unUsuario.Email.Equals(email)
                              select unUsuario;
                elUnicoUsuario = usuario.SingleOrDefault();
            }
            Usuario usuarioCorrecto = new Usuario(elUnicoUsuario.Nombre, elUnicoUsuario.Email, elUnicoUsuario.Clave);
            usuarioCorrecto.UsuarioId = elUnicoUsuario.UsuarioId;
            usuarioCorrecto.Dispositivos = elUnicoUsuario.Dispositivos;
            foreach (Dispositivo unDispositivo in usuarioCorrecto.Dispositivos)
            {
                unDispositivo.TipoDeDispositivo = ObtenerTipoDeDispositivo(unDispositivo.DispositivoId);
            }
            return usuarioCorrecto;
        }

        private TipoDeDispositivo ObtenerTipoDeDispositivo(int dispositivoId)
        {
            Dispositivo elUnicoDispositivo;
            TipoDeDispositivo elUnicoTipoDeDispositivo;
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                var dispositivo = from unDispositivo in persistenciaBaseDatosGestorDispositivos.Dispositivos.Include("TipoDeDispositivo")
                              where unDispositivo.DispositivoId == dispositivoId
                              select unDispositivo;
                elUnicoDispositivo = dispositivo.SingleOrDefault();
                elUnicoTipoDeDispositivo = elUnicoDispositivo.TipoDeDispositivo;
                TipoDeDispositivo tipoDeDispositivoCorrecto = new TipoDeDispositivo(elUnicoTipoDeDispositivo.Nombre, elUnicoTipoDeDispositivo.Descripcion, elUnicoTipoDeDispositivo.Variables);
                tipoDeDispositivoCorrecto.TipoDeDispositivoId = elUnicoTipoDeDispositivo.TipoDeDispositivoId;
                return tipoDeDispositivoCorrecto;
            }
        }

        public Usuario ObtenerUsuarioConId(int usuarioId)
        {
            Usuario elUnicoUsuario;
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                var usuario = from unUsuario in persistenciaBaseDatosGestorDispositivos.Usuarios.Include("Dispositivos")
                              where unUsuario.UsuarioId.Equals(usuarioId)
                              select unUsuario;
                elUnicoUsuario = usuario.SingleOrDefault();
            }
            Usuario usuarioCorrecto = new Usuario(elUnicoUsuario.Nombre, elUnicoUsuario.Email, elUnicoUsuario.Clave);
            usuarioCorrecto.UsuarioId = elUnicoUsuario.UsuarioId;
            usuarioCorrecto.Dispositivos = elUnicoUsuario.Dispositivos;
            foreach (Dispositivo unDispositivo in usuarioCorrecto.Dispositivos)
            {
                unDispositivo.TipoDeDispositivo = ObtenerTipoDeDispositivo(unDispositivo.DispositivoId);
            }
            return usuarioCorrecto;
        }

        private Boolean ExistenUsuarios()
        {
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                List<Usuario> usuarios = persistenciaBaseDatosGestorDispositivos.Usuarios.ToList();
                int cantidadDeUsuarios = usuarios.Count;
                return cantidadDeUsuarios != 0;
            }
        }

        public Boolean ExisteUsuarioId(int usuarioId)
        {
            Boolean existeId = false;
            if (ExistenUsuarios())
            {
                using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
                {
                    List<Usuario> usuarios = persistenciaBaseDatosGestorDispositivos.Usuarios.ToList();
                    for (int i = 0; i < usuarios.Count && !existeId; i++)
                    {
                        Usuario unUsuario = usuarios.ElementAt(i); 
                        if (unUsuario.UsuarioId == usuarioId)
                        {
                            existeId = true;
                        }
                    }
                }
            }
            return existeId;
        }
    }
}
