﻿using DominioGestorDispositivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistenciaGestorDispositivos
{
    public class ManejadorVariable
    {
        public void AgregarVariable(Variable unaVariable)
        {
            if (unaVariable.Minimo > unaVariable.Maximo)
            {
                throw new System.ArgumentException("El mínimo de la variable debe ser menor o igual al máximo");
            }
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                persistenciaBaseDatosGestorDispositivos.Variables.Add(unaVariable);
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
        }

        public List<Variable> ObtenerTodasLasVariables()
        {
            if (!ExistenVariables())
            {
                throw new System.ArgumentException("No existen variables ingresadas");
            }
            List<Variable> variablesARetornar = new List<Variable>();
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                var variables = from variable in persistenciaBaseDatosGestorDispositivos.Variables
                                select variable;
                foreach (Variable variable in variables)
                {
                    Variable unaVariable = new Variable(variable.Nombre, variable.Minimo, variable.Maximo);
                    unaVariable.VariableId = variable.VariableId;
                    variablesARetornar.Add(unaVariable);
                }
                return variablesARetornar;
            }
        }

        public List<Variable> ObtenerTodasLasVariablesQueNoEstanAsociadasAAlgunTipoDeDispositivo()
        {
            try
            {
                List<Variable> variablesQueNoEstanAsociadas = new List<Variable>();
                foreach (var variable in ObtenerTodasLasVariables())
                {
                    if (!VariableEstaAsociadaAAlgunTipoDeDispositivo(variable))
                    {
                        variablesQueNoEstanAsociadas.Add(variable);
                    }
                }
                if (variablesQueNoEstanAsociadas.Count == 0)
                {
                    throw new System.ArgumentException("No existen variables ingresadas que no estén asociadas a un tipo de dispositivo");
                }
                return variablesQueNoEstanAsociadas;
            }
            catch (ArgumentException e)
            {
                throw e;
            }
        }

        private Boolean ExistenVariables()
        {
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                List<Variable> variables = persistenciaBaseDatosGestorDispositivos.Variables.ToList();
                int cantidadDeVariables = variables.Count;
                return cantidadDeVariables != 0;
            }
        }

        public void EliminarVariable(Variable variableAEliminar)
        {
            if (!ExisteVariable(variableAEliminar))
            {
                throw new System.ArgumentException("No se puede eliminar una variable no existente");
            }
            if (VariableEstaAsociadaAAlgunTipoDeDispositivo(variableAEliminar))
            {
                throw new System.ArgumentException("No se puede eliminar una variable asociada a un tipo de dispositivo");
            }
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                Variable laUnicaVariable;
                var unaVariable = from variable in persistenciaBaseDatosGestorDispositivos.Variables
                                    where variable.VariableId == variableAEliminar.VariableId
                                    select variable;
                laUnicaVariable = unaVariable.SingleOrDefault();
                persistenciaBaseDatosGestorDispositivos.Variables.Remove(laUnicaVariable);
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
        }

        private Boolean ExisteVariable(Variable unaVariable)
        {
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                List<Variable> variables = persistenciaBaseDatosGestorDispositivos.Variables.ToList();
                Boolean existeVariable = variables.Contains(unaVariable);
                return existeVariable;
            }
        }

        public Boolean VariableEstaAsociadaAAlgunTipoDeDispositivo(Variable unaVariable)
        {
            Boolean variableEstaAsociada = false;
            ManejadorTipoDeDispositivo unManejadorTipoDeDispositivo = new ManejadorTipoDeDispositivo();
            try
            {
                List<TipoDeDispositivo> tiposDeDispositivo = unManejadorTipoDeDispositivo.ObtenerTodosLosTiposDeDispositivos();
                TipoDeDispositivo unTipoDeDispositivo;
                for (int i = 0; i < tiposDeDispositivo.Count && !variableEstaAsociada; i++)
                {
                    unTipoDeDispositivo = tiposDeDispositivo.ElementAt(i);
                    variableEstaAsociada = VariablePerteneceATipoDeDispositivo(unTipoDeDispositivo, unaVariable);
                }
            }
            catch (ArgumentException e)
            {
                if (e.Message.Equals("No existen tipos de dispositivos ingresados"))
                {
                    variableEstaAsociada = false;
                }
            }
            return variableEstaAsociada;
        }

        public Boolean VariablePerteneceATipoDeDispositivo(TipoDeDispositivo unTipoDeDispositivo, Variable unaVariable)
        {
            ManejadorTipoDeDispositivo unManejadorTipoDeDispositivo = new ManejadorTipoDeDispositivo();
            List<TipoDeDispositivo> tiposDeDispositivo = unManejadorTipoDeDispositivo.ObtenerTodosLosTiposDeDispositivos();
            int posicionTipoDeDipositivo = tiposDeDispositivo.IndexOf(unTipoDeDispositivo);
            TipoDeDispositivo tipoDeDispositivoObtenido = tiposDeDispositivo.ElementAt(posicionTipoDeDipositivo);
            Boolean variablePertenece = tipoDeDispositivoObtenido.Variables.Contains(unaVariable);
            return variablePertenece;
        }

        public void ModificarVariable(Variable variableModificada)
        {
            if (!ExisteVariable(variableModificada))
            {
                throw new System.ArgumentException("No se puede modificar una variable no existente");
            }
            if (VariableEstaAsociadaAAlgunTipoDeDispositivo(variableModificada))
            {
                throw new System.ArgumentException("No se puede modificar una variable asociada a un tipo de dispositivo");
            }
            if (variableModificada.Minimo > variableModificada.Maximo)
            {
                throw new System.ArgumentException("El mínimo de la variable debe ser menor o igual al máximo");
            }
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                Variable laUnicaVariable;
                var unaVariable = from variable in persistenciaBaseDatosGestorDispositivos.Variables
                                    where variable.VariableId == variableModificada.VariableId
                                    select variable;
                laUnicaVariable = unaVariable.SingleOrDefault();
                laUnicaVariable.Nombre = variableModificada.Nombre;
                laUnicaVariable.Minimo = variableModificada.Minimo;
                laUnicaVariable.Maximo = variableModificada.Maximo;
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
        }

        public Variable ObtenerVariable(Variable unaVariable)
        {
            if (!ExisteVariable(unaVariable))
            {
                throw new System.ArgumentException("No se puede obtener una variable no existente");
            }
            Variable laUnicaVariable;
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                var variable = from laVariable in persistenciaBaseDatosGestorDispositivos.Variables
                              where laVariable.VariableId == unaVariable.VariableId
                              select laVariable;
                laUnicaVariable = variable.SingleOrDefault();
            }
            Variable variableCorrecta = new Variable(laUnicaVariable.Nombre, laUnicaVariable.Minimo, laUnicaVariable.Maximo);
            variableCorrecta.VariableId = laUnicaVariable.VariableId;
            return variableCorrecta;
        }
    }
}
