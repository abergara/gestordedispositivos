﻿using DominioGestorDispositivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistenciaGestorDispositivos
{
    public class ManejadorDispositivo
    {
        public void AgregarDispositivo(int unUsuarioId, Dispositivo unDispositivo)
        {
            ManejadorUsuario unManejadorUsuario = new ManejadorUsuario();
            Usuario unUsuario = unManejadorUsuario.ObtenerUsuarioConId(unUsuarioId);
            unUsuario.AgregarDispositivo(unDispositivo);
            unManejadorUsuario.ModificarUsuario(unUsuario);
        }

        public void ModificarDispositivo(Dispositivo dispositivoModificado)
        {
            if (!ExisteDispositivo(dispositivoModificado))
            {
                throw new System.ArgumentException("No se puede modificar un dispositivo no existente");
            }
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                Dispositivo elUnicoDispositivo;
                var unDispositivo = from dispositivo in persistenciaBaseDatosGestorDispositivos.Dispositivos.Include("TipoDeDispositivo")
                                    where dispositivo.DispositivoId == dispositivoModificado.DispositivoId
                                    select dispositivo;
                elUnicoDispositivo = unDispositivo.SingleOrDefault();
                elUnicoDispositivo.Nombre = dispositivoModificado.Nombre;
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
        }

        public void EliminarDispositivo(Dispositivo dispositivoAEliminar)
        {
            if (!ExisteDispositivo(dispositivoAEliminar))
            {
                throw new System.ArgumentException("No se puede eliminar un dispositivo no existente");
            }
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                Dispositivo elUnicoDispositivo;
                var unDispositivo = from dispositivo in persistenciaBaseDatosGestorDispositivos.Dispositivos.Include("TipoDeDispositivo")
                                    where dispositivo.DispositivoId == dispositivoAEliminar.DispositivoId
                                    select dispositivo;
                elUnicoDispositivo = unDispositivo.SingleOrDefault();
                EliminarRegistrosDeDispositivo(elUnicoDispositivo);
                persistenciaBaseDatosGestorDispositivos.Dispositivos.Remove(elUnicoDispositivo);
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
        }

        public Boolean ExisteDispositivo(Dispositivo unDispositivo)
        {
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                List<Dispositivo> dispositivos = persistenciaBaseDatosGestorDispositivos.Dispositivos.ToList();
                Boolean existeDispositivo = dispositivos.Contains(unDispositivo);
                return existeDispositivo;
            }
        }

        private void EliminarRegistrosDeDispositivo(Dispositivo dispositivoAEliminar)
        {
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                List<RegistroDeVariable> registrosDeVariables = persistenciaBaseDatosGestorDispositivos.RegistrosDeVariables.ToList();
                for (int i = 0; i < registrosDeVariables.Count; i++)
                {
                    RegistroDeVariable unRegistro = registrosDeVariables.ElementAt(i);
                    if (unRegistro.DispositivoId == dispositivoAEliminar.DispositivoId)
                    {
                        persistenciaBaseDatosGestorDispositivos.RegistrosDeVariables.Remove(unRegistro);
                    }
                }
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
        }

        public Boolean VariablePerteneceADispositivo(Dispositivo unDispositivo, Variable unaVariable)
        {
            if (!ExisteDispositivo(unDispositivo))
            {
                throw new System.ArgumentException("No existe el dispositivo indicado");
            }
            ManejadorVariable unManejadorVariable = new ManejadorVariable();
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                List<Dispositivo> dispositivos = persistenciaBaseDatosGestorDispositivos.Dispositivos.Include("TipoDeDispositivo").ToList();
                int posicionDipositivoAObtener = dispositivos.IndexOf(unDispositivo);
                Dispositivo dispositivoObtenido = dispositivos.ElementAt(posicionDipositivoAObtener);
                return unManejadorVariable.VariablePerteneceATipoDeDispositivo(dispositivoObtenido.TipoDeDispositivo, unaVariable);
            }

        }

        public List<Dispositivo> ObtenerTodosLosDispositivos()
        {
            if (!ExistenDispositivos())
            {
                throw new System.ArgumentException("No existen dispositivos ingresados");
            }
            List<Dispositivo> dispositivosARetornar = new List<Dispositivo>();
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                var dispositivos = from dispositivo in persistenciaBaseDatosGestorDispositivos.Dispositivos.Include("TipoDeDispositivo")
                                          select dispositivo;
                foreach (Dispositivo dispositivo in dispositivos)
                {
                    Dispositivo unDispositivo = new Dispositivo(dispositivo.Nombre, dispositivo.TipoDeDispositivo);
                    unDispositivo.DispositivoId = dispositivo.DispositivoId;
                    dispositivosARetornar.Add(unDispositivo);
                }
                return dispositivosARetornar;
            }
        }

        private Boolean ExistenDispositivos()
        {
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                List<Dispositivo> dispositivos = persistenciaBaseDatosGestorDispositivos.Dispositivos.ToList();
                int cantidadDispositivo = dispositivos.Count;
                return cantidadDispositivo != 0;
            }
        }

        public Variable ObtenerVariableDeDispositivo(int dispositivoId, string nombreVariable)
        {
            ManejadorTipoDeDispositivo unManejadorTipoDeDispositivo = new ManejadorTipoDeDispositivo();
            List<Dispositivo> dispositivos = ObtenerTodosLosDispositivos();
            Dispositivo unDispositivo = new Dispositivo();
            foreach (var dispositivo in dispositivos)
            {
                if (dispositivo.DispositivoId == dispositivoId)
                {
                    unDispositivo = dispositivo;
                    break;
                }
            }
            if (unDispositivo.DispositivoId == 0)
            {
                throw new System.ArgumentException("No existe el dispositivo");
            }
            TipoDeDispositivo tipoDeDispositivo = unManejadorTipoDeDispositivo.ObtenerTipoDeDispositivo(unDispositivo.TipoDeDispositivo);
            List<Variable> variablesDelDispositivo = tipoDeDispositivo.Variables;
            Variable variableRequerida = ObtenerVariableDeListaDeVariables(variablesDelDispositivo, nombreVariable);
            return variableRequerida;
        }

        private Variable ObtenerVariableDeListaDeVariables(List<Variable> variablesDelDispositivo, string nombreVariable)
        {
            Variable variableRequerida = new Variable();
            foreach (var variable in variablesDelDispositivo)
            {
                if (variable.Nombre.Equals(nombreVariable))
                {
                    variableRequerida = variable;
                }
            }
            if (variableRequerida.VariableId == 0)
            {
                throw new System.ArgumentException("La variable buscada no pertenece al dispositivo");
            }
            return variableRequerida;
        }
    }
}
