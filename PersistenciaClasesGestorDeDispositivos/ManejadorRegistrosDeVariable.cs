﻿using DominioGestorDispositivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersistenciaGestorDispositivos
{
    public class ManejadorRegistrosDeVariable
    {
        public List<RegistroDeVariable> ObtenerRegistrosParaDispositivoVariableFechas(int dispositivoId, int variableId, DateTime fechaDesde, DateTime fechaHasta)
        {
            List<RegistroDeVariable> registrosDeVariableARetornar = new List<RegistroDeVariable>();
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                var registrosDeVariable = from registroDeVariable in persistenciaBaseDatosGestorDispositivos.RegistrosDeVariables
                                          where registroDeVariable.DispositivoId == dispositivoId && registroDeVariable.VariableId == variableId &&
                                               (DateTime.Compare(registroDeVariable.FechaYHora, fechaDesde) > 0 || DateTime.Compare(registroDeVariable.FechaYHora, fechaDesde) == 0) &&
                                               (DateTime.Compare(registroDeVariable.FechaYHora, fechaHasta) < 0 || DateTime.Compare(registroDeVariable.FechaYHora, fechaHasta) == 0)
                                          orderby registroDeVariable.FechaYHora
                                          select registroDeVariable;
                foreach (var registroDeVariable in registrosDeVariable)
                {
                    registrosDeVariableARetornar.Add(registroDeVariable);
                }
                return registrosDeVariableARetornar;
            }
        }

        public void AgregarRegistroDeVariable(RegistroDeVariable unRegistroDeVariable)
        {
            ManejadorVariable unManejadorVariable = new ManejadorVariable();
            Dispositivo unDispositivo = new Dispositivo();
            unDispositivo.DispositivoId = unRegistroDeVariable.DispositivoId;
            Variable unaVariable = new Variable();
            unaVariable.VariableId = unRegistroDeVariable.VariableId;
            CorroborarQueExistenDispositivoYVariable(unDispositivo, unaVariable);
            Variable variableObtenida = unManejadorVariable.ObtenerVariable(unaVariable);
            double minimoVariable = variableObtenida.Minimo;
            double maximoVariable = variableObtenida.Maximo;
            double valorAIngresar = unRegistroDeVariable.Valor;
            if (valorAIngresar > maximoVariable || valorAIngresar < minimoVariable)
            {
                throw new System.ArgumentException("El valor a ingresar debe estar entre el mínimo y máximo de la variable");
            }
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                persistenciaBaseDatosGestorDispositivos.RegistrosDeVariables.Add(unRegistroDeVariable);
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
        }

        private void CorroborarQueExistenDispositivoYVariable(Dispositivo unDispositivo, Variable unaVariable)
        {
            ManejadorDispositivo unManejadorDispositivo = new ManejadorDispositivo();
            if (!unManejadorDispositivo.ExisteDispositivo(unDispositivo))
            {
                throw new System.ArgumentException("El registro posee un dispositivo no existente");
            }
            if (!ExisteVariableParaRegistro(unaVariable))
            {
                throw new System.ArgumentException("El registro posee una variable no existente");
            }
            if (!unManejadorDispositivo.VariablePerteneceADispositivo(unDispositivo, unaVariable))
            {
                throw new System.ArgumentException("La variable del registro no pertenece al dispositivo");
            }
        }

        private Boolean ExisteVariableParaRegistro(Variable unaVariable)
        {
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                List<Variable> variables = persistenciaBaseDatosGestorDispositivos.Variables.ToList();
                Boolean existeVariable = variables.Contains(unaVariable);
                return existeVariable;
            }
        }
    }
}
