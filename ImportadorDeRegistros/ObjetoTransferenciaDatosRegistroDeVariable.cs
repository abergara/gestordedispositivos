﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportadorDeRegistros
{
    public class ObjetoTransferenciaDatosRegistroDeVariable
    {
        public int DispositivoId { get; set; }
        public string NombreVariable { get; set; }
        public double Valor { get; set; }
        public DateTime FechaYHora { get; set; }

        public ObjetoTransferenciaDatosRegistroDeVariable(int unDispositivoId, string unNombreVariable, double valor, DateTime fechaYHora)
        {
            DispositivoId = unDispositivoId;
            NombreVariable = unNombreVariable;
            Valor = valor;
            FechaYHora = fechaYHora;
        }

        public ObjetoTransferenciaDatosRegistroDeVariable()
        {
        }
    }
}
