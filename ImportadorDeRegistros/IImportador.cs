﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportadorDeRegistros
{
    public interface IImportador
    {
        List<ObjetoTransferenciaDatosRegistroDeVariable> ObtenerRegistros();

        string ObtenerNombre();
    }
}
