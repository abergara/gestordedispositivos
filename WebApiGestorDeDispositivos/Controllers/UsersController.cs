﻿using DominioGestorDispositivos;
using PersistenciaClasesDominioLog;
using PersistenciaGestorDispositivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApiGestorDeDispositivos.Controllers
{
    public class UsersController : ApiController
    {
        private ManejadorUsuario unManejador;

        public UsersController()
        {
            unManejador = new ManejadorUsuario();
        }

        // GET api/users
        public Usuario Get(int id)
        {
            return unManejador.ObtenerUsuarioConId(id);
        }
        
        // GET api/users/5
        public IHttpActionResult Get(int id, string email, string clave)
        {
            try 
            {
                Usuario unUsuario = unManejador.ObtenerUsuario(email);
                if (unUsuario.Clave.Equals(clave))
                {
                    ManejadorDeLog unManejadorLog = ManejadorDeLog.ObtenerInstanciaManejadorDeLog();
                    unManejadorLog.RegistrarInicioDeSesion(email);
                    return Ok(unUsuario.UsuarioId);
                }
                else
                {
                    return BadRequest("Contraseña incorrecta");
                }
            }
            catch(ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            catch(Exception excepcion)
            {
                return BadRequest(excepcion.Message);
            }
        }

        // POST api/users
        public IHttpActionResult Post(Usuario usuario)
        {
            try
            {
                List<Dispositivo> unaLista = new List<Dispositivo>();
                usuario.Dispositivos = unaLista;
                unManejador.AgregarUsuario(usuario);
                return Ok();
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

        }

        // PUT api/users/5
        public void Put([FromBody]Usuario usuario, [FromBody]Dispositivo dispositivo)
        {
            usuario.EliminarDispositivo(dispositivo);
            unManejador.ModificarUsuario(usuario);
        }
        
        // DELETE api/users/5
        public void Delete(int id)
        {
        }
    }
}
