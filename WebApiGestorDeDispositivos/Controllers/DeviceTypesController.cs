﻿using DominioGestorDispositivos;
using PersistenciaGestorDispositivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Helpers;
using System.Web.Http;

namespace WebApiGestorDeDispositivos.Controllers
{
    public class DeviceTypesController : ApiController
    {
        private ManejadorTipoDeDispositivo unManejador;
        public DeviceTypesController()
        {
            unManejador = new ManejadorTipoDeDispositivo();
        }

        // GET api/devicetypes
        public IHttpActionResult Get()
        {
            try
            {
                HttpRequestMessage re = new HttpRequestMessage();
                var headers = re.Headers;

                if (headers.Contains("usuarioId"))
                {
                    string token = headers.GetValues("usuarioId").First();
                }
                List<TipoDeDispositivo> tiposDeDispositivo = unManejador.ObtenerTodosLosTiposDeDispositivos();
                return Ok(tiposDeDispositivo);
            }
            catch (ArgumentException e) {
                return BadRequest(e.Message);
            }
        }

        // GET api/devicetypes/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/devicetypes
        public void Post([FromBody]string value)
        {
        }

        // PUT api/devicetypes/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/devicetypes/5
        public void Delete(int id)
        {
        }
    }
}
