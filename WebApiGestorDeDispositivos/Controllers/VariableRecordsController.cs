﻿using DominioGestorDispositivos;
using PersistenciaGestorDispositivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApiGestorDeDispositivos.Controllers
{
    public class VariableRecordsController : ApiController
    {
        private ManejadorRegistrosDeVariable unManejador;

        public VariableRecordsController()
        {
            unManejador = new ManejadorRegistrosDeVariable();
        }

        // GET api/variablerecord
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/variablerecord/5
        public List<RegistroDeVariable> Get(int id, int idVariable, string fechaDesde, string fechaHasta)
        {
            DateTime fechaInicio = DateTime.Parse(fechaDesde);
            DateTime fechaFin = DateTime.Parse(fechaHasta);
            return unManejador.ObtenerRegistrosParaDispositivoVariableFechas(id, idVariable, fechaInicio, fechaFin);
        }

        // POST api/variablerecord
        public IHttpActionResult Post([FromBody]RegistroDeVariable unRegistroDeVariable)
        {
            try
            {
                DateTime fechaActual = DateTime.Now;
                unRegistroDeVariable.FechaYHora = fechaActual;
                unManejador.AgregarRegistroDeVariable(unRegistroDeVariable);
                return Ok();
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        // PUT api/variablerecord/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/variablerecord/5
        public void Delete(int id)
        {
        }
    }
}
