﻿using DominioGestorDispositivos;
using PersistenciaGestorDispositivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApiGestorDeDispositivos.Controllers
{
    public class DevicesController : ApiController
    {
        private ManejadorDispositivo unManejador;
        private ManejadorUsuario unManejadorUsuario;
        
        public DevicesController()
        {
            unManejador = new ManejadorDispositivo();
            unManejadorUsuario = new ManejadorUsuario();
        }

        // GET api/devices
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/devices/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/devices
        public void Post(Dispositivo unDispositivo)
        {
            unManejador.ModificarDispositivo(unDispositivo);
        }

        // PUT api/devices/5
        public IHttpActionResult Put(int id, [FromBody]Dispositivo unDispositivo)
        {
            if (unManejadorUsuario.ExisteUsuarioId(id))
            {
                unManejador.AgregarDispositivo(id, unDispositivo);
                return Ok();
            }
            else
            {
                return BadRequest("El usuario no esta registrado");
            }
        }

        // DELETE api/devices/5
        public void Delete(int id)
        {
            Dispositivo dispositivoABorrar = new Dispositivo();
            dispositivoABorrar.DispositivoId = id;
            unManejador.EliminarDispositivo(dispositivoABorrar);
        }
    }
}
