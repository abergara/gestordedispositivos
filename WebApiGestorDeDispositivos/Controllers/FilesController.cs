﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace WebApiGestorDeDispositivos.Controllers
{
    public class FilesController : ApiController
    {
        // GET api/files/5
        public HttpResponseMessage Get(int id)
        {
            HttpResponseMessage result = null;
            var localPath = HttpContext.Current.Server.MapPath("~/DevicePhoto/");
            var localFilePath = localPath + id;
            result = Request.CreateResponse(HttpStatusCode.OK);
            try
            {// serve the file to the client
                result.Content = new StreamContent(new FileStream(localFilePath, FileMode.Open, FileAccess.Read));
                result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = id.ToString();
            }
            catch (Exception e)
            {
                result.Content = new StreamContent(new FileStream(localPath + "/imagenNoDisponible.gif", FileMode.Open, FileAccess.Read));
                result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = id.ToString() + ".gif";
            }
            return result;
        }

        // POST api/files
        public IHttpActionResult Post(int id)
        {
            var solicitud = HttpContext.Current.Request;
            if (solicitud.Files.Count > 0)
            {
                foreach (string archivo in solicitud.Files)
                {
                    var postedFile = solicitud.Files[archivo];
                    var filePath = HttpContext.Current.Server.MapPath(string.Format("~/DevicePhoto/" + id));
                    postedFile.SaveAs(filePath);
                }
                return Ok(true);
            }
            else
            {
                return BadRequest();
            }
        }

        // DELETE api/files/5
        public void Delete(int id)
        {
            var filePath = HttpContext.Current.Server.MapPath(string.Format("~/DevicePhoto/" + id));
            File.Delete(filePath);
        }
    }
}
