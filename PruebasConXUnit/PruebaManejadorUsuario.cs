﻿using DominioGestorDispositivos;
using PersistenciaGestorDispositivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PruebasXUnit
{
    [Collection("MisPruebas")]
    public class PruebaManejadorUsuario
    {
        [Fact]
        public void PruebaAgregarUsuario()
        {
            ManejadorUsuario unManejador = new ManejadorUsuario();
            Usuario unUsuario = new Usuario("Carlos Rodriguez", "carrod@hotmail.com", "clave1");
            unManejador.AgregarUsuario(unUsuario);
            Boolean usuarioBienAgregado = unManejador.ExisteUsuario(unUsuario);
            unManejador.EliminarUsuario(unUsuario);
            Assert.True(usuarioBienAgregado);
        }

        [Fact]
        public void PruebaAgregarUsuarioConEmailIngresado()
        {
            ManejadorUsuario unManejador = new ManejadorUsuario();
            Usuario unUsuario = new Usuario("Carlos Rodriguez", "carrod@hotmail.com", "clave1");
            unManejador.AgregarUsuario(unUsuario);
            Assert.Throws<System.ArgumentException>(
               delegate
               {
                   unManejador.AgregarUsuario(unUsuario);
            
               });
            unManejador.EliminarUsuario(unUsuario);
        }

        [Fact]
        public void PruebaAgregarUsuarioEmailFormatoErroneo()
        {
            ManejadorUsuario unManejador = new ManejadorUsuario();
            Usuario unUsuario = new Usuario("Carlos Rodriguez", "carrod.com", "clave1");
            Assert.Throws<System.ArgumentException>(
               delegate
               {
                   unManejador.AgregarUsuario(unUsuario);

               });
        }

        [Fact]
        public void PruebaModificarUsuario()
        {
            ManejadorUsuario unManejador = new ManejadorUsuario();
            Usuario unUsuario = new Usuario("Carlos Rodriguez", "carrod@hotmail.com", "clave1");
            unManejador.AgregarUsuario(unUsuario);
            Usuario usuarioModificado = new Usuario("Carlos Rodriguez", "carrod@hotmail.com", "clave2");
            Usuario usuarioAgregado = unManejador.ObtenerUsuario("carrod@hotmail.com");
            usuarioModificado.UsuarioId = usuarioAgregado.UsuarioId;
            unManejador.ModificarUsuario(usuarioModificado);
            Usuario usuarioObtenido = unManejador.ObtenerUsuario("carrod@hotmail.com");
            Boolean usuarioBienModificado = usuarioObtenido.Clave.Equals("clave2");
            unManejador.EliminarUsuario(unUsuario);
            Assert.True(usuarioBienModificado);
        }

        [Fact]
        public void PruebaModificarUsuarioConUsuarioNoExistente()
        {
            ManejadorUsuario unManejador = new ManejadorUsuario();
            Usuario unUsuario = new Usuario("Joaquin", "joaco@hotmail.com", "123");
            Assert.Throws<System.ArgumentException>(
               delegate
               {
                   unManejador.ModificarUsuario(unUsuario);

               });
        }
        
        [Fact]
        public void PruebaEliminarUsuario()
        {
            ManejadorUsuario unManejador = new ManejadorUsuario();
            Usuario unUsuario = new Usuario("Carlos Rodriguez", "carrod@hotmail.com", "clave1");
            unManejador.AgregarUsuario(unUsuario);
            unManejador.EliminarUsuario(unUsuario);
            Boolean usuarioBienEliminado = !unManejador.ExisteUsuario(unUsuario);
            Assert.True(usuarioBienEliminado);
        }

        [Fact]
        public void PruebaEliminarUsuarioConUsuarioNoExistente()
        {
            ManejadorUsuario unManejador = new ManejadorUsuario();
            Usuario unUsuario = new Usuario("Joaquin", "joaco@hotmail.com", "123");
            Assert.Throws<System.ArgumentException>(
               delegate
               {
                   unManejador.EliminarUsuario(unUsuario);

               });
        }

        [Fact]
        public void PruebaExisteUsuario()
        {
            ManejadorUsuario unManejador = new ManejadorUsuario();
            Usuario unUsuario = new Usuario("Carlos Rodriguez", "carrod@hotmail.com", "clave1");
            unManejador.AgregarUsuario(unUsuario);
            Boolean existeUsuario = unManejador.ExisteUsuario(unUsuario);
            unManejador.EliminarUsuario(unUsuario);
            Assert.True(existeUsuario);
        }

        [Fact]
        public void PruebaObtenerUsuario()
        {
            ManejadorUsuario unManejador = new ManejadorUsuario();
            Usuario unUsuario = new Usuario("Carlos Rodriguez", "carrod@hotmail.com", "clave1");
            unManejador.AgregarUsuario(unUsuario);
            Usuario usuarioObtenido = unManejador.ObtenerUsuario("carrod@hotmail.com");
            Boolean usuarioBienObtenido = unUsuario.Equals(usuarioObtenido);
            unManejador.EliminarUsuario(unUsuario);
            Assert.True(usuarioBienObtenido);
        }

        [Fact]
        public void PruebaObtenerUsuarioSinUsuarioExistente()
        {
            ManejadorUsuario unManejador = new ManejadorUsuario();
            Assert.Throws<System.ArgumentException>(
               delegate
               {
                   unManejador.ObtenerUsuario("carrod@hotmail.com");

               });
        }

        [Fact]
        public void PruebaObtenerUsuarioConId()
        {
            ManejadorUsuario unManejador = new ManejadorUsuario();
            Usuario unUsuario = new Usuario("Carlos Rodriguez", "carrod@hotmail.com", "clave1");
            unManejador.AgregarUsuario(unUsuario);
            Usuario usuarioObtenidoConMail = unManejador.ObtenerUsuario("carrod@hotmail.com");
            Usuario usuarioObtenidoConId = unManejador.ObtenerUsuarioConId(usuarioObtenidoConMail.UsuarioId);
            Boolean usuarioBienObtenido = unUsuario.Equals(usuarioObtenidoConId);
            unManejador.EliminarUsuario(unUsuario);
            Assert.True(usuarioBienObtenido);
        }

        [Fact]
        public void PruebaExisteUsuarioId()
        {
            ManejadorUsuario unManejador = new ManejadorUsuario();
            Usuario unUsuario = new Usuario("Carlos Rodriguez", "carrod@hotmail.com", "clave1");
            unManejador.AgregarUsuario(unUsuario);
            Usuario usuarioAgregado = unManejador.ObtenerUsuario("carrod@hotmail.com");
            Boolean existeId = unManejador.ExisteUsuarioId(usuarioAgregado.UsuarioId);
            unManejador.EliminarUsuario(unUsuario);
            Assert.True(existeId);
        }

        [Fact]
        public void PruebaExisteUsuarioIdConUsuarioIdNoExistente()
        {
            ManejadorUsuario unManejador = new ManejadorUsuario();
            Usuario unUsuario = new Usuario("Carlos Rodriguez", "carrod@hotmail.com", "clave1");
            unManejador.AgregarUsuario(unUsuario);
            int usuarioId = 98;
            Boolean existeId = unManejador.ExisteUsuarioId(usuarioId);
            unManejador.EliminarUsuario(unUsuario);
            Assert.False(existeId);
        }

        [Fact]
        public void PruebaExisteUsuarioIdSinUsuariosAgregados()
        {
            ManejadorUsuario unManejador = new ManejadorUsuario();
            int usuarioId = 7;
            Boolean existeId = unManejador.ExisteUsuarioId(usuarioId);
            Assert.False(existeId);
        }
    }
}
