﻿using DominioGestorDispositivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PruebasXUnit
{
    public class PruebaTipoDeDispositivo
    {
        [Fact]
        public void PruebaConstructor()
        {
            Variable unaVariable = new Variable("Variable1", 1, 1);
            List<Variable> variables = new List<Variable>();
            variables.Add(unaVariable);
            TipoDeDispositivo unTipoDeDispositivo = new TipoDeDispositivo("Termostato", "una descripción", variables);
            Boolean nombreCorrecto = unTipoDeDispositivo.Nombre.Equals("Termostato");
            Boolean descripcionCorrecta = unTipoDeDispositivo.Descripcion.Equals("una descripción");
            Assert.True(nombreCorrecto && descripcionCorrecta);
        }

        [Fact]
        public void PruebaEqualTiposDeDispositivosIguales()
        {
            TipoDeDispositivo unTipoDeDispositivo = new TipoDeDispositivo();
            unTipoDeDispositivo.Nombre = "Termostato";
            TipoDeDispositivo elMismoTipoDeDispositivo = new TipoDeDispositivo();
            elMismoTipoDeDispositivo.Nombre = "Termostato";
            Assert.Equal(unTipoDeDispositivo, elMismoTipoDeDispositivo);
        }

        [Fact]
        public void PruebaEqualTiposDeDispositivosDistintos()
        {
            TipoDeDispositivo unTipoDeDispositivo = new TipoDeDispositivo();
            unTipoDeDispositivo.Nombre = "Termostato";
            unTipoDeDispositivo.TipoDeDispositivoId = 1;
            TipoDeDispositivo otroTipoDeDispositivo = new TipoDeDispositivo();
            otroTipoDeDispositivo.Nombre = "Timmer";
            unTipoDeDispositivo.TipoDeDispositivoId = 2;
            Assert.NotEqual(unTipoDeDispositivo, otroTipoDeDispositivo);
        }
    }
}
