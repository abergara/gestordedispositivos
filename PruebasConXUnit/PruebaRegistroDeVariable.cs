﻿using DominioGestorDispositivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PruebasXUnit
{
    public class PruebaRegistroDeVariable
    {
        [Fact]
        public void PruebaConstructor()
        {
            Variable unaVariable = new Variable("Variable1", 1, 1);
            List<Variable> variables = new List<Variable>();
            variables.Add(unaVariable);
            TipoDeDispositivo unTipoDeDispositivo = new TipoDeDispositivo("Termostato", "una descripción", variables);
            Dispositivo unDispositivo = new Dispositivo("unDispositivo", unTipoDeDispositivo);
            double unValor = 12;
            DateTime unaFecha = DateTime.Parse("13/10/1991");
            RegistroDeVariable unRegistroDeVariable = new RegistroDeVariable(unDispositivo.DispositivoId, unaVariable.VariableId, unValor, unaFecha);
            Boolean dispositivoCorrecto = unRegistroDeVariable.DispositivoId.Equals(unDispositivo.DispositivoId);
            Boolean variableCorrecta = unRegistroDeVariable.VariableId.Equals(unaVariable.VariableId);
            Boolean valorCorrecto = unRegistroDeVariable.Valor == unValor;
            Boolean fechaCorrecta = unRegistroDeVariable.FechaYHora.Equals(unaFecha);
            Assert.True(dispositivoCorrecto && variableCorrecta && valorCorrecto && fechaCorrecta);
        }
    }
}
