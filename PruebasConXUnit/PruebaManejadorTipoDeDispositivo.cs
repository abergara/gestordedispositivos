﻿using DominioGestorDispositivos;
using PersistenciaGestorDispositivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PruebasXUnit
{
    [Collection("MisPruebas")]
    public class PruebaManejadorTipoDeDispositivo
    {
        private List<TipoDeDispositivo> CrearTiposDeDispositivosDePrueba()
        {
            Variable unaVariable = new Variable("Variable1", 1, 1);
            List<Variable> variables = new List<Variable>();
            variables.Add(unaVariable);
            Variable variableSegundoTipoDeDispositivo = new Variable("Variable2", 1, 1);
            List<Variable> variablesDelSegundoTipoDeDispositivo = new List<Variable>();
            variablesDelSegundoTipoDeDispositivo.Add(variableSegundoTipoDeDispositivo);
            TipoDeDispositivo unTipoDeDispositivo = new TipoDeDispositivo("Termostato", "una descripción", variables);
            TipoDeDispositivo unTipoDeDispositivoDos = new TipoDeDispositivo("Timmer", "una descripción2", variablesDelSegundoTipoDeDispositivo);
            TipoDeDispositivo unTipoDeDispositivoTres = new TipoDeDispositivo("Sensor de movimiento", "una descripción3", variables);
            List<TipoDeDispositivo> tiposDeDispositivos = new List<TipoDeDispositivo>();
            tiposDeDispositivos.Add(unTipoDeDispositivo);
            tiposDeDispositivos.Add(unTipoDeDispositivoDos);
            tiposDeDispositivos.Add(unTipoDeDispositivoTres);
            return tiposDeDispositivos;
        }

        private void AgregarTiposDeDispositivosDePrueba()
        {
            List<TipoDeDispositivo> tiposDeDispositivos = CrearTiposDeDispositivosDePrueba();
            TipoDeDispositivo unTipoDeDispositivo = tiposDeDispositivos.ElementAt(0);
            TipoDeDispositivo unTipoDeDispositivoDos = tiposDeDispositivos.ElementAt(1);
            TipoDeDispositivo unTipoDeDispositivoTres = tiposDeDispositivos.ElementAt(2);
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos.Add(unTipoDeDispositivo);
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos.Add(unTipoDeDispositivoDos);
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos.Add(unTipoDeDispositivoTres);
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
        }


        private void EliminarTiposDeDispositivosDePrueba()
        {
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                foreach (var variable in persistenciaBaseDatosGestorDispositivos.Variables)
                {
                    persistenciaBaseDatosGestorDispositivos.Variables.Remove(variable);
                }
                foreach (var tipoDeDispositivo in persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos)
                {
                    persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos.Remove(tipoDeDispositivo);
                }
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
        }

        private void EliminarDatosDePrueba()
        {
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                foreach (var variable in persistenciaBaseDatosGestorDispositivos.Variables)
                {
                    persistenciaBaseDatosGestorDispositivos.Variables.Remove(variable);
                }
                foreach (var dispositivo in persistenciaBaseDatosGestorDispositivos.Dispositivos)
                {
                    persistenciaBaseDatosGestorDispositivos.Dispositivos.Remove(dispositivo);
                }
                foreach (var tipoDeDispositivo in persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos)
                {
                    persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos.Remove(tipoDeDispositivo);
                }
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
        }

        [Fact]
        public void PruebaObtenerTodosLosTiposDeDispositivos()
        {
            ManejadorTipoDeDispositivo unManejador = new ManejadorTipoDeDispositivo();
            AgregarTiposDeDispositivosDePrueba();
            List<TipoDeDispositivo> tiposDeDispositivosAgregados = unManejador.ObtenerTodosLosTiposDeDispositivos();
            List<TipoDeDispositivo> tiposDeDispositivos = CrearTiposDeDispositivosDePrueba();
            Boolean tiposDeDispositivosBienObtenidos = tiposDeDispositivos.ElementAt(0).Equals(tiposDeDispositivosAgregados.ElementAt(0));
            tiposDeDispositivosBienObtenidos = tiposDeDispositivosBienObtenidos && tiposDeDispositivos.ElementAt(1).Equals(tiposDeDispositivosAgregados.ElementAt(1));
            tiposDeDispositivosBienObtenidos = tiposDeDispositivosBienObtenidos && tiposDeDispositivos.ElementAt(2).Equals(tiposDeDispositivosAgregados.ElementAt(2));
            EliminarTiposDeDispositivosDePrueba();
            Assert.True(tiposDeDispositivosBienObtenidos);
        }

        [Fact]
        public void PruebaObtenerTodosLosTiposDeDispositivosSinTiposDeDispositivosAgregados()
        {
            ManejadorTipoDeDispositivo unManejador = new ManejadorTipoDeDispositivo();
            Assert.Throws<System.ArgumentException>(
               delegate
               {
                   unManejador.ObtenerTodosLosTiposDeDispositivos();

               });
        }

        [Fact]
        public void PruebaAgregarTipoDeDispositivo()
        {
            ManejadorTipoDeDispositivo unManejadorTipoDeDispositivo = new ManejadorTipoDeDispositivo();
            ManejadorVariable unManejadorVariable = new ManejadorVariable();
            List<TipoDeDispositivo> tiposDeDispositivo = CrearTiposDeDispositivosDePrueba();
            TipoDeDispositivo unTipoDeDispositivo = tiposDeDispositivo.ElementAt(0);
            Variable variableDelDispositivo = unTipoDeDispositivo.Variables.ElementAt(0);
            unManejadorVariable.AgregarVariable(variableDelDispositivo);
            unManejadorTipoDeDispositivo.AgregarTipoDeDispositivo(unTipoDeDispositivo);
            TipoDeDispositivo tipoDeDispositivoAgregado = unManejadorTipoDeDispositivo.ObtenerTodosLosTiposDeDispositivos().ElementAt(0);
            Boolean tipoDeDispositivoBienAgregado = tipoDeDispositivoAgregado.Nombre.Equals("Termostato");
            tipoDeDispositivoBienAgregado = tipoDeDispositivoBienAgregado && tipoDeDispositivoAgregado.Descripcion.Equals("una descripción");
            unManejadorTipoDeDispositivo.EliminarTipoDeDispositivo(tipoDeDispositivoAgregado);
            unManejadorVariable.EliminarVariable(variableDelDispositivo);
            Assert.True(tipoDeDispositivoBienAgregado);
        }

        [Fact]
        public void PruebaAgregarTipoDeDispositivoConTipoDeDispositivoExistente()
        {
            ManejadorTipoDeDispositivo unManejadorTipoDeDispositivo = new ManejadorTipoDeDispositivo();
            ManejadorVariable unManejadorVariable = new ManejadorVariable();
            List<TipoDeDispositivo> tiposDeDispositivo = CrearTiposDeDispositivosDePrueba();
            TipoDeDispositivo unTipoDeDispositivo = tiposDeDispositivo.ElementAt(0);
            Variable variableDelDispositivo = unTipoDeDispositivo.Variables.ElementAt(0);
            unManejadorVariable.AgregarVariable(variableDelDispositivo);
            unManejadorTipoDeDispositivo.AgregarTipoDeDispositivo(unTipoDeDispositivo);
            TipoDeDispositivo tipoDeDispositivoAgregado = unManejadorTipoDeDispositivo.ObtenerTodosLosTiposDeDispositivos().ElementAt(0);
            Assert.Throws<System.ArgumentException>(
              delegate
              {
                  unManejadorTipoDeDispositivo.AgregarTipoDeDispositivo(tipoDeDispositivoAgregado);

              });
            unManejadorTipoDeDispositivo.EliminarTipoDeDispositivo(tipoDeDispositivoAgregado);
            unManejadorVariable.EliminarVariable(variableDelDispositivo);
        }

        [Fact]
        public void PruebaAgregarTipoDeDispositivoSinVariables()
        {
            ManejadorTipoDeDispositivo unManejadorTipoDeDispositivo = new ManejadorTipoDeDispositivo();
            List<Variable> variables = new List<Variable>();
            TipoDeDispositivo unTipoDeDispositivo = new TipoDeDispositivo("nombre", "descripcion", variables);
            Assert.Throws<System.ArgumentException>(
              delegate
              {
                  unManejadorTipoDeDispositivo.AgregarTipoDeDispositivo(unTipoDeDispositivo);

              });
        }

        [Fact]
        public void PruebaAgregarTipoDeDispositivoConVariablesDeIgualNombre()
        {
            ManejadorTipoDeDispositivo unManejadorTipoDeDispositivo = new ManejadorTipoDeDispositivo();
            ManejadorVariable unManejadorVariable = new ManejadorVariable();
            List<TipoDeDispositivo> tiposDeDispositivo = CrearTiposDeDispositivosDePrueba();
            TipoDeDispositivo unTipoDeDispositivo = tiposDeDispositivo.ElementAt(0);
            Variable variableDelDispositivo = unTipoDeDispositivo.Variables.ElementAt(0);
            unManejadorVariable.AgregarVariable(variableDelDispositivo);
            unTipoDeDispositivo.Variables.Add(variableDelDispositivo);
            Assert.Throws<System.ArgumentException>(
              delegate
              {
                  unManejadorTipoDeDispositivo.AgregarTipoDeDispositivo(unTipoDeDispositivo);

              });
            unManejadorVariable.EliminarVariable(variableDelDispositivo);
        }

        [Fact]
        public void PruebaModificarTipoDeDispositivo()
        {
            ManejadorTipoDeDispositivo unManejadorTipoDeDispositivo = new ManejadorTipoDeDispositivo();
            ManejadorVariable unManejadorVariable = new ManejadorVariable();
            List<TipoDeDispositivo> tiposDeDispositivo = CrearTiposDeDispositivosDePrueba();
            TipoDeDispositivo unTipoDeDispositivo = tiposDeDispositivo.ElementAt(0);
            Variable variableDelDispositivo = unTipoDeDispositivo.Variables.ElementAt(0);
            unManejadorVariable.AgregarVariable(variableDelDispositivo);
            unManejadorTipoDeDispositivo.AgregarTipoDeDispositivo(unTipoDeDispositivo);
            TipoDeDispositivo tipoDeDispositivoAgregado = unManejadorTipoDeDispositivo.ObtenerTodosLosTiposDeDispositivos().ElementAt(0);
            tipoDeDispositivoAgregado.Descripcion = "tipo de prueba";
            unManejadorTipoDeDispositivo.ModificarTipoDeDispositivo(tipoDeDispositivoAgregado);
            TipoDeDispositivo tipoDeDispositivoModificado = unManejadorTipoDeDispositivo.ObtenerTodosLosTiposDeDispositivos().ElementAt(0);
            Boolean tipoDeDispositivoBienModificado = tipoDeDispositivoModificado.Descripcion.Equals("tipo de prueba");
            unManejadorTipoDeDispositivo.EliminarTipoDeDispositivo(tipoDeDispositivoAgregado);
            unManejadorVariable.EliminarVariable(variableDelDispositivo);
            Assert.True(tipoDeDispositivoBienModificado);
        }

        [Fact]
        public void PruebaModificarTipoDeDispositivoConTipoDeDispositivoExistente()
        {
            ManejadorTipoDeDispositivo unManejador = new ManejadorTipoDeDispositivo();
            ManejadorVariable unManejadorVariable = new ManejadorVariable();
            List<TipoDeDispositivo> tiposDeDispositivo = CrearTiposDeDispositivosDePrueba();
            TipoDeDispositivo unTipoDeDispositivo = tiposDeDispositivo.ElementAt(0);
            Variable variableDelDispositivo = unTipoDeDispositivo.Variables.ElementAt(0);
            TipoDeDispositivo otroTipoDeDispositivo = tiposDeDispositivo.ElementAt(1);
            Variable variableDelOtroDispositivo = otroTipoDeDispositivo.Variables.ElementAt(0);
            unManejadorVariable.AgregarVariable(variableDelDispositivo);
            unManejadorVariable.AgregarVariable(variableDelOtroDispositivo);
            unManejador.AgregarTipoDeDispositivo(unTipoDeDispositivo);
            unManejador.AgregarTipoDeDispositivo(otroTipoDeDispositivo);
            TipoDeDispositivo primerTipoDeDispositivoAgregado = unManejador.ObtenerTodosLosTiposDeDispositivos().ElementAt(0);
            TipoDeDispositivo segundoTipoDeDispositivoAgregado = unManejador.ObtenerTodosLosTiposDeDispositivos().ElementAt(1);
            segundoTipoDeDispositivoAgregado.Nombre = "Termostato";
            Assert.Throws<System.ArgumentException>(
              delegate
              {
                  unManejador.ModificarTipoDeDispositivo(segundoTipoDeDispositivoAgregado);

              });
            unManejador.EliminarTipoDeDispositivo(segundoTipoDeDispositivoAgregado);
            unManejador.EliminarTipoDeDispositivo(primerTipoDeDispositivoAgregado);
            unManejadorVariable.EliminarVariable(variableDelDispositivo);
            unManejadorVariable.EliminarVariable(variableDelOtroDispositivo);
        }

        [Fact]
        public void PruebaModificarTipoDeDispositivoConTipoDeDispositivoNoExistente()
        {
            ManejadorTipoDeDispositivo unManejador = new ManejadorTipoDeDispositivo();
            List<TipoDeDispositivo> tiposDeDispositivo = CrearTiposDeDispositivosDePrueba();
            TipoDeDispositivo unTipoDeDispositivo = tiposDeDispositivo.ElementAt(0);
            Assert.Throws<System.ArgumentException>(
              delegate
              {
                  unManejador.ModificarTipoDeDispositivo(unTipoDeDispositivo);

              });
        }

        [Fact]
        public void PruebaModificarTipoDeDispositivoConVariablesDeIgualNombre()
        {
            ManejadorTipoDeDispositivo unManejadorTipoDeDispositivo = new ManejadorTipoDeDispositivo();
            ManejadorVariable unManejadorVariable = new ManejadorVariable();
            List<TipoDeDispositivo> tiposDeDispositivo = CrearTiposDeDispositivosDePrueba();
            TipoDeDispositivo unTipoDeDispositivo = tiposDeDispositivo.ElementAt(0);
            Variable variableDelDispositivo = unTipoDeDispositivo.Variables.ElementAt(0);
            unManejadorVariable.AgregarVariable(variableDelDispositivo);
            unManejadorTipoDeDispositivo.AgregarTipoDeDispositivo(unTipoDeDispositivo);
            TipoDeDispositivo tipoDeDispositivoAgregado = unManejadorTipoDeDispositivo.ObtenerTodosLosTiposDeDispositivos().ElementAt(0);
            tipoDeDispositivoAgregado.Descripcion = "tipo de prueba";
            unManejadorTipoDeDispositivo.ModificarTipoDeDispositivo(tipoDeDispositivoAgregado);
            tipoDeDispositivoAgregado.Variables.Add(variableDelDispositivo);
            tipoDeDispositivoAgregado.Variables.Add(variableDelDispositivo);
            Assert.Throws<System.ArgumentException>(
              delegate
              {
                  unManejadorTipoDeDispositivo.ModificarTipoDeDispositivo(tipoDeDispositivoAgregado);

              });
            unManejadorTipoDeDispositivo.EliminarTipoDeDispositivo(tipoDeDispositivoAgregado);
            unManejadorVariable.EliminarVariable(variableDelDispositivo);
        }

        [Fact]
        public void PruebaEliminarTipoDeDispositivo()
        {
            ManejadorTipoDeDispositivo unManejadorTipoDeDispositivo = new ManejadorTipoDeDispositivo();
            ManejadorVariable unManejadorVariable = new ManejadorVariable();
            List<TipoDeDispositivo> tiposDeDispositivo = CrearTiposDeDispositivosDePrueba();
            TipoDeDispositivo unTipoDeDispositivo = tiposDeDispositivo.ElementAt(0);
            TipoDeDispositivo otroTipoDeDispositivo = tiposDeDispositivo.ElementAt(1);
            otroTipoDeDispositivo.Nombre = "otro tipo de dispositivo";
            Variable variableDelDispositivo = unTipoDeDispositivo.Variables.ElementAt(0);
            Variable variableDelOtroDispositivo = otroTipoDeDispositivo.Variables.ElementAt(0);
            unManejadorVariable.AgregarVariable(variableDelDispositivo);
            unManejadorVariable.AgregarVariable(variableDelOtroDispositivo);
            unManejadorTipoDeDispositivo.AgregarTipoDeDispositivo(unTipoDeDispositivo);
            unManejadorTipoDeDispositivo.AgregarTipoDeDispositivo(otroTipoDeDispositivo);
            TipoDeDispositivo tipoDeDispositivoAgregado = unManejadorTipoDeDispositivo.ObtenerTodosLosTiposDeDispositivos().ElementAt(0);
            unManejadorTipoDeDispositivo.EliminarTipoDeDispositivo(tipoDeDispositivoAgregado);
            List<TipoDeDispositivo> tiposDeDispositivoAgregados = unManejadorTipoDeDispositivo.ObtenerTodosLosTiposDeDispositivos();
            Boolean tipoDeDispositivoBienEliminado = tiposDeDispositivoAgregados.Count == 1;
            TipoDeDispositivo elUnicoTipoDeDispositivoAgregado = tiposDeDispositivoAgregados.ElementAt(0);
            tipoDeDispositivoBienEliminado = tipoDeDispositivoBienEliminado && elUnicoTipoDeDispositivoAgregado.Nombre.Equals("otro tipo de dispositivo");
            unManejadorTipoDeDispositivo.EliminarTipoDeDispositivo(elUnicoTipoDeDispositivoAgregado);
            unManejadorVariable.EliminarVariable(variableDelDispositivo);
            unManejadorVariable.EliminarVariable(variableDelOtroDispositivo);
            Assert.True(tipoDeDispositivoBienEliminado);
        }

        [Fact]
        public void PruebaEliminarTipoDeDispositivoConTipoDeDispositivoNoExistente()
        {
            ManejadorTipoDeDispositivo unManejador = new ManejadorTipoDeDispositivo();
            List<TipoDeDispositivo> tiposDeDispositivo = CrearTiposDeDispositivosDePrueba();
            TipoDeDispositivo unTipoDeDispositivo = tiposDeDispositivo.ElementAt(0);
            Assert.Throws<System.ArgumentException>(
              delegate
              {
                  unManejador.EliminarTipoDeDispositivo(unTipoDeDispositivo);

              });
        }

        [Fact]
        public void PruebaObtenerTipoDeDispositivo()
        {
            ManejadorTipoDeDispositivo unManejadorTipoDeDispositivo = new ManejadorTipoDeDispositivo();
            ManejadorVariable unManejadorVariable = new ManejadorVariable();
            List<TipoDeDispositivo> tiposDeDispositivo = CrearTiposDeDispositivosDePrueba();
            TipoDeDispositivo unTipoDeDispositivo = tiposDeDispositivo.ElementAt(0);
            Variable variableDelDispositivo = unTipoDeDispositivo.Variables.ElementAt(0);
            unManejadorVariable.AgregarVariable(variableDelDispositivo);
            unManejadorTipoDeDispositivo.AgregarTipoDeDispositivo(unTipoDeDispositivo);
            TipoDeDispositivo tipoDeDispositivoAgregado = unManejadorTipoDeDispositivo.ObtenerTodosLosTiposDeDispositivos().ElementAt(0);
            TipoDeDispositivo tipoDeDispositivoObtenido = unManejadorTipoDeDispositivo.ObtenerTipoDeDispositivo(tipoDeDispositivoAgregado);
            Boolean tipoDeDispositivoBienObtenido = tipoDeDispositivoObtenido.Nombre.Equals("Termostato");
            tipoDeDispositivoBienObtenido = tipoDeDispositivoBienObtenido && tipoDeDispositivoObtenido.Descripcion.Equals("una descripción");
            unManejadorTipoDeDispositivo.EliminarTipoDeDispositivo(tipoDeDispositivoObtenido);
            unManejadorVariable.EliminarVariable(variableDelDispositivo);
            Assert.True(tipoDeDispositivoBienObtenido);
        }

        [Fact]
        public void PruebaObtenerTipoDeDispositivoConTipoDeDispositivoNoExistente()
        {
            ManejadorTipoDeDispositivo unManejador = new ManejadorTipoDeDispositivo();
            List<TipoDeDispositivo> tiposDeDispositivo = CrearTiposDeDispositivosDePrueba();
            TipoDeDispositivo unTipoDeDispositivo = tiposDeDispositivo.ElementAt(0);
            Assert.Throws<System.ArgumentException>(
              delegate
              {
                  unManejador.ObtenerTipoDeDispositivo(unTipoDeDispositivo);

              });
        }

        [Fact]
        public void PruebaTipoDeDispositivoEstaAsociadoAAlgunDispositivo()
        {
            ManejadorTipoDeDispositivo unManejadorTipoDeDispositivo = new ManejadorTipoDeDispositivo();
            ManejadorVariable unManejadorVariable = new ManejadorVariable();
            ManejadorUsuario unManejadorUsuario = new ManejadorUsuario();
            ManejadorDispositivo unManejadorDispositivo = new ManejadorDispositivo();
            List<TipoDeDispositivo> tiposDeDispositivo = CrearTiposDeDispositivosDePrueba();
            TipoDeDispositivo unTipoDeDispositivo = tiposDeDispositivo.ElementAt(0);
            Variable variableDelDispositivo = unTipoDeDispositivo.Variables.ElementAt(0);
            unManejadorVariable.AgregarVariable(variableDelDispositivo);
            unManejadorTipoDeDispositivo.AgregarTipoDeDispositivo(unTipoDeDispositivo);
            TipoDeDispositivo tipoDeDispositivoAgregado = unManejadorTipoDeDispositivo.ObtenerTodosLosTiposDeDispositivos().ElementAt(0);
            Variable variableDelTipo = tipoDeDispositivoAgregado.Variables.ElementAt(0);
            Usuario unUsuario = new Usuario("nombre", "email@email.com", "clave");
            unManejadorUsuario.AgregarUsuario(unUsuario);
            Usuario usuarioAgregado = unManejadorUsuario.ObtenerUsuario("email@email.com");
            Dispositivo unDispositivo = new Dispositivo("nombre", tipoDeDispositivoAgregado);
            unManejadorDispositivo.AgregarDispositivo(usuarioAgregado.UsuarioId, unDispositivo);
            Boolean tipoDeDispositivoEstaAsociado = unManejadorTipoDeDispositivo.TipoDeDispositivoEstaAsociadoAAlgunDispositivo(unTipoDeDispositivo);
            List<Dispositivo> dispositivos = unManejadorDispositivo.ObtenerTodosLosDispositivos();
            EliminarDatosDePrueba();
            unManejadorUsuario.EliminarUsuario(usuarioAgregado);
            Assert.True(tipoDeDispositivoEstaAsociado);
        }

        [Fact]
        public void PruebaTipoDeDispositivoEstaAsociadoAAlgunDispositivoSinTipoAsociado()
        {
            ManejadorTipoDeDispositivo unManejadorTipoDeDispositivo = new ManejadorTipoDeDispositivo();
            ManejadorVariable unManejadorVariable = new ManejadorVariable();
            ManejadorUsuario unManejadorUsuario = new ManejadorUsuario();
            ManejadorDispositivo unManejadorDispositivo = new ManejadorDispositivo();
            List<TipoDeDispositivo> tiposDeDispositivo = CrearTiposDeDispositivosDePrueba();
            TipoDeDispositivo unTipoDeDispositivo = tiposDeDispositivo.ElementAt(0);
            Variable variableDelDispositivo = unTipoDeDispositivo.Variables.ElementAt(0);
            unManejadorVariable.AgregarVariable(variableDelDispositivo);
            unManejadorTipoDeDispositivo.AgregarTipoDeDispositivo(unTipoDeDispositivo);
            TipoDeDispositivo tipoDeDispositivoAgregado = unManejadorTipoDeDispositivo.ObtenerTodosLosTiposDeDispositivos().ElementAt(0);
            Variable variableDelTipo = tipoDeDispositivoAgregado.Variables.ElementAt(0);
            Usuario unUsuario = new Usuario("nombre", "email@email.com", "clave");
            unManejadorUsuario.AgregarUsuario(unUsuario);
            Usuario usuarioAgregado = unManejadorUsuario.ObtenerUsuario("email@email.com");
            Dispositivo unDispositivo = new Dispositivo("nombre", tipoDeDispositivoAgregado);
            unManejadorDispositivo.AgregarDispositivo(usuarioAgregado.UsuarioId, unDispositivo);
            unTipoDeDispositivo.Nombre = "tipo no asociado";
            unTipoDeDispositivo.TipoDeDispositivoId = tipoDeDispositivoAgregado.TipoDeDispositivoId - 1;
            Boolean tipoDeDispositivoEstaAsociado = unManejadorTipoDeDispositivo.TipoDeDispositivoEstaAsociadoAAlgunDispositivo(unTipoDeDispositivo);
            List<Dispositivo> dispositivos = unManejadorDispositivo.ObtenerTodosLosDispositivos();
            EliminarDatosDePrueba();
            unManejadorUsuario.EliminarUsuario(usuarioAgregado);
            Assert.False(tipoDeDispositivoEstaAsociado);
        }

        [Fact]
        public void PruebaTipoDeDispositivoEstaAsociadoAAlgunDispositivoSinDispositivos()
        {
            ManejadorTipoDeDispositivo unManejadorTipoDeDispositivo = new ManejadorTipoDeDispositivo();
            List<TipoDeDispositivo> tiposDeDispositivo = CrearTiposDeDispositivosDePrueba();
            TipoDeDispositivo unTipoDeDispositivo = tiposDeDispositivo.ElementAt(0);
            Boolean tipoDeDispositivoEstaAsociado = unManejadorTipoDeDispositivo.TipoDeDispositivoEstaAsociadoAAlgunDispositivo(unTipoDeDispositivo);
            Assert.False(tipoDeDispositivoEstaAsociado);
        }
    }
}
