﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using DominioGestorDispositivos;

namespace PruebasXUnit
{
    public class PruebaUsuario
    {
        private Dispositivo CrearDispositivoDePrueba()
        {
            TipoDeDispositivo unTipoDeDispositivo = new TipoDeDispositivo();
            unTipoDeDispositivo.Nombre = "Termostato";
            unTipoDeDispositivo.Descripcion = "Dispositivo para medir la temperatura..";
            Dispositivo unDispositivo = new Dispositivo("Nombre de dispositivo", unTipoDeDispositivo);
            return unDispositivo;
        }

        [Fact]
        public void PruebaEqualsUsuariosIguales()
        {
            Usuario unUsuario = new Usuario("Carlos Rodriguez", "carrod@hotmail.com","clave1");
            Usuario elMismoUsuario = new Usuario("Carlos Rodriguez", "carrod@hotmail.com", "clave1");
            Assert.Equal(unUsuario, elMismoUsuario);
        }

        [Fact]
        public void PruebaEqualsUsuariosDistintos()
        {
            Usuario unUsuario = new Usuario("Carlos Rodriguez", "carrod@hotmail.com", "clave1");
            Usuario otroUsuario = new Usuario("Esteban Machado", "estumachad@hotmail.com", "clave2"); 
            Assert.NotEqual(unUsuario, otroUsuario);
        }

        [Fact]
        public void PruebaConstructor()
        {
            Usuario unUsuario = new Usuario("Carlos Rodriguez", "carrod@hotmail.com", "clave1");
            Boolean nombreCorrecto = unUsuario.Nombre.Equals("Carlos Rodriguez");
            Boolean emailCorrecto = unUsuario.Email.Equals("carrod@hotmail.com");
            Boolean claveCorrecta = unUsuario.Clave.Equals("clave1");
            Assert.True(nombreCorrecto && emailCorrecto && claveCorrecta);
        }

        [Fact]
        public void PruebaAgregarDispositivo()
        {
            Usuario unUsuario = new Usuario("Carlos Rodriguez", "carrod@hotmail.com", "clave1");
            Dispositivo unDispositivo = CrearDispositivoDePrueba();
            unUsuario.AgregarDispositivo(unDispositivo);
            Dispositivo dispositivoAgregado = unUsuario.Dispositivos.ElementAt(0);
            Assert.Equal(unDispositivo, dispositivoAgregado);
        }

        [Fact]
        public void PruebaModificarDispositivo()
        {
            Usuario unUsuario = new Usuario("Carlos Rodriguez", "carrod@hotmail.com", "clave1");
            Dispositivo unDispositivo = CrearDispositivoDePrueba();
            unUsuario.AgregarDispositivo(unDispositivo);
            Dispositivo dispositivoModificado = CrearDispositivoDePrueba();
            dispositivoModificado.Nombre = "Dispositivo Modificado";
            unUsuario.ModificarDispositivo(unDispositivo, dispositivoModificado);
            Dispositivo dispositivoBienModificado = unUsuario.Dispositivos.ElementAt(0);
            Assert.Equal(dispositivoModificado, dispositivoBienModificado);
        }

        [Fact]
        public void PruebaEliminarDispositivo()
        {
            Usuario unUsuario = new Usuario("Carlos Rodriguez", "carrod@hotmail.com", "clave1");
            Dispositivo unDispositivo = CrearDispositivoDePrueba();
            unUsuario.AgregarDispositivo(unDispositivo);
            unUsuario.EliminarDispositivo(unDispositivo);
            int cantidadDeDispositivos = unUsuario.Dispositivos.Count;
            Assert.Equal(cantidadDeDispositivos, 0);
        }
    }
}
