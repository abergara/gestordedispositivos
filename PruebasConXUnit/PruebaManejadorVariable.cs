﻿using DominioGestorDispositivos;
using PersistenciaGestorDispositivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PruebasXUnit
{
    [Collection("MisPruebas")]
    public class PruebaManejadorVariable
    {
        private List<TipoDeDispositivo> CrearTiposDeDispositivosDePrueba()
        {
            Variable unaVariable = new Variable("Variable1", 1, 1);
            List<Variable> variables = new List<Variable>();
            variables.Add(unaVariable);
            TipoDeDispositivo unTipoDeDispositivo = new TipoDeDispositivo("Termostato", "una descripción", variables);
            TipoDeDispositivo unTipoDeDispositivoDos = new TipoDeDispositivo("Timmer", "una descripción2", variables);
            TipoDeDispositivo unTipoDeDispositivoTres = new TipoDeDispositivo("Sensor de movimiento", "una descripción3", variables);
            List<TipoDeDispositivo> tiposDeDispositivos = new List<TipoDeDispositivo>();
            tiposDeDispositivos.Add(unTipoDeDispositivo);
            tiposDeDispositivos.Add(unTipoDeDispositivoDos);
            tiposDeDispositivos.Add(unTipoDeDispositivoTres);
            return tiposDeDispositivos;
        }

        private void AgregarTiposDeDispositivosDePrueba()
        {
            List<TipoDeDispositivo> tiposDeDispositivos = CrearTiposDeDispositivosDePrueba();
            TipoDeDispositivo unTipoDeDispositivo = tiposDeDispositivos.ElementAt(0);
            TipoDeDispositivo unTipoDeDispositivoDos = tiposDeDispositivos.ElementAt(1);
            TipoDeDispositivo unTipoDeDispositivoTres = tiposDeDispositivos.ElementAt(2);
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos.Add(unTipoDeDispositivo);
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos.Add(unTipoDeDispositivoDos);
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos.Add(unTipoDeDispositivoTres);
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
        }


        private void EliminarTiposDeDispositivosDePrueba()
        {
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                foreach (var variable in persistenciaBaseDatosGestorDispositivos.Variables)
                {
                    persistenciaBaseDatosGestorDispositivos.Variables.Remove(variable);
                }
                foreach (var tipoDeDispositivo in persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos)
                {
                    persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos.Remove(tipoDeDispositivo);
                }
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
        }

        [Fact]
        public void PruebaAgregarVariable()
        {
            ManejadorVariable unManejador = new ManejadorVariable();
            Variable unaVariable = new Variable("Variable1", 1, 1);
            unManejador.AgregarVariable(unaVariable);
            List<Variable> variables = unManejador.ObtenerTodasLasVariables();
            Boolean variableBienAgregada = variables.Contains(unaVariable);
            unManejador.EliminarVariable(unaVariable);
            Assert.True(variableBienAgregada);
        }

        [Fact]
        public void PruebaAgregarVariableConMinimoMayorAMaximo()
        {
            ManejadorVariable unManejador = new ManejadorVariable();
            Variable unaVariable = new Variable("Variable1", 3, 1);
            Assert.Throws<System.ArgumentException>(
               delegate
               {
                   unManejador.AgregarVariable(unaVariable);
               });
        }

        [Fact]
        public void PruebaModificarVariable()
        {
            ManejadorVariable unManejador = new ManejadorVariable();
            Variable unaVariable = new Variable("Variable1", 1, 1);
            unManejador.AgregarVariable(unaVariable);
            Variable variableModificada = new Variable("Variable1", 1, 4);
            List<Variable> variables = unManejador.ObtenerTodasLasVariables();
            Variable variableAgregada = variables.ElementAt(0);
            variableModificada.VariableId = variableAgregada.VariableId;
            unManejador.ModificarVariable(variableModificada);
            variables = unManejador.ObtenerTodasLasVariables();
            Variable variableObtenida = variables.ElementAt(0);
            Boolean variableBienModificada = variableObtenida.Maximo == 4;
            unManejador.EliminarVariable(unaVariable);
            Assert.True(variableBienModificada);
        }

        [Fact]
        public void PruebaModificarVariableConVariableNoExistente()
        {
            ManejadorVariable unManejador = new ManejadorVariable();
            Variable unaVariable = new Variable();
            unaVariable.Nombre = "Variable";
            Assert.Throws<System.ArgumentException>(
             delegate
             {
                 unManejador.ModificarVariable(unaVariable);
             });
        }
        
        [Fact]
        public void PruebaEliminarVariable()
        {
            ManejadorVariable unManejador = new ManejadorVariable();
            Variable unaVariable = new Variable("Variable1", 1, 1);
            Variable otraVariable = new Variable("Otra variable", 1, 1);
            unManejador.AgregarVariable(unaVariable);
            unManejador.AgregarVariable(otraVariable); 
            unManejador.EliminarVariable(unaVariable);
            List<Variable> variables = unManejador.ObtenerTodasLasVariables();
            Boolean variableBienEliminada = variables.Count == 1;
            variableBienEliminada = variableBienEliminada && variables.ElementAt(0).Nombre.Equals("Otra variable");
            unManejador.EliminarVariable(otraVariable);
            Assert.True(variableBienEliminada);
        }

        [Fact]
        public void PruebaEliminarVariableConVariableNoExistente()
        {
            ManejadorVariable unManejador = new ManejadorVariable();
            Variable unaVariable = new Variable("Variable1", 1, 3);
            Assert.Throws<System.ArgumentException>(
               delegate
               {
                   unManejador.EliminarVariable(unaVariable);
               });
        }

        [Fact]
        public void PruebaObtenerTodasLasVariables()
        {
            ManejadorVariable unManejador = new ManejadorVariable();
            Variable unaVariable = new Variable("Variable1", 1, 1);
            Variable otraVariable = new Variable("Variable2", 1, 1);
            unManejador.AgregarVariable(unaVariable);
            unManejador.AgregarVariable(otraVariable);
            List<Variable> variablesAgregadas = unManejador.ObtenerTodasLasVariables();
            Boolean variablesBienObtenidas = unaVariable.Nombre.Equals(variablesAgregadas.ElementAt(0).Nombre);
            variablesBienObtenidas = variablesBienObtenidas && otraVariable.Nombre.Equals(variablesAgregadas.ElementAt(1).Nombre);
            unManejador.EliminarVariable(unaVariable);
            unManejador.EliminarVariable(otraVariable);
            Assert.True(variablesBienObtenidas);
        }

        [Fact]
        public void PruebaObtenerTodasLasVariablesSinVariablesAgregadas()
        {
            ManejadorVariable unManejador = new ManejadorVariable();
            Assert.Throws<System.ArgumentException>(
               delegate
               {
                   unManejador.ObtenerTodasLasVariables();
               });
        }

        [Fact]
        public void PruebaObtenerTodasLasVariablesQueNoEstanAsociadasAAlgunTipoDeDispositivo()
        {
            ManejadorVariable unManejador = new ManejadorVariable();
            Variable unaVariable = new Variable("Variable1", 1, 1);
            Variable otraVariable = new Variable("Variable2", 1, 1);
            unManejador.AgregarVariable(unaVariable);
            unManejador.AgregarVariable(otraVariable);
            List<Variable> variablesAgregadas = unManejador.ObtenerTodasLasVariablesQueNoEstanAsociadasAAlgunTipoDeDispositivo();
            Boolean variablesBienObtenidas = unaVariable.Nombre.Equals(variablesAgregadas.ElementAt(0).Nombre);
            variablesBienObtenidas = variablesBienObtenidas && otraVariable.Nombre.Equals(variablesAgregadas.ElementAt(1).Nombre);
            unManejador.EliminarVariable(unaVariable);
            unManejador.EliminarVariable(otraVariable);
            Assert.True(variablesBienObtenidas);
        }

        [Fact]
        public void PruebaObtenerTodasLasVariablesQueNoEstanAsociadasAAlgunTipoDeDispositivoSinVariablesAgregadas()
        {
            ManejadorVariable unManejador = new ManejadorVariable();
            Assert.Throws<System.ArgumentException>(
               delegate
               {
                   unManejador.ObtenerTodasLasVariablesQueNoEstanAsociadasAAlgunTipoDeDispositivo();
               });
        }

        [Fact]
        public void PruebaVariablePerteneceATipoDeDispositivo()
        {
            ManejadorTipoDeDispositivo unManejadorTipoDeDispositivo = new ManejadorTipoDeDispositivo();
            ManejadorVariable unManejadorVariable = new ManejadorVariable();
            AgregarTiposDeDispositivosDePrueba();
            List<TipoDeDispositivo> tiposDeDispositivosAgregados = unManejadorTipoDeDispositivo.ObtenerTodosLosTiposDeDispositivos();
            TipoDeDispositivo unTipoDeDispositivo = tiposDeDispositivosAgregados.ElementAt(0);
            Variable unaVariable = unTipoDeDispositivo.Variables.ElementAt(0);
            Boolean variablePertenece = unManejadorVariable.VariablePerteneceATipoDeDispositivo(unTipoDeDispositivo, unaVariable);
            EliminarTiposDeDispositivosDePrueba();
            Assert.True(variablePertenece);
        }

        [Fact]
        public void PruebaVariablePerteneceATipoDeDispositivoConVariableNoPerteneciente()
        {
            ManejadorTipoDeDispositivo unManejadorTipoDeDispositivo = new ManejadorTipoDeDispositivo();
            ManejadorVariable unManejadorVariable = new ManejadorVariable();
            AgregarTiposDeDispositivosDePrueba();
            List<TipoDeDispositivo> tiposDeDispositivosAgregados = unManejadorTipoDeDispositivo.ObtenerTodosLosTiposDeDispositivos();
            TipoDeDispositivo unTipoDeDispositivo = tiposDeDispositivosAgregados.ElementAt(0);
            TipoDeDispositivo otroTipoDeDispositivo = tiposDeDispositivosAgregados.ElementAt(1);
            Variable unaVariable = otroTipoDeDispositivo.Variables.ElementAt(0);
            Boolean variablePertenece = unManejadorVariable.VariablePerteneceATipoDeDispositivo(unTipoDeDispositivo, unaVariable);
            EliminarTiposDeDispositivosDePrueba();
            Assert.False(variablePertenece);
        }

        [Fact]
        public void PruebaObtenerVariable()
        {
            ManejadorVariable unManejador = new ManejadorVariable();
            Variable unaVariable = new Variable("Variable1", 1, 3);
            unManejador.AgregarVariable(unaVariable);
            Variable variableObtenida = unManejador.ObtenerVariable(unaVariable);
            Boolean variableBienObtenida = unaVariable.Equals(variableObtenida);
            unManejador.EliminarVariable(unaVariable);
            Assert.True(variableBienObtenida);
        }

        [Fact]
        public void PruebaObtenerVariableConVariableNoExistente()
        {
            ManejadorVariable unManejador = new ManejadorVariable();
            Variable unaVariable = new Variable("Variable1", 1, 3);
            Assert.Throws<System.ArgumentException>(
               delegate
               {
                   unManejador.ObtenerVariable(unaVariable);

               });
        }

        [Fact]
        public void PruebaVariableEstaAsociadaAAlgunTipoDeDispositivo()
        {
            ManejadorTipoDeDispositivo unManejadorTipoDeDispositivo = new ManejadorTipoDeDispositivo();
            ManejadorVariable unManejadorVariable = new ManejadorVariable();
            AgregarTiposDeDispositivosDePrueba();
            List<TipoDeDispositivo> tiposDeDispositivosAgregados = unManejadorTipoDeDispositivo.ObtenerTodosLosTiposDeDispositivos();
            TipoDeDispositivo unTipoDeDispositivo = tiposDeDispositivosAgregados.ElementAt(0);
            Variable unaVariable = unTipoDeDispositivo.Variables.ElementAt(0);
            Boolean variableEstaAsociada = unManejadorVariable.VariableEstaAsociadaAAlgunTipoDeDispositivo(unaVariable);
            EliminarTiposDeDispositivosDePrueba();
            Assert.True(variableEstaAsociada);
        }

        [Fact]
        public void PruebaVariableEstaAsociadaAAlgunTipoDeDispositivoSinVariableAsociada()
        {
            ManejadorTipoDeDispositivo unManejadorTipoDeDispositivo = new ManejadorTipoDeDispositivo();
            ManejadorVariable unManejadorVariable = new ManejadorVariable();
            AgregarTiposDeDispositivosDePrueba();
            List<TipoDeDispositivo> tiposDeDispositivosAgregados = unManejadorTipoDeDispositivo.ObtenerTodosLosTiposDeDispositivos();
            TipoDeDispositivo unTipoDeDispositivo = tiposDeDispositivosAgregados.ElementAt(0);
            Variable unaVariable = unTipoDeDispositivo.Variables.ElementAt(0);
            unaVariable.VariableId = -1;
            Boolean variableEstaAsociada = unManejadorVariable.VariableEstaAsociadaAAlgunTipoDeDispositivo(unaVariable);
            EliminarTiposDeDispositivosDePrueba();
            Assert.False(variableEstaAsociada);
        }

        [Fact]
        public void PruebaVariableEstaAsociadaAAlgunTipoDeDispositivoSinTiposDeDispositivo()
        {
            ManejadorVariable unManejadorVariable = new ManejadorVariable();
            Variable unaVariable = new Variable("nombre", 2, 2);
            Boolean variableEstaAsociada = unManejadorVariable.VariableEstaAsociadaAAlgunTipoDeDispositivo(unaVariable);
            Assert.False(variableEstaAsociada);
        }
    }
}
