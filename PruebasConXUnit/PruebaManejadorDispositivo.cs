﻿using DominioGestorDispositivos;
using PersistenciaGestorDispositivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PruebasXUnit
{
    [Collection("MisPruebas")]
    public class PruebaManejadorDispositivo
    {
        private TipoDeDispositivo CrearTipoDeDispositivoDePrueba()
        { 
            TipoDeDispositivo unTipoDeDispositivo = new TipoDeDispositivo();
            unTipoDeDispositivo.Nombre = "Termostato";
            unTipoDeDispositivo.Descripcion = "Dispositivo para medir la temperatura..";
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos.Add(unTipoDeDispositivo);
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
            return unTipoDeDispositivo;
        }

        private Dispositivo CrearDispositivoDePrueba()
        {
            TipoDeDispositivo unTipoDeDispositivo = CrearTipoDeDispositivoDePrueba();   
            Dispositivo unDispositivo = new Dispositivo("Nombre de dispositivo", unTipoDeDispositivo);
            return unDispositivo;
        }

        private void EliminarTiposDeDispositivosDePrueba()
        {
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                foreach (var variable in persistenciaBaseDatosGestorDispositivos.Variables)
                {
                    persistenciaBaseDatosGestorDispositivos.Variables.Remove(variable);
                }
                foreach (var tipoDeDispositivo in persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos)
                {
                    persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos.Remove(tipoDeDispositivo);
                }
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
        }

        [Fact]
        public void PruebaAgregarDispositivo()
        {
            ManejadorUsuario unManejadorUsuario = new ManejadorUsuario();
            ManejadorDispositivo unManejadorDispositivo = new ManejadorDispositivo(); 
            Usuario unUsuario = new Usuario("Carlos Rodriguez", "carrod@hotmail.com", "clave1");
            unManejadorUsuario.AgregarUsuario(unUsuario);
            Dispositivo unDispositivo = CrearDispositivoDePrueba();
            unManejadorDispositivo.AgregarDispositivo(unUsuario.UsuarioId, unDispositivo);
            Usuario usuarioAgregado = unManejadorUsuario.ObtenerUsuario(unUsuario.Email);
            Boolean dispositivoBienAgregado = usuarioAgregado.Dispositivos.Contains(unDispositivo);
            unManejadorDispositivo.EliminarDispositivo(unDispositivo);
            unManejadorUsuario.EliminarUsuario(unUsuario);
            EliminarTiposDeDispositivosDePrueba();
            Assert.True(dispositivoBienAgregado);
        }

        [Fact]
        public void PruebaModificarDispositivo()
        {
            ManejadorUsuario unManejadorUsuario = new ManejadorUsuario();
            ManejadorDispositivo unManejadorDispositivo = new ManejadorDispositivo();
            Usuario unUsuario = new Usuario("Carlos Rodriguez", "carrod@hotmail.com", "clave1");
            unManejadorUsuario.AgregarUsuario(unUsuario);
            Dispositivo unDispositivo = CrearDispositivoDePrueba();
            unManejadorDispositivo.AgregarDispositivo(unUsuario.UsuarioId, unDispositivo);
            Usuario usuarioAgregado = unManejadorUsuario.ObtenerUsuario(unUsuario.Email);
            Dispositivo dispositivoAgregado = usuarioAgregado.Dispositivos.ElementAt(0);
            unDispositivo.Nombre = "Modificado";
            unDispositivo.DispositivoId = dispositivoAgregado.DispositivoId;
            unManejadorDispositivo.ModificarDispositivo(unDispositivo);
            usuarioAgregado = unManejadorUsuario.ObtenerUsuario(unUsuario.Email);
            Dispositivo dispositivoModificado = usuarioAgregado.Dispositivos.ElementAt(0);
            Boolean dispositivoBienModificado = dispositivoModificado.Nombre.Equals("Modificado");
            unManejadorDispositivo.EliminarDispositivo(unDispositivo);
            unManejadorUsuario.EliminarUsuario(unUsuario);
            EliminarTiposDeDispositivosDePrueba(); 
            Assert.True(dispositivoBienModificado);
        }

        [Fact]
        public void PruebaModificarDispositivoConDispositivoNoExistente()
        {
            ManejadorDispositivo unManejadorDispositivo = new ManejadorDispositivo();
            Dispositivo unDispositivo = new Dispositivo();
            unDispositivo.Nombre = "Dispositivo";
            Assert.Throws<System.ArgumentException>(
             delegate
             {
                 unManejadorDispositivo.ModificarDispositivo(unDispositivo);
             });
        }

        [Fact]
        public void PruebaEliminarDispositivo()
        {
            ManejadorUsuario unManejadorUsuario = new ManejadorUsuario();
            ManejadorDispositivo unManejadorDispositivo = new ManejadorDispositivo();
            Usuario unUsuario = new Usuario("Carlos Rodriguez", "carrod@hotmail.com", "clave1");
            unManejadorUsuario.AgregarUsuario(unUsuario);
            Dispositivo unDispositivo = CrearDispositivoDePrueba();
            unManejadorDispositivo.AgregarDispositivo(unUsuario.UsuarioId, unDispositivo);
            unManejadorDispositivo.EliminarDispositivo(unDispositivo);
            Boolean dispositivoBienEliminado = unUsuario.Dispositivos.Count == 0;
            unManejadorUsuario.EliminarUsuario(unUsuario);
            EliminarTiposDeDispositivosDePrueba();
            Assert.True(dispositivoBienEliminado);
        }

        [Fact]
        public void PruebaEliminarDispositivoConDispositivoNoExistente()
        {
            ManejadorDispositivo unManejadorDispositivo = new ManejadorDispositivo();
            Dispositivo unDispositivo = new Dispositivo();
            unDispositivo.Nombre = "Dispositivo";
            Assert.Throws<System.ArgumentException>(
             delegate
             {
                 unManejadorDispositivo.EliminarDispositivo(unDispositivo);
             });
        }

        [Fact]
        public void PruebaExisteDispositivo()
        {
            ManejadorUsuario unManejadorUsuario = new ManejadorUsuario();
            ManejadorDispositivo unManejadorDispositivo = new ManejadorDispositivo();
            Usuario unUsuario = new Usuario("Carlos Rodriguez", "carrod@hotmail.com", "clave1");
            unManejadorUsuario.AgregarUsuario(unUsuario);
            Dispositivo unDispositivo = CrearDispositivoDePrueba();
            unManejadorDispositivo.AgregarDispositivo(unUsuario.UsuarioId, unDispositivo);
            Usuario usuarioAgregado = unManejadorUsuario.ObtenerUsuario(unUsuario.Email);
            Dispositivo dispositivoAgregado = usuarioAgregado.Dispositivos.ElementAt(0);
            Boolean existeDispositivo = unManejadorDispositivo.ExisteDispositivo(dispositivoAgregado);
            unManejadorDispositivo.EliminarDispositivo(unDispositivo);
            unManejadorUsuario.EliminarUsuario(unUsuario);
            EliminarTiposDeDispositivosDePrueba();
            Assert.True(existeDispositivo);
        }

        [Fact]
        public void PruebaExisteDispositivoConDispositivoNoExistente()
        {
            ManejadorDispositivo unManejadorDispositivo = new ManejadorDispositivo();
            Dispositivo unDispositivo = CrearDispositivoDePrueba();
            Boolean existeDispositivo = unManejadorDispositivo.ExisteDispositivo(unDispositivo);
            Assert.False(existeDispositivo);
        }

        [Fact]
        public void PruebaVariablePerteneceADispositivoConDispositivoNoExistente()
        {
            ManejadorDispositivo unManejadorDispositivo = new ManejadorDispositivo();
            Dispositivo unDispositivo = new Dispositivo();
            unDispositivo.Nombre = "Dispositivo";
            Variable unaVariable = new Variable();
            Assert.Throws<System.ArgumentException>(
             delegate
             {
                 unManejadorDispositivo.VariablePerteneceADispositivo(unDispositivo, unaVariable);
             });
        }

        [Fact]
        public void PruebaVariablePerteneceADispositivoConVariableNoPerteneciente()
        {
            ManejadorUsuario unManejadorUsuario = new ManejadorUsuario();
            ManejadorDispositivo unManejadorDispositivo = new ManejadorDispositivo();
            Usuario unUsuario = new Usuario("Carlos Rodriguez", "carrod@hotmail.com", "clave1");
            unManejadorUsuario.AgregarUsuario(unUsuario);
            Dispositivo unDispositivo = CrearDispositivoDePrueba();
            unManejadorDispositivo.AgregarDispositivo(unUsuario.UsuarioId, unDispositivo);
            Usuario usuarioAgregado = unManejadorUsuario.ObtenerUsuario(unUsuario.Email);
            Dispositivo dispositivoAgregado = usuarioAgregado.Dispositivos.ElementAt(0);
            Variable unaVariable = new Variable();
            Boolean variablePertenece = unManejadorDispositivo.VariablePerteneceADispositivo(dispositivoAgregado, unaVariable);
            unManejadorDispositivo.EliminarDispositivo(unDispositivo);
            unManejadorUsuario.EliminarUsuario(unUsuario);
            EliminarTiposDeDispositivosDePrueba();
            Assert.False(variablePertenece);
        }

        [Fact]
        public void PruebaObtenerTodosLosDispositivos()
        {
            ManejadorUsuario unManejadorUsuario = new ManejadorUsuario();
            ManejadorDispositivo unManejadorDispositivo = new ManejadorDispositivo();
            Usuario unUsuario = new Usuario("Carlos Rodriguez", "carrod@hotmail.com", "clave1");
            unManejadorUsuario.AgregarUsuario(unUsuario);
            Dispositivo unDispositivo = CrearDispositivoDePrueba();
            Dispositivo otroDispositivo = CrearDispositivoDePrueba();
            otroDispositivo.Nombre = "otro nombre de dispositivo";
            unManejadorDispositivo.AgregarDispositivo(unUsuario.UsuarioId, unDispositivo);
            unManejadorDispositivo.AgregarDispositivo(unUsuario.UsuarioId, otroDispositivo);
            List<Dispositivo> dispositivosAgregados = unManejadorDispositivo.ObtenerTodosLosDispositivos();
            Dispositivo primerDispositivoAgregado = dispositivosAgregados.ElementAt(0);
            Dispositivo segundoDispositivoAgregado = dispositivosAgregados.ElementAt(1);
            Boolean dispositivosBienObtenidos = unDispositivo.Nombre.Equals(primerDispositivoAgregado.Nombre);
            dispositivosBienObtenidos = dispositivosBienObtenidos && otroDispositivo.Nombre.Equals(segundoDispositivoAgregado.Nombre);
            dispositivosBienObtenidos = dispositivosBienObtenidos && dispositivosAgregados.Count == 2;
            unManejadorDispositivo.EliminarDispositivo(primerDispositivoAgregado);
            unManejadorDispositivo.EliminarDispositivo(segundoDispositivoAgregado);
            unManejadorUsuario.EliminarUsuario(unUsuario);
            EliminarTiposDeDispositivosDePrueba();
            Assert.True(dispositivosBienObtenidos);
        }

        [Fact]
        public void PruebaObtenerTodosLosDispositivosSinDispositivosAgregados()
        {
            ManejadorDispositivo unManejador = new ManejadorDispositivo();
            Assert.Throws<System.ArgumentException>(
               delegate
               {
                   unManejador.ObtenerTodosLosDispositivos();

               });
        }

        [Fact]
        public void PruebaObtenerVariableDeDispositivo()
        {
            ManejadorUsuario unManejadorUsuario = new ManejadorUsuario();
            ManejadorDispositivo unManejadorDispositivo = new ManejadorDispositivo();
            ManejadorVariable unManejadorVariable = new ManejadorVariable();
            ManejadorTipoDeDispositivo unManejadorTipoDeDispositivo = new ManejadorTipoDeDispositivo();
            Variable unaVariable = new Variable("nombre", 1, 1);
            unManejadorVariable.AgregarVariable(unaVariable);
            List<Variable> variables = new List<Variable>();
            variables.Add(unaVariable);
            TipoDeDispositivo unTipoDeDispositivo = new TipoDeDispositivo("Termostato", "Dispositivo para medir la temperatura..", variables);
            unManejadorTipoDeDispositivo.AgregarTipoDeDispositivo(unTipoDeDispositivo);
            Dispositivo unDispositivo = new Dispositivo("dispositivo", unTipoDeDispositivo);
            Usuario unUsuario = new Usuario("Carlos Rodriguez", "carrod@hotmail.com", "clave1");
            unManejadorUsuario.AgregarUsuario(unUsuario);
            unManejadorDispositivo.AgregarDispositivo(unUsuario.UsuarioId, unDispositivo);
            Usuario usuarioAgregado = unManejadorUsuario.ObtenerUsuario("carrod@hotmail.com");
            Dispositivo dispositivoAgregado = usuarioAgregado.Dispositivos.ElementAt(0);
            Variable variableObtenida = unManejadorDispositivo.ObtenerVariableDeDispositivo(dispositivoAgregado.DispositivoId, "nombre");
            Boolean variableBienObtenida = variableObtenida.Nombre.Equals("nombre");
            unManejadorDispositivo.EliminarDispositivo(dispositivoAgregado);
            EliminarTiposDeDispositivosDePrueba();
            unManejadorUsuario.EliminarUsuario(usuarioAgregado);
            Assert.True(variableBienObtenida);
        }

        [Fact]
        public void PruebaObtenerVariableDeDispositivoSinDispositivos()
        {
            ManejadorDispositivo unManejador = new ManejadorDispositivo();
            Assert.Throws<System.ArgumentException>(
               delegate
               {
                   unManejador.ObtenerVariableDeDispositivo(1, "nombre");

               });
        }

        [Fact]
        public void PruebaObtenerVariableDeDispositivoSinDispositivo()
        {
            ManejadorUsuario unManejadorUsuario = new ManejadorUsuario();
            ManejadorDispositivo unManejadorDispositivo = new ManejadorDispositivo();
            Usuario unUsuario = new Usuario("Carlos Rodriguez", "carrod@hotmail.com", "clave1");
            unManejadorUsuario.AgregarUsuario(unUsuario);
            Dispositivo unDispositivo = CrearDispositivoDePrueba();
            unManejadorDispositivo.AgregarDispositivo(unUsuario.UsuarioId, unDispositivo);
            Assert.Throws<System.ArgumentException>(
               delegate
               {
                   unManejadorDispositivo.ObtenerVariableDeDispositivo(-1, "nombre");
               });
            unManejadorDispositivo.EliminarDispositivo(unDispositivo);
            unManejadorUsuario.EliminarUsuario(unUsuario);
            EliminarTiposDeDispositivosDePrueba();
        }

        [Fact]
        public void PruebaObtenerVariableDeDispositivoSinVariable()
        {
            ManejadorUsuario unManejadorUsuario = new ManejadorUsuario();
            ManejadorDispositivo unManejadorDispositivo = new ManejadorDispositivo();
            Usuario unUsuario = new Usuario("Carlos Rodriguez", "carrod@hotmail.com", "clave1");
            unManejadorUsuario.AgregarUsuario(unUsuario);
            Dispositivo unDispositivo = CrearDispositivoDePrueba();
            unManejadorDispositivo.AgregarDispositivo(unUsuario.UsuarioId, unDispositivo);
            Usuario usuarioAgregado = unManejadorUsuario.ObtenerUsuario("carrod@hotmail.com");
            Dispositivo dispositivoAgregado = usuarioAgregado.Dispositivos.ElementAt(0);
            Assert.Throws<System.ArgumentException>(
               delegate
               {
                   unManejadorDispositivo.ObtenerVariableDeDispositivo(dispositivoAgregado.DispositivoId, "nombre de variable no existente");
               });
            unManejadorDispositivo.EliminarDispositivo(dispositivoAgregado);
            unManejadorUsuario.EliminarUsuario(usuarioAgregado);
            EliminarTiposDeDispositivosDePrueba();
        }
    }
}
