﻿using DominioGestorDispositivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PruebasXUnit
{
    public class PruebaVariable
    {
        [Fact]
        public void PruebaConstructor()
        {
            Variable unaVariable = new Variable("Variable 1",1,1);
            Boolean nombreCorrecto = unaVariable.Nombre.Equals("Variable 1");
            Boolean minimoCorrecto = unaVariable.Minimo == 1;
            Boolean maximoCorrecto = unaVariable.Maximo == 1;
            Assert.True(nombreCorrecto && minimoCorrecto && maximoCorrecto);
        }

        [Fact]
        public void PruebaEqualVariablesIguales()
        {
            Variable unaVariable = new Variable("Nombre Variable", 1, 2);
            Variable laMismaVariable = new Variable("Nombre Variable", 1, 2);
            Assert.Equal(unaVariable, laMismaVariable);
        }

        [Fact]
        public void PruebaEqualVariablesDistintas()
        {
            Variable unaVariable = new Variable("Nombre Variable", 1, 2);
            Variable otraVariable = new Variable("Otro Nombre Variable", 1, 3);
            otraVariable.VariableId = 1;
            Assert.NotEqual(unaVariable, otraVariable);
        }
    }
}
