﻿using DominioGestorDispositivos;
using PersistenciaGestorDispositivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PruebasXUnit
{
    [Collection("MisPruebas")]
    public class PruebaManejadorRegistrosDeVariable
    {
        private void AgregarRegistrosDeVariablesDePrueba()
        {
            List<RegistroDeVariable> registrosDeVariables = CrearRegistrosDeVariablesDePrueba();
            RegistroDeVariable unRegistroDeVariable = registrosDeVariables.ElementAt(0);
            RegistroDeVariable otroRegistroDeVariable = registrosDeVariables.ElementAt(1);
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                persistenciaBaseDatosGestorDispositivos.RegistrosDeVariables.Add(unRegistroDeVariable);
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                persistenciaBaseDatosGestorDispositivos.RegistrosDeVariables.Add(otroRegistroDeVariable);
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
        }

        private List<RegistroDeVariable> CrearRegistrosDeVariablesDePrueba()
        {
            CrearDispositivoDePrueba();

            Dispositivo dispositivoDelRegistro = ObtenerPrimerDispositivo();
            Variable variableDelRegistro = ObtenerPrimerVariable();

            double unValor = 12;
            DateTime unaFecha = DateTime.Parse("13/10/1991");
            RegistroDeVariable unRegistroDeVariable = new RegistroDeVariable(dispositivoDelRegistro.DispositivoId, variableDelRegistro.VariableId, unValor, unaFecha);
            DateTime otraFecha = DateTime.Parse("14/10/1991");
            RegistroDeVariable otroRegistroDeVariable = new RegistroDeVariable(dispositivoDelRegistro.DispositivoId, variableDelRegistro.VariableId, unValor, otraFecha);
            List<RegistroDeVariable> registrosDeVariables = new List<RegistroDeVariable>();
            registrosDeVariables.Add(unRegistroDeVariable);
            registrosDeVariables.Add(otroRegistroDeVariable);
            return registrosDeVariables;
        }

        private void CrearDispositivoDePrueba()
        {
            TipoDeDispositivo unTipoDeDispositivo = CrearTipoDeDispositivoDePrueba();
            Dispositivo unDispositivo = new Dispositivo("Nombre de dispositivo", unTipoDeDispositivo);

            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                persistenciaBaseDatosGestorDispositivos.Variables.Attach(unDispositivo.TipoDeDispositivo.Variables.ElementAt(0));
                persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos.Attach(unDispositivo.TipoDeDispositivo);
                persistenciaBaseDatosGestorDispositivos.Dispositivos.Add(unDispositivo);
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
        }

        private TipoDeDispositivo CrearTipoDeDispositivoDePrueba()
        {
            List<Variable> variables = CrearVariablesDePrueba();
            TipoDeDispositivo unTipoDeDispositivo = new TipoDeDispositivo("Termostato", "una descripción", variables);
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                persistenciaBaseDatosGestorDispositivos.Variables.Attach(variables.ElementAt(0));
                persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos.Add(unTipoDeDispositivo);
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
            TipoDeDispositivo tipoDeDispositivoObtenido = ObtenerPrimerTipoDeDispositivo();
            return tipoDeDispositivoObtenido;
        }
        
        private List<Variable> CrearVariablesDePrueba()
        {
            Variable unaVariable = new Variable("Variable1", 1, 21);
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                persistenciaBaseDatosGestorDispositivos.Variables.Add(unaVariable);
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
            List<Variable> variables = new List<Variable>();
            Variable variableObtenida = ObtenerPrimerVariable();
            variables.Add(variableObtenida);
            return variables;
        }

        public Dispositivo ObtenerPrimerDispositivo()
        {
            List<Dispositivo> dispositivosARetornar = new List<Dispositivo>();
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                var dispositivos = from dispositivo in persistenciaBaseDatosGestorDispositivos.Dispositivos.Include("TipoDeDispositivo")
                                   select dispositivo;
                foreach (var dispositivo in dispositivos)
                {
                    dispositivosARetornar.Add(dispositivo);
                }
                return dispositivosARetornar.ElementAt(0);
            }
        }

        public Variable ObtenerPrimerVariable()
        {
            List<Variable> variablesARetornar = new List<Variable>();
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                var variables = from variable in persistenciaBaseDatosGestorDispositivos.Variables
                                select variable;
                foreach (var variable in variables)
                {
                    variablesARetornar.Add(variable);
                }
                return variablesARetornar.ElementAt(0);
            }
        }

        public TipoDeDispositivo ObtenerPrimerTipoDeDispositivo()
        {
            List<TipoDeDispositivo> tiposDeDispositivosARetornar = new List<TipoDeDispositivo>();
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                var tiposDeDispositivos = from tipoDeDispositivo in persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos.Include("Variables")
                                          select tipoDeDispositivo;
                foreach (var tipoDeDispositivo in tiposDeDispositivos)
                {
                    tiposDeDispositivosARetornar.Add(tipoDeDispositivo);
                }
                return tiposDeDispositivosARetornar.ElementAt(0);
            }
        }

        private void EliminarRegistrosDeVariablesDePrueba()
        {
            using (var persistenciaBaseDatosGestorDispositivos = new ContextoClasesGestorDeDispositivos())
            {
                foreach (var variable in persistenciaBaseDatosGestorDispositivos.Variables)
                {
                    persistenciaBaseDatosGestorDispositivos.Variables.Remove(variable);
                }
                foreach (var dispositivo in persistenciaBaseDatosGestorDispositivos.Dispositivos)
                {
                    persistenciaBaseDatosGestorDispositivos.Dispositivos.Remove(dispositivo);
                }
                foreach (var registro in persistenciaBaseDatosGestorDispositivos.RegistrosDeVariables)
                {
                    persistenciaBaseDatosGestorDispositivos.RegistrosDeVariables.Remove(registro);
                }
                foreach (var tipoDeDispositivo in persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos)
                {
                    persistenciaBaseDatosGestorDispositivos.TiposDeDispositivos.Remove(tipoDeDispositivo);
                }
                persistenciaBaseDatosGestorDispositivos.SaveChanges();
            }
        }

        [Fact]
        public void PruebaObtenerRegistrosParaDispositivoVariableFechas()
        {
            ManejadorRegistrosDeVariable unManejador = new ManejadorRegistrosDeVariable();
            AgregarRegistrosDeVariablesDePrueba();
            Dispositivo unDispositivo = ObtenerPrimerDispositivo();
            Variable unaVariable = ObtenerPrimerVariable();
            DateTime unaFecha = DateTime.Parse("13/10/1991");
            List<RegistroDeVariable> registrosDeVariablesAgregados = unManejador.ObtenerRegistrosParaDispositivoVariableFechas(unDispositivo.DispositivoId, unaVariable.VariableId, unaFecha, unaFecha);
            Boolean cantidadDeRegistrosObtenidosCorrecta = registrosDeVariablesAgregados.Count == 1;
            Boolean valorDeRegistroObtenidoCorrecto = registrosDeVariablesAgregados.ElementAt(0).Valor == 12;
            EliminarRegistrosDeVariablesDePrueba();
            Assert.True(cantidadDeRegistrosObtenidosCorrecta && valorDeRegistroObtenidoCorrecto);
        }

        [Fact]
        public void PruebaAgregarRegistroDeVariableConValorValido()
        {
            ManejadorRegistrosDeVariable unManejador = new ManejadorRegistrosDeVariable();
            List<RegistroDeVariable> registrosDeVariables = CrearRegistrosDeVariablesDePrueba();
            RegistroDeVariable unRegistro = registrosDeVariables.ElementAt(0);
            unManejador.AgregarRegistroDeVariable(unRegistro);
            DateTime unaFecha = DateTime.Parse("13/10/1991");
            List<RegistroDeVariable> registrosDeVariablesAgregados = unManejador.ObtenerRegistrosParaDispositivoVariableFechas(unRegistro.DispositivoId, unRegistro.VariableId, unaFecha, unaFecha);
            Boolean cantidadDeRegistrosObtenidosCorrecta = registrosDeVariablesAgregados.Count == 1;
            Boolean valorDeRegistroObtenidoCorrecto = registrosDeVariablesAgregados.ElementAt(0).Valor == 12;
            EliminarRegistrosDeVariablesDePrueba();
            Assert.True(cantidadDeRegistrosObtenidosCorrecta && valorDeRegistroObtenidoCorrecto);
        }

        [Fact]
        public void PruebaAgregarRegistroDeVariableConValorNoValido()
        {
            ManejadorRegistrosDeVariable unManejador = new ManejadorRegistrosDeVariable();
            List<RegistroDeVariable> registrosDeVariables = CrearRegistrosDeVariablesDePrueba();
            RegistroDeVariable unRegistro = registrosDeVariables.ElementAt(0);
            int unaVariableId = unRegistro.VariableId;
            unRegistro.VariableId = unaVariableId;
            unRegistro.Valor = 57;
            Assert.Throws<System.ArgumentException>(
               delegate
               {
                   unManejador.AgregarRegistroDeVariable(unRegistro);
               });
            EliminarRegistrosDeVariablesDePrueba();
        }

        [Fact]
        public void PruebaAgregarRegistroDeVariableConDispositivoNoIngresado()
        {
            ManejadorRegistrosDeVariable unManejador = new ManejadorRegistrosDeVariable();
            Dispositivo unDispositivo = new Dispositivo();
            RegistroDeVariable unRegistro = new RegistroDeVariable();
            unRegistro.DispositivoId = unDispositivo.DispositivoId;
            Assert.Throws<System.ArgumentException>(
               delegate
               {
                   unManejador.AgregarRegistroDeVariable(unRegistro);
               });
        }

        [Fact]
        public void PruebaAgregarRegistroDeVariableConVariableNoPertenecienteADipositivo()
        {
            ManejadorRegistrosDeVariable unManejador = new ManejadorRegistrosDeVariable();
            List<RegistroDeVariable> registrosDeVariables = CrearRegistrosDeVariablesDePrueba();
            RegistroDeVariable unRegistro = registrosDeVariables.ElementAt(0);
            unManejador.AgregarRegistroDeVariable(unRegistro);
            Variable unaVariable = new Variable();
            unRegistro.VariableId = unaVariable.VariableId;
            Assert.Throws<System.ArgumentException>(
               delegate
               {
                   unManejador.AgregarRegistroDeVariable(unRegistro);
               });
            EliminarRegistrosDeVariablesDePrueba();
        }
    }
}
