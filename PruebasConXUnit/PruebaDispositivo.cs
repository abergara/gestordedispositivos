﻿using DominioGestorDispositivos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace PruebasXUnit
{
    public class PruebaDispositivo
    {
        private TipoDeDispositivo CrearTipoDeDispositivoDePrueba()
        {
            TipoDeDispositivo unTipoDeDispositivo = new TipoDeDispositivo();
            unTipoDeDispositivo.Nombre = "Termostato";
            unTipoDeDispositivo.Descripcion = "Dispositivo para medir la temperatura..";
            return unTipoDeDispositivo;
        }

        [Fact]
        public void PruebaEqualsDispositivosIguales()
        {
            TipoDeDispositivo unTipoDeDispositivo = CrearTipoDeDispositivoDePrueba();
            Dispositivo unDispositivo = new Dispositivo("Nombre de dispositivo", unTipoDeDispositivo);
            Dispositivo elMismoDispositivo = new Dispositivo("Nombre de dispositivo", unTipoDeDispositivo);;
            Assert.Equal(unDispositivo, elMismoDispositivo);
        }

        [Fact]
        public void PruebaEqualsDispositivosDistintos()
        {
            TipoDeDispositivo unTipoDeDispositivo = CrearTipoDeDispositivoDePrueba();
            Dispositivo unDispositivo = new Dispositivo("Nombre de dispositivo", unTipoDeDispositivo);
            Dispositivo otroDispositivo = new Dispositivo("Nombre de otro dispositivo", unTipoDeDispositivo);
            otroDispositivo.DispositivoId = 1;
            Assert.NotEqual(unDispositivo, otroDispositivo);
        }

        [Fact]
        public void PruebaConstructor()
        {
            TipoDeDispositivo unTipoDeDispositivo = CrearTipoDeDispositivoDePrueba();
            Dispositivo unDispositivo = new Dispositivo("Nombre de dispositivo", unTipoDeDispositivo);
            Boolean nombreCorrecto = unDispositivo.Nombre.Equals("Nombre de dispositivo");
            TipoDeDispositivo elTipoDeDispositivo = unDispositivo.TipoDeDispositivo;
            Boolean nombreTipoDispositivoCorrecto = elTipoDeDispositivo.Nombre.Equals("Termostato");
            Boolean DescripcionTipoDispositivoCorrecta = elTipoDeDispositivo.Descripcion.Equals("Dispositivo para medir la temperatura..");
            Assert.True(nombreCorrecto && nombreTipoDispositivoCorrecto && DescripcionTipoDispositivoCorrecta);
        }
    }
}
