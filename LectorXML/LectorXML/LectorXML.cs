﻿using ImportadorDeRegistros;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace LectorXML
{
    public class LectorXML : IImportador
    {
        public string ObtenerNombre()
        {
            return "LectorXML";
        }

        public List<ObjetoTransferenciaDatosRegistroDeVariable> ObtenerRegistros()
        {
            List<ObjetoTransferenciaDatosRegistroDeVariable> DTOsARetornar = new List<ObjetoTransferenciaDatosRegistroDeVariable>();
            bool formatoCorrecto = true;
            string rutaArchivo = Path.Combine(Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%"), "registros\\migracion.xml");
            XmlTextReader lector = new XmlTextReader(rutaArchivo);
            lector.WhitespaceHandling = WhitespaceHandling.None;
            lector.Read();
            lector.Read();
            if (lector.NodeType.Equals(XmlNodeType.Element) && lector.Name.Equals("Dispositivos"))
            {
                lector.Read();
                DTOsARetornar = ObtenerDTOs(lector);
                if (!(lector.NodeType.Equals(XmlNodeType.EndElement) && lector.Name.Equals("Dispositivos")))
                {
                    formatoCorrecto = false;
                }
            }
            else
            {
                formatoCorrecto = false;
            }

            if (formatoCorrecto)
            {
                return DTOsARetornar;
            }
            else
            {
                throw new ArgumentException("El formato del archivo XML no es correcto. Revise el manual del sistema para mas información.");
            }
        }

        private List<ObjetoTransferenciaDatosRegistroDeVariable> ObtenerDTOs(XmlTextReader lector)
        {
            List<ObjetoTransferenciaDatosRegistroDeVariable> DTOsARetornar = new List<ObjetoTransferenciaDatosRegistroDeVariable>();
            List<ObjetoTransferenciaDatosRegistroDeVariable> DTOsDeDispositivo;
            int dispositivoId;
            while (lector.NodeType.Equals(XmlNodeType.Element) && lector.Name.Equals("Dispositivo"))
            {
                if (lector.HasAttributes)
                {
                    lector.MoveToNextAttribute();
                    if (lector.Name.Equals("Identificador"))
                    {
                        dispositivoId = int.Parse(lector.Value);
                        lector.MoveToElement();
                        lector.Read();
                        DTOsDeDispositivo = ObtenerDTOsPorDispositivo(dispositivoId, lector);
                        if (!(lector.NodeType.Equals(XmlNodeType.EndElement) && lector.Name.Equals("Dispositivo")))
                        {
                            throw new ArgumentException("El formato del archivo XML no es correcto. Revise el manual del sistema para mas información.");
                        }
                        lector.Read();
                        DTOsARetornar = UnirListas(DTOsARetornar, DTOsDeDispositivo);
                    }
                }
            }
            return DTOsARetornar;
        }

        private List<ObjetoTransferenciaDatosRegistroDeVariable> ObtenerDTOsPorDispositivo(int dispositivoId, XmlTextReader lector)
        {
            List<ObjetoTransferenciaDatosRegistroDeVariable> DTOsARetornar = new List<ObjetoTransferenciaDatosRegistroDeVariable>();
            List<ObjetoTransferenciaDatosRegistroDeVariable> DTOsDeDispositivoYVariable;
            string nombreVariable;
            while (lector.NodeType.Equals(XmlNodeType.Element) && lector.Name.Equals("Variable"))
            {
                if (lector.HasAttributes)
                {
                    lector.MoveToNextAttribute();
                    if (lector.Name.Equals("nombre"))
                    {
                        nombreVariable = lector.Value;
                        lector.MoveToElement();
                        lector.Read();
                        DTOsDeDispositivoYVariable = ObtenerDTOsPorDispositivoYVariable(dispositivoId, nombreVariable, lector);
                        if (!(lector.NodeType.Equals(XmlNodeType.EndElement) && lector.Name.Equals("Variable")))
                        {
                            throw new ArgumentException("El formato del archivo XML no es correcto. Revise el manual del sistema para mas información.");
                        }
                        lector.Read();
                        DTOsARetornar = UnirListas(DTOsARetornar, DTOsDeDispositivoYVariable);
                    }
                }
            }
            return DTOsARetornar;
        }

        private List<ObjetoTransferenciaDatosRegistroDeVariable> UnirListas(List<ObjetoTransferenciaDatosRegistroDeVariable> DTOsInicial, List<ObjetoTransferenciaDatosRegistroDeVariable> DTOsFinal)
        {
            foreach (var DTO in DTOsFinal)
            {
                DTOsInicial.Add(DTO);
            }
            return DTOsInicial;
        }

        private List<ObjetoTransferenciaDatosRegistroDeVariable> ObtenerDTOsPorDispositivoYVariable(int dispositivoId, string nombreVariable, XmlTextReader lector)
        {
            List<ObjetoTransferenciaDatosRegistroDeVariable> DTOsARetornar = new List<ObjetoTransferenciaDatosRegistroDeVariable>();
            DateTime fecha;
            double valor;
            while (lector.NodeType.Equals(XmlNodeType.Element) && lector.Name.Equals("Valor"))
            {
                if (lector.HasAttributes)
                {
                    lector.MoveToNextAttribute();
                    if (lector.Name.Equals("fecha"))
                    {
                        fecha = DateTime.Parse(lector.Value);
                        lector.MoveToElement();
                        lector.Read();
                        valor = ObtenerValorRegistro(lector);
                        ObjetoTransferenciaDatosRegistroDeVariable unDTO = new ObjetoTransferenciaDatosRegistroDeVariable(dispositivoId, nombreVariable, valor, fecha);
                        DTOsARetornar.Add(unDTO);
                    }
                }
            }
            return DTOsARetornar;
        }

        private double ObtenerValorRegistro(XmlTextReader lector)
        {
            double valor;
            if (lector.NodeType.Equals(XmlNodeType.Text))
            {
                valor = double.Parse(lector.Value);
                lector.Read();
                if (lector.NodeType.Equals(XmlNodeType.EndElement) && lector.Name.Equals("Valor"))
                {
                    lector.Read();
                }
                else
                {
                    throw new ArgumentException("El formato del archivo XML no es correcto. Revise el manual del sistema para mas información.");
                }
            }
            else
            {
                throw new ArgumentException("El formato del archivo XML no es correcto. Revise el manual del sistema para mas información.");
            }
            return valor;
        }
    }
}
